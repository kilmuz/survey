package com.model;

/**
 * Created by rungrawee.w on 14/07/2016.
 */
public class LandInfo {
    public String LandScripType  = "";
    public String Parcel_Num  = "";
    public String Parcel_Rai   = "";
    public String Parcel_Nang   = "";
    public String Parcel_Va   = "";
    public String Parcel_Ser  = "";
    public String Parcel_Id  = "";
    public String ZBL  = "";
    public String Moo  = "";
    public String Tmb  = "";
}
