package com.model;

/**
 * Created by rungrawee.w on 17/07/2016.
 */
public class LandUse {

    public boolean isMaster = false;
    public String typeLandUse = "";
    public String landUseName = "";
    public String rai = "-";
    public String nang = "-";
    public String va = "-";
    public String Parcel_Tax = "-1";
    public String Parcel_ID = "";
    public String Parcel_Ser = "";
    public String Parcel_Tmb = "";
}
