package com.model;

import java.util.ArrayList;

/**
 * Created by rungrawee.w on 14/08/2016.
 */
public class BuildingInfo {

    public ArrayList<SurveyBuilding> arLstSurveyBuilding = new ArrayList<SurveyBuilding>();
    public ArrayList<SurveyBuildingOwner> arLstSurveyBuildingOwner = new ArrayList<SurveyBuildingOwner>();
}
