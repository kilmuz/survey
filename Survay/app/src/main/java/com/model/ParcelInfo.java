package com.model;

import java.util.ArrayList;

/**
 * Created by rungrawee.w on 14/08/2016.
 */
public class ParcelInfo {

    public ArrayList<LandUse> arLstLandUse = new ArrayList<LandUse>();
    public ArrayList<SurveyParcelOwner> arLstParcelOwner = new ArrayList<SurveyParcelOwner>();
}
