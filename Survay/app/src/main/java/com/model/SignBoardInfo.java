package com.model;

import java.util.ArrayList;

/**
 * Created by rungrawee.w on 14/08/2016.
 */
public class SignBoardInfo {

    public ArrayList<SurveySignBoard> arLstSurveySignBoard = new ArrayList<SurveySignBoard>();
    public ArrayList<SurveySignBoardOwner> arLstSurveySignBoardOwner = new ArrayList<SurveySignBoardOwner>();
}
