package com.model;

/**
 * Created by rungrawee.w on 17/07/2016.
 */
public class SurveyBuilding {
    public int Build_No = -1;
    public String Parcel_ID = "";
    public String Parcel_Ser = "";
    public String Parcel_Tmb = "";
    public String Building_Moo = "";
    public String Building_Group = "";
    public String Building_Num = "";
    public String Building_Location = "";
    public String Building_Type = "";
    public String Building_Room = "";
    public String Building_Floor = "";
    public String Building_Width = "";
    public String Building_Higth = "";
    public String Building_UseA = "";
    public String Building_UseB = "";
    public String Building_UseC = "";
    public String Building_Rent = "";
    public String Building_Tax = "-1";
    public String CreateBy = "mobile";
    public String CreateDate = "";
    public String ModifyBy = "";
    public String ModifyDate = "";
    public String StatusLink = "";

}
