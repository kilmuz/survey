package com.model;

/**
 * Created by rungrawee.w on 17/07/2016.
 */
public class SurveySignBoard {
    public String Sign_No = "";
    public String Parcel_ID = "";
    public String Parcel_Ser = "";
    public String Parcel_Tmb = "";
    public String Sign_Type = "";
    public String Sign_txt = "";
    public String Sign_Width = "";
    public String Sign_Higth = "";
    public String Sign_Side = "";
    public String Sign_Tax = "-1";
    public String CreateBy = "mobile";
    public String CreateDate = "";
    public String ModifyBy = "";
    public String ModifyDate = "";
    public String StatusLink = "";
}
