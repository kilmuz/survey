package com.survay;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.Object.ShareData;
import com.ObjectDatabase.DataBase;
import com.ObjectDatabase.DatabaseManger;
import com.adapter.SubBlockAdapter;
import com.model.SubBlockParcel;

import java.util.ArrayList;

public class ListSubBlockParcel extends AppCompatActivity {
    private RecyclerView recyclerView = null;
    private SubBlockAdapter blockAdapter = null;
    Bundle bundle = null;
    private TextView tvAmount = null;
    ArrayList<SubBlockParcel> arrayListBlockParcel = new ArrayList<SubBlockParcel>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_sub_block_parcel);

        bundle = getIntent().getExtras();
        DataBase db = new DataBase(this);
        ShareData.setDB(db);

        ShareData.arLstLandUse.clear();
        recyclerView = (RecyclerView)findViewById(R.id.recyclerview);
        tvAmount = (TextView)findViewById(R.id.tvAmount);


    }

    @Override
    protected void onResume() {
        super.onResume();

        initial();
        initialValue();
    }

    private void initialValue()
    {
        tvAmount.setText("จำนวน "+arrayListBlockParcel.size()+" รายการ");
    }
    private void initial()
    {
        DatabaseManger dbManager = new DatabaseManger();


        String prefixBlockParcel = "";
        if( bundle != null )
            prefixBlockParcel = bundle.getString("blockParcel");

        arrayListBlockParcel = dbManager.getSubBlockParcel(prefixBlockParcel);
        blockAdapter = new SubBlockAdapter(arrayListBlockParcel , ListSubBlockParcel.this);
        recyclerView.setLayoutManager(new LinearLayoutManager(ListSubBlockParcel.this));
        recyclerView.setAdapter(blockAdapter);
        blockAdapter.setListenerItemClick(new SubBlockAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, SubBlockParcel subBlockParcel) {
                Intent intent = new Intent(ListSubBlockParcel.this, MainActivity.class);

                intent.putExtra("blockParcel" , subBlockParcel.blockParcel);
                intent.putExtra("Parcel_Ser" , subBlockParcel.Parcel_Ser);
                intent.putExtra("Parcel_ID" , subBlockParcel.Parcel_ID);
                intent.putExtra("Parcel_Tmb" , subBlockParcel.Parcel_Tmb);
//                intent.putExtra("ListBlockParcel", arrayListBlockParcel);
                intent.putExtra("count_zbl" , position);
                ShareData.getInstance().arLstSubBlockParcel = arrayListBlockParcel;
                ShareData.getInstance().PATH_PIC_SUB_SECOND = subBlockParcel.blockParcel.substring(0,3);
                startActivity(intent);
            }
        });
    }
}
