package com.survay;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import com.Object.ShareData;
import com.Object.utils;
import com.ObjectDatabase.DataBase;
import com.ObjectDatabase.DatabaseManger;
import com.model.BuildingInfo;
import com.model.LandUse;
import com.model.PictureInfo;
import com.model.SignBoardInfo;
import com.model.SubBlockParcel;
import com.model.SurveyBuilding;
import com.model.SurveyBuildingOwner;
import com.model.SurveyParcelOwner;
import com.model.SurveySignBoard;
import com.model.SurveySignBoardOwner;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends BaseOnActivity {

    FrameLayout mFragmentContainer = null;
    List<Fragment> fList = null;
    ViewPager vPager = null;
    TabLayout tabLayout = null;

    Bundle bundle = null;
    String parcelZBL = "";
    String Parcel_Ser = "";
    String Parcel_ID = "";
    String Parcel_Tmb = "";
    DataBase mDB = null;
    TextView tvSave = null;
    TextView tvNext = null;
    TextView tvPrevious = null;
    ImageButton imbHome = null;

    int nCountZBL = -1;
    ArrayList<SubBlockParcel> arLstSubBlockParcel = null;

    private final String PATH_PIC_SURVEY = Environment.getExternalStorageDirectory().getAbsolutePath()+"/pic_survey/"+ShareData.getInstance().PATH_PIC_SUB_FIRST+"/"+ShareData.getInstance().PATH_PIC_SUB_SECOND;
    private final String TAG = getClass().getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        DataBase db = new DataBase(this);
//        ShareData.setDB(db);
//        DatabaseManger dbManager = new DatabaseManger();
//        Log.e(getClass().getSimpleName(),"\n---------Start-------");
//        Log.e(getClass().getSimpleName(),"getTypeBuilding = "+dbManager.getTypeBuilding());
//        Log.e(getClass().getSimpleName(),"getTypeLanduse = "+dbManager.getTypeLanduse());
//        Log.e(getClass().getSimpleName(),"getTypeSignBoard = "+dbManager.getTypeSignBoard());
//        Log.e(getClass().getSimpleName(),"getIdTypeBuilding = "+dbManager.getIdTypeBuilding("เก็บน้ำมัน"));
//        Log.e(getClass().getSimpleName(),"getIdTypeLanduse = "+dbManager.getIdTypeLanduse("ทำนา"));
//        Log.e(getClass().getSimpleName(),"getIdTypeSignBoard = "+dbManager.getIdTypeSignBoard("อักษรไทยล้วน"));
//        Log.e(getClass().getSimpleName(),"getLandInfo = "+dbManager.getLandInfo(1).get(0));
//        Log.e(getClass().getSimpleName(),"---------End-------\n");
        bundle = getIntent().getExtras();



        initial();
        initialEvent();
    }

    private void initialEvent()
    {
        tvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                {
                    onClickSave(true,1);
                }

            }
        });

        tvNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onClickSave(false,1);
            }
        });
        tvPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickSave(false,0);
            }
        });
    }

    private void onClickSave(final boolean isSave, final int isNext) {
        final Dialog dlgSave = new Dialog(MainActivity.this);
        dlgSave.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dlgSave.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dlgSave.setContentView(R.layout.dlg_save);
        dlgSave.setCancelable(true);

        final boolean nxt = (isNext==0)? false : true;
        Button btnSave = (Button)dlgSave.findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dlgSave.dismiss();

                if (isNext == -1 && isSave) {
//                    new ProcessSave().execute(nxt);
                    save();
                } else {
                    if (isSave) {
//                        new ProcessSave().execute(nxt);
                        if( ShareData.arLstSignBoardInfo.size() <= 0 && ShareData.arLstBuildingInfo.size() <= 0
                                && (ShareData.arLstParcelOwner.size() <= 0 || ShareData.arLstLandUse.size() <=0) )
                        {
                            utils.createPopup(MainActivity.this , "กรุณาเพิ่มข้อมูล" , "");
                        }
                        else
                        {
                            save();
                            utils.createPopup(MainActivity.this, "สำเร็จ", "", "ตกลง", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    goNextOrPrev(nxt);
                                }
                            });
                        }
                    } else
                        goNextOrPrev(nxt);
                }

            }
        });

        Button btnCancel = (Button)dlgSave.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dlgSave.dismiss();
                if (!isSave) {
                    goNextOrPrev(nxt);
                }
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dlgSave.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dlgSave.show();
        dlgSave.getWindow().setAttributes(lp);
    }

    private void goNextOrPrev(boolean isNext) {
        int count_zbl = (isNext) ? ++nCountZBL : --nCountZBL;
        if (isNext) {
            if (count_zbl> arLstSubBlockParcel.size()-1){
                count_zbl=0;
            }
        } else {
            if (count_zbl < 0) {
                count_zbl = arLstSubBlockParcel.size() - 1;
            }
        }

        Intent intent = getIntent();
        intent.putExtra("count_zbl" , count_zbl);
        intent.putExtra("blockParcel" , arLstSubBlockParcel.get(count_zbl).blockParcel);
        intent.putExtra("Parcel_Ser" , arLstSubBlockParcel.get(count_zbl).Parcel_Ser);
        intent.putExtra("Parcel_ID" , arLstSubBlockParcel.get(count_zbl).Parcel_ID);
        finish();
        startActivity(intent);
    }

    private void initial()
    {
        ShareData.arBuildingBitmap = new ArrayList<PictureInfo>();
        ShareData.arSignBoardBitmap = new ArrayList<PictureInfo>();

        arLstSubBlockParcel = new ArrayList<SubBlockParcel>();
        arLstSubBlockParcel = ShareData.getInstance().arLstSubBlockParcel;
        tabLayout = (TabLayout)findViewById(R.id.tabsLayout);
        if( bundle != null )
        {
            if( bundle.containsKey("blockParcel") )
            {
                parcelZBL = bundle.getString("blockParcel");
            }

            if( bundle.containsKey("Parcel_ID") )
            {
                Parcel_ID = bundle.getString("Parcel_ID");
            }

            if( bundle.containsKey("Parcel_Tmb") )
            {
                Parcel_Tmb = bundle.getString("Parcel_Tmb");
            }

            if( bundle.containsKey("Parcel_Ser") )
            {
                Parcel_Ser = bundle.getString("Parcel_Ser");
            }

            if( bundle.containsKey("ListBlockParcel") )
            {
                arLstSubBlockParcel = (ArrayList<SubBlockParcel>) getIntent().getSerializableExtra("ListBlockParcel");
            }

            if(bundle.containsKey("count_zbl")) {
                nCountZBL = bundle.getInt("count_zbl");
            }
        }

        ShareData.arMapImageBitmap.clear();
        File f = new File(PATH_PIC_SURVEY);
        if (!f.exists())
        {
            f.mkdir();
        } else {
            LoadImage(f);
        }

        List<Fragment> fragments = new ArrayList<Fragment>();
        fragments.add(DetailParcelFragment.newInstance(parcelZBL));
        fragments.add(MainBuildingFragment.newInstance(parcelZBL));
        fragments.add(MainBoardFragment.newInstance(parcelZBL));
        MyPageAdapter pageAdapter = new MyPageAdapter(getSupportFragmentManager(), fragments);

        vPager = (ViewPager)findViewById(R.id.viewpager);
        vPager.setAdapter(pageAdapter);
        vPager.setOffscreenPageLimit(3);
        tabLayout.setupWithViewPager(vPager);

        tvSave = (TextView)findViewById(R.id.tvSave);
        tvNext = (TextView)findViewById(R.id.tvNext);
        tvPrevious = (TextView)findViewById(R.id.tvPrevious);
        imbHome = (ImageButton)findViewById(R.id.iBtnHome);
        imbHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this,ListBlockParcel.class);

                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(i);
                finish(); // to end the current activity
            }
        });

        mDB = ShareData.getDB();
    }

    private void save()
    {

        {
            TableSurveyParcelOwnerUse();
            TableSurveyParcelLandUse();

            TableSurveySignBoard();
            TableSurveyBuilding();
            SaveImage();

            UpdateStatusSurvey();
        }
    }

    private void LoadImage(File file) {
        String[] fileNames = file.list();
        for (int i = 0; i < fileNames.length; i++) {
            String filename = fileNames[i];
            Bitmap mBitmap = BitmapFactory.decodeFile(file.getPath() + "/" + filename);
            String[] imgName = filename.split("_");
                if (imgName[0].equals("l")) {

                    if (imgName[1].equals(parcelZBL)) {
                        if (imgName[2].equals("1.jpg")) {
                            ShareData.arMapImageBitmap.put(131, mBitmap);
                        }
                        if (imgName[2].equals("2.jpg")) {
                            ShareData.arMapImageBitmap.put(132, mBitmap);
                        }
                        if (imgName[2].equals("3.jpg")) {
                            ShareData.arMapImageBitmap.put(133, mBitmap);
                        }
                    }
                } else if (imgName[0].equals("b")) {
                    String rowid = imgName[2];
                    PictureInfo pictureInfo = ShareData.arBuildingImageBitmap.get(imgName[1]+"_"+rowid);
                    if (pictureInfo==null) {pictureInfo = new PictureInfo();}
                    if (imgName[3].equals("1.jpg")) {
                        pictureInfo.picture1 = mBitmap;
                    }
                    if (imgName[3].equals("2.jpg")) {
                        pictureInfo.picture2 = mBitmap;
                    }
                    if (imgName[3].equals("3.jpg")) {
                        pictureInfo.picture3 = mBitmap;
                    }
                    ShareData.arBuildingImageBitmap.put(imgName[1]+"_"+rowid,pictureInfo);

                } else if (imgName[0].equals("s")) {
                    String rowid = imgName[2];
                    PictureInfo pictureInfo = ShareData.arSignBoardImageBitmap.get(imgName[1]+"_"+rowid);
                    if (pictureInfo==null) {pictureInfo = new PictureInfo();}
                    if (imgName[3].equals("1.jpg")) {
                        pictureInfo.picture1 = mBitmap;
                    }
                    if (imgName[3].equals("2.jpg")) {
                        pictureInfo.picture2 = mBitmap;
                    }
                    if (imgName[3].equals("3.jpg")) {
                        pictureInfo.picture3 = mBitmap;
                    }
                    ShareData.arSignBoardImageBitmap.put(imgName[1]+"_"+rowid,pictureInfo);
                }

        }
    }

    private void SaveImage() {

        /*File dir = new File(PATH_PIC_SURVEY);
        if(!dir.exists())
            dir.mkdirs();*/

        //String introString = "_";
        for (Map.Entry<Integer, Bitmap> entry : ShareData.arMapImageBitmap.entrySet()) {
            /*if (entry.getKey() > 300) {
                introString = "s_";
                rowId = "_" + rowId;
            } else if (entry.getKey() > 200) {
                introString = "b_";
                rowId = "_" + rowId;
            } else if (entry.getKey() > 100) {
                introString = "l_";
            }
            try {
                String num = "" + entry.getKey();
                Bitmap bmp = entry.getValue();
                File file = new File(dir, introString + parcelZBL + rowId + "_" + num.substring(num.length()-1) +".jpg");
                FileOutputStream fOut = new FileOutputStream(file);

                bmp.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
                fOut.flush();
                fOut.close();
            } catch (IOException e) {
                e.printStackTrace();
            }*/
            SaveImageWithRowId("",entry.getKey(),entry.getValue(),parcelZBL);
        }

        ShareData.arMapImageBitmap = new HashMap<>();
    }

    private void SaveImageWithRowId(String rowId,int key,Bitmap bitmap,String pacelZBL) {
        if (bitmap == null) return;


        File dir = new File(PATH_PIC_SURVEY);
        if(!dir.exists())
            dir.mkdirs();

        String introString = "_";
        if (key > 300) {
            introString = "s_";
            rowId = "_" + rowId;
        } else if (key > 200) {
            introString = "b_";
            rowId = "_" + rowId;
        } else if (key > 100) {
            introString = "l_";
        }
        try {
            String num = "" + key;
            Bitmap bmp = bitmap;
            File file = new File(dir, introString + pacelZBL + rowId + "_" + num.substring(num.length()-1) +".jpg");
            FileOutputStream fOut = new FileOutputStream(file);

            bmp.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
            fOut.flush();
            fOut.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void DeleteImage(String type) {
//        try {
            File dir = new File(PATH_PIC_SURVEY);
            if(!dir.exists())
                return;
            else {
                File[] files = dir.listFiles();
                for (int i = 0; i < files.length; i++) {
                    String filename = files[i].getName();
                    String[] imgName = filename.split("_");
                    /*if (imgName[1].equals(parcelZBL) && type.equals("s"))
                        files[i].delete();
                    else */if (imgName[1].equals(parcelZBL) && type.equals(imgName[0]))
                        files[i].delete();

                }
            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }

    private void UpdateStatusSurvey()
    {
        ContentValues cv = new ContentValues();
//        cv.put("ZBL","");
//        cv.put("Parcel_Doc","");
//        cv.put("Parcel_ID","");
//        cv.put("Parcel_Ser","");
//        cv.put("Parcel_Num","");
//        cv.put("Parcel_Rai","");
//        cv.put("Parcel_Nang","");
//        cv.put("Parcel_Va","");
//        cv.put("Parcel_Moo","");
//        cv.put("Parcel_Tmb","");
        cv.put("status","1");

        String condition = "Parcel_ID = '"+Parcel_ID+"' and Parcel_Ser = '"+Parcel_Ser + "' and Parcel_Tmb = '"+Parcel_Tmb+"'";
        ContentValues cvResult = ShareData.getDB().Update("SurveyParcel",cv,condition,null);
        if( cvResult.getAsInteger("result") > -1 ) {
            Log.i(TAG, "--------Update success UpdateStatusSurvey ------");
        }
        else {
            Log.e(TAG, "-------- Update failed UpdateStatusSurvey --------");
        }
    }

    private void TableSurveySignBoard()
    {
        ContentValues cv = new ContentValues();
        ArrayList<SignBoardInfo> arLstSingBoardInfo = ShareData.arLstSignBoardInfo;
        if( arLstSingBoardInfo == null ) return;
        if( arLstSingBoardInfo.size() <= 0 ) return;
        DeleteImage("s");

        SignBoardInfo signBoardInfoTmp = arLstSingBoardInfo.get(0);
        ArrayList<SurveySignBoard> arLstSurveySignBoardTmb = signBoardInfoTmp.arLstSurveySignBoard;
        String Parcel_ID = arLstSurveySignBoardTmb.get(0).Parcel_ID;
        String Parcel_Ser = arLstSurveySignBoardTmb.get(0).Parcel_Ser;
        //if( z == 0 )
        {
            //DELETE
            String condition = "";
            ArrayList<SignBoardInfo> arLstSignBoardInfoTmp = DatabaseManger.getSignBoardInfo(
                    Parcel_ID,
                    Parcel_Ser,
                    "");


            for( int u=0; u < arLstSignBoardInfoTmp.size(); u++ )
            {
                ArrayList<SurveySignBoardOwner> arLstSurveyBuildingOwner = arLstSignBoardInfoTmp.get(u).arLstSurveySignBoardOwner;

                //for( int m=0; m < arLstSurveyBuildingOwner.size(); m++ )
                {
                    condition = "Sign_No = "+arLstSurveyBuildingOwner.get(0).Sign_No;
                    Delete("SurveySignBoardOwner",condition);
                }
            }

            condition = "Parcel_ID = "+Parcel_ID+" and Parcel_Ser = "+Parcel_Ser;
            Delete("SurveySignBoard",condition);
        }


        for( int i=0; i < arLstSingBoardInfo.size();i++ )
        {
            ArrayList<SurveySignBoard> arLstSurveySignBoard = arLstSingBoardInfo.get(i).arLstSurveySignBoard;
            ArrayList<SurveySignBoardOwner> arLstSurveySignBoardOwner = arLstSingBoardInfo.get(i).arLstSurveySignBoardOwner;
            if( arLstSurveySignBoard == null || arLstSurveySignBoardOwner ==null ) continue;
            if( arLstSurveySignBoard.size() <= 0 || arLstSurveySignBoardOwner.size() <= 0) continue;

            /*for (HashMap.Entry<String, PictureInfo> pic : ShareData.arSignBoardImageBitmap.entrySet()) {
                for (SurveySignBoard signBoard : arLstSurveySignBoard) {
                    String[] img = pic.getKey().split("_");
                    if (img[1].equals(signBoard.Sign_No)) {
                        if (signBoard.Parcel_ID == "") {continue;}
                        ShareData.arSignBoardNewImageBitmap.put(img[0]+"_"+signBoard.Parcel_ID,pic.getValue());
                    }
                }
            }*/

            for( int n=0; n < arLstSurveySignBoard.size(); n++ )
            {
                //if( !arLstSurveySignBoard.get(n).Sign_Type.equals("-1") )
                {
                    //insert
                    ContentValues cvResult = new ContentValues();
                    cv.put("Parcel_ID",arLstSurveySignBoard.get(n).Parcel_ID);
                    cv.put("Parcel_Ser",arLstSurveySignBoard.get(n).Parcel_Ser);
                    cv.put("Parcel_TMB",arLstSurveySignBoard.get(n).Parcel_Tmb);

                    cv.put("Sign_Type",arLstSurveySignBoard.get(n).Sign_Type);
                    cv.put("Sign_txt",arLstSurveySignBoard.get(n).Sign_txt);
                    cv.put("Sign_Width",arLstSurveySignBoard.get(n).Sign_Width);
                    cv.put("Sign_Higth",arLstSurveySignBoard.get(n).Sign_Higth);
                    cv.put("Sign_Side",arLstSurveySignBoard.get(n).Sign_Side);
                    cv.put("Sign_Tax",arLstSurveySignBoard.get(n).Sign_Tax);
                    cv.put("CreateBy",arLstSurveySignBoard.get(n).CreateBy);
                    cv.put("CreateDate",arLstSurveySignBoard.get(n).CreateDate);
                    cv.put("ModifyBy",arLstSurveySignBoard.get(n).ModifyBy);
                    cv.put("ModifyDate",arLstSurveySignBoard.get(n).ModifyDate);
                    cv.put("StatusLink",arLstSurveySignBoard.get(n).StatusLink);

                    cvResult = mDB.Insert("SurveySignBoard" , cv);
                    if( cvResult.getAsInteger("result") > -1 ) {
                        Log.i(TAG, "--------Insert success TableSurveySignBoard ------");
                        TableSurveySignBoardOwner(arLstSurveySignBoardOwner.get(n), cvResult.getAsInteger("result"));

                        if (i < ShareData.arSignBoardBitmap.size()) {
                            PictureInfo pictureInfo = ShareData.arSignBoardBitmap.get(i);
                            if (pictureInfo != null) {
                                SaveImageWithRowId(cvResult.getAsInteger("result").toString(), DetailBoardActivity.PICK_IMAGE_ID_1, pictureInfo.picture1,parcelZBL);
                                SaveImageWithRowId(cvResult.getAsInteger("result").toString(), DetailBoardActivity.PICK_IMAGE_ID_2, pictureInfo.picture2,parcelZBL);
                                SaveImageWithRowId(cvResult.getAsInteger("result").toString(), DetailBoardActivity.PICK_IMAGE_ID_3, pictureInfo.picture3,parcelZBL);
                            }
                        }
                        /*
                        for (HashMap.Entry<String,PictureInfo> obj : ShareData.arSignBoardNewImageBitmap.entrySet()) {
                            String[] key = obj.getKey().split("_");
                            if (key[1].equals(arLstSurveySignBoard.get(n).Parcel_ID)) {
                                PictureInfo pictureInfo = obj.getValue(); //ShareData.arSignBoardNewImageBitmap.get(parcelZBL + "_" + arLstSurveySignBoard.get(n).Parcel_ID);
                                if (pictureInfo != null) {
                                    SaveImageWithRowId(cvResult.getAsInteger("result").toString(), DetailBoardActivity.PICK_IMAGE_ID_1, pictureInfo.picture1,key[0]);
                                    SaveImageWithRowId(cvResult.getAsInteger("result").toString(), DetailBoardActivity.PICK_IMAGE_ID_2, pictureInfo.picture2,key[0]);
                                    SaveImageWithRowId(cvResult.getAsInteger("result").toString(), DetailBoardActivity.PICK_IMAGE_ID_3, pictureInfo.picture3,key[0]);
                                }
                            }
                        }*/
                    }
                    else {
                        Log.e(TAG, "-------- Insert failed TableSurveySignBoard --------");
                    }
                }
            }
        }

        //reset data
        ShareData.arLstSurveySignBoard = new ArrayList<SurveySignBoard>();
        ShareData.arLstSurveySignBoardOwner = new ArrayList<SurveySignBoardOwner>();
        ShareData.arLstSignBoardInfo = new ArrayList<SignBoardInfo>();
        ShareData.arSignBoardImageBitmap = new HashMap<>();
        ShareData.arSignBoardBitmap = new ArrayList<PictureInfo>();
    }

    private void TableSurveySignBoardOwner(SurveySignBoardOwner surveySignBoardOwner , int Sign_No)
    {
        ContentValues cv = new ContentValues();

        //for( int n=0; n < arLstSurveyOwner.size(); n++ )
        {
            //insert
            ContentValues cvResult = new ContentValues();
            cv.put("HN_ID",surveySignBoardOwner.HN_ID);
            cv.put("Sign_No",Sign_No);
            cv.put("FIRST_NAME",surveySignBoardOwner.FIRST_NAME);
            cv.put("LAST_NAME",surveySignBoardOwner.LAST_NAME);
            cv.put("NUM",surveySignBoardOwner.NUM);
            cv.put("MOO",surveySignBoardOwner.MOO);
            cv.put("CustomerID",surveySignBoardOwner.CustomerID);
            cvResult = mDB.Insert("SurveySignBoardOwner" , cv);
            if( cvResult.getAsInteger("result") > -1 ) {
                Log.i(TAG, "--------Insert success TableSurveySignBoardOwner ------");
            }
            else {
                Log.e(TAG, "-------- Insert failed TableSurveySignBoardOwner --------");
            }
        }
    }

    private void TableSurveyBuilding()
    {
        ContentValues cv = new ContentValues();
        ArrayList<BuildingInfo> arLstBuildingInfo = ShareData.arLstBuildingInfo;
        if( arLstBuildingInfo == null ) return;
        if( arLstBuildingInfo.size() <= 0 ) return;

        BuildingInfo buildingInfoTmp = arLstBuildingInfo.get(0);
        ArrayList<SurveyBuilding> arLstSurveyBuildingTmb = buildingInfoTmp.arLstSurveyBuilding;
        String Parcel_ID = arLstSurveyBuildingTmb.get(0).Parcel_ID;
        String Parcel_Ser = arLstSurveyBuildingTmb.get(0).Parcel_Ser;
        //if( z == 0 )
        {
            //DELETE
            DeleteImage("b");
            String condition = "";
            ArrayList<BuildingInfo> arLstBuildingInfoTmp = DatabaseManger.getBuildingInfo(
                    Parcel_ID,
                    Parcel_Ser,
                    "");

            for( int u=0; u < arLstBuildingInfoTmp.size(); u++ )
            {
                //for( int m=0; m < arLstBuildingInfoTmp.size(); m++ )
                {
                    ArrayList<SurveyBuildingOwner> arLstSurveyBuildingOwner = arLstBuildingInfoTmp.get(u).arLstSurveyBuildingOwner;
                    condition = "Build_No = "+arLstSurveyBuildingOwner.get(0).Building_No;
                    Delete("SurveyBuildingOwner",condition);
                }
            }

            condition = "Parcel_ID = "+Parcel_ID+" and Parcel_Ser = "+Parcel_Ser;
            Delete("SurveyBuilding",condition);

        }

        for( int z=0; z < arLstBuildingInfo.size(); z++ )
        {
            ContentValues cvResult = new ContentValues();
            BuildingInfo buildingInfo = arLstBuildingInfo.get(z);
            ArrayList<SurveyBuilding> arLstSurveyBuilding = buildingInfo.arLstSurveyBuilding;
            ArrayList<SurveyBuildingOwner> arLstSurveyBuildingOwner = buildingInfo.arLstSurveyBuildingOwner;

            if( buildingInfo == null ) return;
            if( arLstSurveyBuilding == null || arLstSurveyBuildingOwner == null) return;
            if( arLstSurveyBuilding.size() <= 0 || arLstSurveyBuildingOwner.size() <=0 ) return;

            for( int n=0; n < arLstSurveyBuilding.size(); n++ )
            {
                //if( !arLstSurveyBuilding.get(n).Building_Type.equals("-1") )
                {
                    //insert
                    cvResult = new ContentValues();
                    cv.put("Parcel_ID",arLstSurveyBuilding.get(n).Parcel_ID);
                    cv.put("Parcel_Ser",arLstSurveyBuilding.get(n).Parcel_Ser);
                    cv.put("Parcel_TMB",arLstSurveyBuilding.get(n).Parcel_Tmb);
                    cv.put("Building_Moo",arLstSurveyBuilding.get(n).Building_Moo);
                    cv.put("Building_Group",arLstSurveyBuilding.get(n).Building_Group);
                    cv.put("Building_Num",arLstSurveyBuilding.get(n).Building_Num);
                    cv.put("Building_Location",arLstSurveyBuilding.get(n).Building_Location);
                    cv.put("Building_Room",arLstSurveyBuilding.get(n).Building_Room);
                    cv.put("Building_Floor",arLstSurveyBuilding.get(n).Building_Floor);
                    cv.put("Building_Width",arLstSurveyBuilding.get(n).Building_Width);
                    cv.put("Building_Higth",arLstSurveyBuilding.get(n).Building_Higth);
                    cv.put("Building_UseA",arLstSurveyBuilding.get(n).Building_UseA);
                    cv.put("Building_UseB",arLstSurveyBuilding.get(n).Building_UseB);
                    cv.put("Building_UseC",arLstSurveyBuilding.get(n).Building_UseC);
                    cv.put("Building_Rent",arLstSurveyBuilding.get(n).Building_Rent);
                    cv.put("Building_Tax",arLstSurveyBuilding.get(n).Building_Tax);
                    cv.put("Building_Type",arLstSurveyBuilding.get(n).Building_Type);

                    cv.put("CreateBy",arLstSurveyBuilding.get(n).CreateBy);
                    cv.put("CreateDate",arLstSurveyBuilding.get(n).CreateDate);
                    cv.put("ModifyBy",arLstSurveyBuilding.get(n).ModifyBy);
                    cv.put("ModifyDate",arLstSurveyBuilding.get(n).ModifyDate);
                    cv.put("StatusLink",arLstSurveyBuilding.get(n).StatusLink);
                    //utils.printContentValues(cv);
                    cvResult = mDB.Insert("SurveyBuilding" , cv);
                    if( cvResult.getAsInteger("result") > -1 ) {
                        Log.i(TAG, "--------Insert success TableSurveyBuilding ------");
                        TableSurveyBuildingOwner(arLstSurveyBuildingOwner.get(n) , cvResult.getAsInteger("result"));

                        if (z < ShareData.arBuildingBitmap.size()) {
                            PictureInfo pictureInfo = ShareData.arBuildingBitmap.get(z);
                            if (pictureInfo != null) {
                                SaveImageWithRowId(cvResult.getAsInteger("result").toString(), DetailBuildingActivity.PICK_IMAGE_ID_1, pictureInfo.picture1,parcelZBL);
                                SaveImageWithRowId(cvResult.getAsInteger("result").toString(), DetailBuildingActivity.PICK_IMAGE_ID_2, pictureInfo.picture2,parcelZBL);
                                SaveImageWithRowId(cvResult.getAsInteger("result").toString(), DetailBuildingActivity.PICK_IMAGE_ID_3, pictureInfo.picture3,parcelZBL);
                            }
                        }
                    }
                    else {
                        Log.e(TAG, "-------- Insert failed TableSurveyBuilding --------");
                    }
                }
            }
        }

        //reset data
        ShareData.arLstSurveyBuilding= new ArrayList<SurveyBuilding>();
        ShareData.arLstSurveyBuildingOwner= new ArrayList<SurveyBuildingOwner>();
        ShareData.arLstBuildingInfo = new ArrayList<BuildingInfo>();
        ShareData.arBuildingImageBitmap = new HashMap<>();
        ShareData.arBuildingBitmap = new ArrayList<PictureInfo>();
    }

    private void TableSurveyBuildingOwner(SurveyBuildingOwner arLstSurveyOwner,int Sign_No)
    {
        ContentValues cv = new ContentValues();
        //ArrayList<SurveyBuildingOwner> arLstSurveyOwner = ShareData.arLstSurveyBuildingOwner;
        //for( int n=0; n < arLstSurveyOwner.size(); n++ )
//        {
            //delete by condition
//            ContentValues cvResult = new ContentValues();
//            utils.printContentValues(cv);
//            String condition = "Parcel_ID = "+arLstSurveyOwner.get(n).Parcel_ID+" and Parcel_Ser = "+arLstSurveyOwner.get(n).Parcel_Ser;
//            String condition = "Build_No = "+arLstSurveyOwner.Building_No;
//            cvResult = ShareData.getDB().Delete("SurveyBuildingOwner",condition,null);
//            if( cvResult.getAsInteger("result") > -1 ) {
//                Log.i(TAG, "--------Delete success TableSurveyBuildingOwner ------");
//            }
//            else {
//                Log.e(TAG, "-------- Delete failed TableSurveyBuildingOwner --------");
//            }
//        }

        //for( int n=0; n < arLstSurveyOwner.size(); n++ )
        {
            //insert
            ContentValues cvResult = new ContentValues();
            cv.put("Build_No",Sign_No);
            cv.put("HN_ID",arLstSurveyOwner.PrefixName);
            cv.put("FIRST_NAME",arLstSurveyOwner.FIRST_NAME);
            cv.put("LAST_NAME",arLstSurveyOwner.LAST_NAME);
            cv.put("CustomerID",arLstSurveyOwner.CustomerID);

            cvResult = mDB.Insert("SurveyBuildingOwner" , cv);
            if( cvResult.getAsInteger("result") > -1 ) {
                Log.i(TAG, "--------Insert success TableSurveyBuildingOwner ------");
            }
            else {
                Log.e(TAG, "-------- Insert failed TableSurveyBuildingOwner --------");
            }
        }
    }

    private void TableSurveyParcelOwnerUse()
    {
        ContentValues cv = new ContentValues();
        ArrayList<SurveyParcelOwner> arLstSurveyOwner = ShareData.arLstParcelOwner;
        for( int n=0; n < arLstSurveyOwner.size(); n++ )
        {
            //delete by condition
            ContentValues cvResult = new ContentValues();
            utils.printContentValues(cv);
            String condition = "Parcel_ID = "+arLstSurveyOwner.get(n).Parcel_ID+" and Parcel_Ser = "+arLstSurveyOwner.get(n).Parcel_Ser;
            cvResult = ShareData.getDB().Delete("SurveyParcelOwnerUse",condition,null);
            if( cvResult.getAsInteger("result") > -1 ) {
                Log.i(TAG, "--------Delete success TableSurveyParcelOwnerUse ------");
            }
            else {
                Log.e(TAG, "-------- Delete failed TableSurveyParcelOwnerUse --------");
            }
        }

        for( int n=0; n < arLstSurveyOwner.size(); n++ )
        {
            //insert
            ContentValues cvResult = new ContentValues();
            cv.put("Parcel_ID",arLstSurveyOwner.get(n).Parcel_ID);
            cv.put("Parcel_Ser",arLstSurveyOwner.get(n).Parcel_Ser);
            cv.put("FullName",arLstSurveyOwner.get(n).fullName);
            cv.put("Adds",arLstSurveyOwner.get(n).address);
            cv.put("CustomerID",arLstSurveyOwner.get(n).CustomerID);
            cv.put("PrefixName",arLstSurveyOwner.get(n).PrefixName);
            cv.put("FirstName",arLstSurveyOwner.get(n).FIRST_NAME);
            cv.put("LastName",arLstSurveyOwner.get(n).LAST_NAME);

            utils.printContentValues(cv);
            cvResult = mDB.Insert("SurveyParcelOwnerUse" , cv);
            if( cvResult.getAsInteger("result") > -1 ) {
                Log.i(TAG, "--------Insert success TableSurveyParcelOwnerUse ------");
            }
            else {
                Log.e(TAG, "-------- Insert failed TableSurveyParcelOwnerUse --------");
            }
        }

        //reset data
        ShareData.arLstParcelOwner = new ArrayList<SurveyParcelOwner>();
    }
    private void TableSurveyParcelLandUse()
    {
        ContentValues cv = new ContentValues();
        ArrayList<LandUse> arLstLandUse= ShareData.arLstLandUse;
        if( arLstLandUse == null ) return;
        if( arLstLandUse.size() <= 0 ) return;

        ContentValues cvResult = new ContentValues();
        String condition = String.format("Parcel_ID = '%s' and Parcel_Ser = '%s' and Parcel_Tmb = '%s'"
                ,arLstLandUse.get(0).Parcel_ID
                ,arLstLandUse.get(0).Parcel_Ser
                ,arLstLandUse.get(0).Parcel_Tmb);

        cvResult = mDB.Delete("SurveyParcelLandUse" , condition,null);
        if( cvResult.getAsInteger("result") > -1 )
        {
            //delete success
            for( int n=0; n < arLstLandUse.size(); n++ )
            {
                cvResult = new ContentValues();
                cv.put("Parcel_ID",arLstLandUse.get(n).Parcel_ID);
                cv.put("Parcel_Ser",arLstLandUse.get(n).Parcel_Ser);
                cv.put("Parcel_Tmb",arLstLandUse.get(n).Parcel_Tmb);
                cv.put("Parcel_Use",arLstLandUse.get(n).typeLandUse);
                cv.put("Parcel_Rai",arLstLandUse.get(n).rai);
                cv.put("Parcel_Nang",arLstLandUse.get(n).nang);
                cv.put("Parcel_Va",arLstLandUse.get(n).va);
                cv.put("Parcel_Tax",arLstLandUse.get(n).Parcel_Tax);

                utils.printContentValues(cv);
                Log.e( TAG,"condition = "+condition );

                if( !arLstLandUse.get(n).typeLandUse.equals("-1") )
                {
                    cvResult = mDB.Insert("SurveyParcelLandUse" , cv);
                    if( cvResult.getAsInteger("result") > -1 )
                    {
                        Log.i(TAG , "--------insert success TableSurveyParcelLandUse ------");
                    }
                    else
                    {
                        Log.e(TAG , "-------- insert failed TableSurveyParcelLandUse --------");
                    }
                }
            }
        }
    }

    private boolean Delete(String table , String condition)
    {
        ContentValues cvResult = new ContentValues();
        cvResult = ShareData.getDB().Delete(table , condition , null);
        if( cvResult.getAsInteger("result") > -1  )
        {
            Log.i(TAG , "--------delete success "+table+"------");
            return true;
        }
        else
        {
            Log.i(TAG , "--------delete failed "+table+"------");
            return false;
        }
    }

    class MyPageAdapter extends FragmentPagerAdapter {

        private List<Fragment> fragments;
        private String tabTitles[] = new String[] { "รายละเอียดทรัพย์สิน", "ข้อมูลโรงเรือน", "ข้อมูลป้าย" };

        public MyPageAdapter(FragmentManager fm, List<Fragment> fragments) {

            super(fm);

            this.fragments = fragments;
        }

        @Override
        public Fragment getItem(int position) {
                return this.fragments.get(position);
        }


        @Override
        public int getCount() {

            return 3;

        }

        @Override
        public CharSequence getPageTitle(int position) {
            // Generate title based on item position
            return tabTitles[position];
        }

    }

    protected class ProcessSave extends AsyncTask<Boolean, Void, Boolean> {

        ProgressDialog pDialog = null;
        @Override
        protected void onPostExecute(Boolean result) {
            save();
            goNextOrPrev(result);
            pDialog.dismiss();
        }

        @Override
        protected Boolean doInBackground(Boolean... params) {
            return params[0];
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("กรุณารอสักครู่");
            pDialog.setCancelable(false);
            pDialog.show();
        }
    }
}
