package com.survay;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.Object.ShareData;
import com.Object.utils;
import com.ObjectDatabase.DatabaseManger;
import com.UICustomize.ImagePicker;
import com.model.LandInfo;
import com.model.LandUse;
import com.model.SurveyParcelOwner;

import java.util.ArrayList;
import java.util.Map;

public class DetailParcelFragment extends Fragment {

    private static final String ARG_ZBL = "zbl";
    private static final int PICK_IMAGE_ID_1 = 131; // the number doesn't matter
    private static final int PICK_IMAGE_ID_2 = 132; // the number doesn't matter
    private static final int PICK_IMAGE_ID_3 = 133; // the number doesn't matter
    private static final String PARCEL_ID = "Parcel_Id"; // the number doesn't matter
    private static final String PARCEL_SER = "Parcel_Ser"; // the number doesn't matter
    private static final String PARCEL_TMB = "Parcel_Tmb"; // the number doesn't matter
    private static final String TAG = "DetailParcelFragment"; // the number doesn't matter

    // TODO: Rename and change types of parameters
    private String parcelZBL;
    private Boolean isCreated = false;

    private View vRoot = null;
    private View vParcelOwner = null;
    private View vLayoutInfoLand = null;
    private View vLayoutInfoParcel = null;

    private EditText edtLandScripType = null;
    private EditText edtParcelId = null;
    private EditText edtParcelNum = null;
    private EditText edtParcelSer = null;
    private EditText edtParcelRai = null;
    private EditText edtParcelNang = null;
    private EditText edtParcelVa = null;
    private EditText edtPrefix = null;
    private EditText edtftName = null;
    private EditText edtlstName = null;
    private EditText edtZBL = null;
    private EditText edtMoo = null;
    private EditText edtParcelTmb = null;

    private TextView tvAddLandUse = null;
    private TextView tvAddParcel = null;
    private TextView tvAddParcelOwner = null;

    private LinearLayout llParcelOwner = null;
    private LinearLayout llParcelBenefitLand = null;
    private LinearLayout llParcelLandUse = null;
    private LayoutInflater inflater = null;

    private ArrayList<LandInfo> arLstLandInfo = null;
    private SurveyParcelOwner surveyParcelOwner = null;

    private Switch swUse = null;
    private Switch swRent = null;

    private ImageView iv1 = null;
    private ImageView iv2 = null;
    private ImageView iv3 = null;

    private Button btnDelete1 = null;
    private Button btnDelete2 = null;
    private Button btnDelete3 = null;

    private DatabaseManger dbManager = null;

    private CheckBox chkLandUse = null;


    private Activity mActivity = null;

    private Dialog dlgAddLandUse = null;
    private Dialog dlgAddParcelOwner = null;
    private Dialog dlgAddParcelUse = null;

    public DetailParcelFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static DetailParcelFragment newInstance(String param1) {

        DetailParcelFragment fragment = new DetailParcelFragment();
        Bundle args = new Bundle();
        args.putString(ARG_ZBL, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            parcelZBL = getArguments().getString(ARG_ZBL);
            mActivity = getActivity();
        }

        ShareData.arLstLandUse= new ArrayList<LandUse>();
        ShareData.arLstParcelOwner = new ArrayList<SurveyParcelOwner>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.inflater = inflater;
        vRoot = inflater.inflate(R.layout.fragment_detail_parcel, container, false);

        initial();
        initialValue();
        initialEvent();

        return vRoot;
    }

    private void initial()
    {
        vParcelOwner = (View)vRoot.findViewById(R.id.layout_parcel_owner);
        vLayoutInfoLand = (View)vRoot.findViewById(R.id.layout_info_land);
        vLayoutInfoParcel = (View)vRoot.findViewById(R.id.layout_info_parcel);

        edtLandScripType = (EditText)vRoot.findViewById(R.id.edtLandScripType);
        edtParcelId = (EditText)vRoot.findViewById(R.id.edtParcelId);
        edtParcelNum = (EditText)vRoot.findViewById(R.id.edtParcelNum);
        edtParcelSer = (EditText)vRoot.findViewById(R.id.edtParcelSer);
        edtParcelRai = (EditText)vRoot.findViewById(R.id.edtParcelRai);
        edtParcelNang = (EditText)vRoot.findViewById(R.id.edtParcelNang);
        edtParcelVa = (EditText)vRoot.findViewById(R.id.edtParcelVa);
        edtPrefix = (EditText)vRoot.findViewById(R.id.edtPrefix);
        edtftName = (EditText)vRoot.findViewById(R.id.edtftName);
        edtlstName = (EditText)vRoot.findViewById(R.id.edtlstName);

        edtZBL = (EditText)vLayoutInfoParcel.findViewById(R.id.edtZBL);
        edtMoo = (EditText)vLayoutInfoParcel.findViewById(R.id.edtMoo);
        edtParcelTmb = (EditText)vLayoutInfoParcel.findViewById(R.id.edtParcelTmb);
        swRent = (Switch)vLayoutInfoParcel.findViewById(R.id.swRent);
        swUse = (Switch)vLayoutInfoParcel.findViewById(R.id.swUse);

        tvAddLandUse = (TextView)vRoot.findViewById(R.id.tvAddLandUse);
        tvAddParcel = (TextView)vRoot.findViewById(R.id.tvAddParcel);
        tvAddParcelOwner = (TextView)vRoot.findViewById(R.id.tvParcelOwner);

        iv1 = (ImageView)vRoot.findViewById(R.id.iv1);
        iv2 = (ImageView)vRoot.findViewById(R.id.iv2);
        iv3 = (ImageView)vRoot.findViewById(R.id.iv3);

        btnDelete1 = (Button)vRoot.findViewById(R.id.btnDelete1);
        btnDelete2 = (Button)vRoot.findViewById(R.id.btnDelete2);
        btnDelete3 = (Button)vRoot.findViewById(R.id.btnDelete3);

        llParcelLandUse = (LinearLayout)vRoot.findViewById(R.id.llParcelLandUse);
        llParcelBenefitLand = (LinearLayout)vRoot.findViewById(R.id.llParcelBenefitLand);
        llParcelBenefitLand.setVisibility(View.VISIBLE);
        llParcelLandUse.setVisibility(View.VISIBLE);

    }
    private void initialValue()
    {
        if(TextUtils.isEmpty(parcelZBL)) return;

        dbManager = new DatabaseManger();
        arLstLandInfo = dbManager.getLandInfo(parcelZBL);
        if( arLstLandInfo != null )
        {
            if( arLstLandInfo.size() <= 0 )
            {
                utils.createPopup(mActivity, "ไม่พบข้อมูล", "", "ตกลง", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mActivity.finish();
                    }
                });
                return ;
            }

            LandInfo landInfo = arLstLandInfo.get(0);
            if( landInfo != null )
            {
                edtLandScripType.setText(landInfo.LandScripType);
                edtParcelId.setText(landInfo.Parcel_Id);
                edtParcelNum.setText(landInfo.Parcel_Num);
                edtParcelSer.setText(landInfo.Parcel_Ser);
                edtParcelRai.setText(landInfo.Parcel_Rai);
                edtParcelNang.setText(landInfo.Parcel_Nang);
                edtParcelVa.setText(landInfo.Parcel_Va);
                edtZBL.setText(landInfo.ZBL);
                edtMoo.setText(landInfo.Moo);
                edtParcelTmb.setText(DatabaseManger.getNameDisSub(landInfo.Tmb));

                initialParcelOwner();
                initialSurveyParcelOwnerUse();
                initialParcelLandUse();
            }
        }

        btnDelete1.setEnabled(false);
        btnDelete2.setEnabled(false);
        btnDelete3.setEnabled(false);

        if (ShareData.arMapImageBitmap.get(PICK_IMAGE_ID_1) != null) {
            iv1.setImageBitmap(ShareData.arMapImageBitmap.get(PICK_IMAGE_ID_1));
            btnDelete1.setEnabled(false);
        }
        if (ShareData.arMapImageBitmap.get(PICK_IMAGE_ID_2) != null) {
            iv2.setImageBitmap(ShareData.arMapImageBitmap.get(PICK_IMAGE_ID_2));
            btnDelete2.setEnabled(false);
        }
        if (ShareData.arMapImageBitmap.get(PICK_IMAGE_ID_3) != null) {
            iv3.setImageBitmap(ShareData.arMapImageBitmap.get(PICK_IMAGE_ID_3));
            btnDelete3.setEnabled(false);
        }
    }

    private void initialSurveyParcelOwnerUse()
    {
        if( dbManager == null || arLstLandInfo==null ) return;
        if( arLstLandInfo.size() <= 0 ) return;

        ArrayList<SurveyParcelOwner> arLstSurveyParcelOwner = dbManager.getSurveyParcelOwnerUse(getLandInfo().getAsString(PARCEL_ID),getLandInfo().getAsString(PARCEL_SER));
        for( int n=0;n<arLstSurveyParcelOwner.size();n++ )
        {
            AddParcelBenefitLand(arLstSurveyParcelOwner.get(n) , "");
        }
    }

    private void initialParcelOwner()
    {
        if( dbManager == null || arLstLandInfo==null ) return;
        if( arLstLandInfo.size() <= 0 ) return;

        ArrayList<SurveyParcelOwner> arLstParcelOwner = dbManager.getSurveyParcelOwner
                (getLandInfo().getAsString(PARCEL_ID)
                ,getLandInfo().getAsString(PARCEL_SER));

        if( arLstParcelOwner == null ) return;
        if( arLstParcelOwner.size() <= 0 ) return;

        surveyParcelOwner = arLstParcelOwner.get(0);

        addParcelOwner(surveyParcelOwner , "owner");
    }
    private void initialParcelLandUse()
    {
        if( dbManager == null || arLstLandInfo==null ) return;
        if( arLstLandInfo.size() <= 0 ) return;

//        ArrayList<String> arLstIdDisSub = dbManager.getIdDisSub(arLstLandInfo.get(0).Tmb);
//        if( arLstIdDisSub.size() <= 0 ) return;

        if (ShareData.arLstLandUse.size() == 0) {
            ArrayList<LandUse> arLstUse = dbManager.getSurveyParcelLandUse(getLandInfo().getAsString(PARCEL_ID)
                    , getLandInfo().getAsString(PARCEL_SER)
                    , getLandInfo().getAsString(PARCEL_TMB));

            ShareData.arLstLandUse = arLstUse;
        }
        addLandUse();
    }

    private void removeParcelBenefitLand(int index)
    {
        if( llParcelBenefitLand == null ) return;
        if( llParcelBenefitLand.getChildCount() <= 0 ) return;
        if( (llParcelBenefitLand.getChildCount()-1) < index ) return;
        if( (ShareData.arLstParcelOwner.size() < index )) return;

        //set CheckBox = false
        if( ShareData.arLstParcelOwner.get(index).CustomerID.equals(surveyParcelOwner.CustomerID))
        {
            {
                chkLandUse.setOnCheckedChangeListener(null);
                chkLandUse.setChecked(false);
                chkLandUse.setOnCheckedChangeListener(onCheckChange);
            }
        }

        llParcelBenefitLand.removeViewAt(index);
        ShareData.arLstParcelOwner.remove(index);
    }
    private void AddParcelBenefitLand(SurveyParcelOwner surveyParcelOwner , String tag)
    {
        if( dbManager == null || arLstLandInfo==null ) return;
        if( arLstLandInfo.size() <= 0 ) return;

        final View vParcelOwner = inflater.inflate(R.layout.layout_parcel_owner,null);
        vParcelOwner.setTag(tag);
        vParcelOwner.setVisibility(View.VISIBLE);

        Button btnDelete = (Button)vParcelOwner.findViewById(R.id.btnDelete);
        TextView tvFullName = (TextView)vParcelOwner.findViewById(R.id.tvFullName);
        TextView tvAddress = (TextView)vParcelOwner.findViewById(R.id.tvAddress);
        TextView tvTopic = (TextView)vParcelOwner.findViewById(R.id.tvTopic);
        View vTopic = (View)vParcelOwner.findViewById(R.id.vTopic);
        View vSpace = (View)vParcelOwner.findViewById(R.id.vSpace);
        CheckBox chkLandUse = (CheckBox) vParcelOwner.findViewById(R.id.chkLandUse);

        chkLandUse.setVisibility(View.GONE);
        tvTopic.setVisibility(View.GONE);
        vTopic.setVisibility(View.GONE);
        vSpace.setVisibility(View.GONE);

        tvFullName.setText(surveyParcelOwner.fullName);
        tvAddress.setText(surveyParcelOwner.address);

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LinearLayout llRootParcelOwner = (LinearLayout) vParcelOwner.findViewById(R.id.llRootParcelOwner);
                int index = ((ViewGroup) llRootParcelOwner.getParent()).indexOfChild(llRootParcelOwner);

                removeParcelBenefitLand(index);
            }
        });

        llParcelBenefitLand.addView(vParcelOwner);
        ShareData.arLstParcelOwner.add(surveyParcelOwner);
    }

    private void initialEvent()
    {
        swRent.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

            }
        });
        swUse.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

            }
        });

        tvAddLandUse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogAddLandUse(-1);
            }
        });
        tvAddParcel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogAddParcel();
            }
        });
//        tvAddParcelOwner.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showDialogAddParcelOwner();
//            }
//        });

        iv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent chooseImageIntent = ImagePicker.getPickImageIntent(mActivity);
                startActivityForResult(chooseImageIntent, PICK_IMAGE_ID_1);
            }
        });

        iv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent chooseImageIntent = ImagePicker.getPickImageIntent(mActivity);
                startActivityForResult(chooseImageIntent, PICK_IMAGE_ID_2);
            }
        });
        iv3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent chooseImageIntent = ImagePicker.getPickImageIntent(mActivity);
                startActivityForResult(chooseImageIntent, PICK_IMAGE_ID_3);
            }
        });

        btnDelete1.setOnClickListener(onDeleteImage);
        btnDelete2.setOnClickListener(onDeleteImage);
        btnDelete3.setOnClickListener(onDeleteImage);
    }

    private View.OnClickListener onDeleteImage = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if( v.getId() == R.id.btnDelete1 )
                dialogDeleteImage(v.getId());
            else if( v.getId() == R.id.btnDelete2 )
                dialogDeleteImage(v.getId());
            else if( v.getId() == R.id.btnDelete3 )
                dialogDeleteImage(v.getId());
        }
    };
    private void dialogDeleteImage(final int Id)
    {
        utils.createPopup(mActivity, "คุณต้องการลบรูป ใช่หรือไม่"
                , ""
                , "ใช่"
                , "ไม่ใช่"
                , new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteImage(Id);
                    }
                }
                , new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
    }
    private void deleteImage(int Id)
    {
        for (Map.Entry<Integer, Bitmap> entry : ShareData.arMapImageBitmap.entrySet()) {
            if (entry.getKey() > 300) {
                if( Id == R.id.btnDelete1 )
                {
                    if( entry.getKey() == PICK_IMAGE_ID_1 )
                    {
                        ShareData.arMapImageBitmap.remove(entry);
                        iv1.setImageDrawable(getResources().getDrawable(R.drawable.image_placeholder));
                        btnDelete1.setEnabled(false);
                    }
                }
                else if( Id == R.id.btnDelete2 )
                {
                    if( entry.getKey() == PICK_IMAGE_ID_2 )
                    {
                        ShareData.arMapImageBitmap.remove(entry);
                        iv2.setImageDrawable(getResources().getDrawable(R.drawable.image_placeholder));
                        btnDelete2.setEnabled(false);
                    }
                }
                else if( Id == R.id.btnDelete3 )
                {
                    if( entry.getKey() == PICK_IMAGE_ID_3 )
                    {
                        ShareData.arMapImageBitmap.remove(entry);
                        iv3.setImageDrawable(getResources().getDrawable(R.drawable.image_placeholder));
                        btnDelete3.setEnabled(false);
                    }
                }
            } else if (entry.getKey() > 200) {
                if( Id == R.id.btnDelete1 )
                {
                    if( entry.getKey() == PICK_IMAGE_ID_1 )
                    {
                        ShareData.arMapImageBitmap.remove(entry);
                        iv1.setImageDrawable(getResources().getDrawable(R.drawable.image_placeholder));
                        btnDelete1.setEnabled(false);
                    }
                }
                else if( Id == R.id.btnDelete2 )
                {
                    if( entry.getKey() == PICK_IMAGE_ID_2 )
                    {
                        ShareData.arMapImageBitmap.remove(entry);
                        iv2.setImageDrawable(getResources().getDrawable(R.drawable.image_placeholder));
                        btnDelete2.setEnabled(false);
                    }
                }
                else if( Id == R.id.btnDelete3 )
                {
                    if( entry.getKey() == PICK_IMAGE_ID_3 )
                    {
                        ShareData.arMapImageBitmap.remove(entry);
                        iv3.setImageDrawable(getResources().getDrawable(R.drawable.image_placeholder));
                        btnDelete3.setEnabled(false);
                    }
                }
            } else if (entry.getKey() > 100) {
                if( Id == R.id.btnDelete1 )
                {
                    if( entry.getKey() == PICK_IMAGE_ID_1 )
                    {
                        ShareData.arMapImageBitmap.remove(entry);
                        iv1.setImageDrawable(getResources().getDrawable(R.drawable.image_placeholder));
                        btnDelete1.setEnabled(false);
                    }
                }
                else if( Id == R.id.btnDelete2 )
                {
                    if( entry.getKey() == PICK_IMAGE_ID_2 )
                    {
                        ShareData.arMapImageBitmap.remove(entry);
                        iv2.setImageDrawable(getResources().getDrawable(R.drawable.image_placeholder));
                        btnDelete2.setEnabled(false);
                    }
                }
                else if( Id == R.id.btnDelete3 )
                {
                    if( entry.getKey() == PICK_IMAGE_ID_3 )
                    {
                        ShareData.arMapImageBitmap.remove(entry);
                        iv3.setImageDrawable(getResources().getDrawable(R.drawable.image_placeholder));
                        btnDelete3.setEnabled(false);
                    }
                }
            }
        }
    }

    private void showDialogAddParcel()
    {
        dlgAddParcelUse = new Dialog(mActivity);
        dlgAddParcelUse.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dlgAddParcelUse.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dlgAddParcelUse.setContentView(R.layout.dlg_add_parcel_use);
        dlgAddParcelUse.setCancelable(true);

        final AutoCompleteTextView edtPrefix = (AutoCompleteTextView)dlgAddParcelUse.findViewById(R.id.edtPrefix);
        final AutoCompleteTextView edtftName = (AutoCompleteTextView)dlgAddParcelUse.findViewById(R.id.edtftName);
        final AutoCompleteTextView edtlstName = (AutoCompleteTextView)dlgAddParcelUse.findViewById(R.id.edtlstName);
        final EditText edtBuildingNum = (EditText)dlgAddParcelUse.findViewById(R.id.edtBuildingNum);
        final TextView tvTopic = (TextView)dlgAddParcelUse.findViewById(R.id.tvTopic);

        tvTopic.setText("เพิ่มผู้ทำประโยชน์ที่ดิน");

        ArrayAdapter<String> adapter = DatabaseManger.getParcelOwnerAdapter(mActivity);
        edtftName.setAdapter(adapter);
//        edtPrefix.setAdapter(adapter);
        edtlstName.setAdapter(adapter);

        Button btnSave = (Button)dlgAddParcelUse.findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if( TextUtils.isEmpty(edtPrefix.getText().toString()) ||
                        TextUtils.isEmpty(edtftName.getText().toString()) ||
                        TextUtils.isEmpty(edtlstName.getText().toString()) ||
                        TextUtils.isEmpty(edtBuildingNum.getText().toString())  )
                {
                    utils.createPopup(mActivity , "กรุณาระบุข้อมูลให้ครบ" , "");
                    return;
                }

                String prefix = edtPrefix.getText().toString();
                String ftName = edtftName.getText().toString();
                String lstName = edtlstName.getText().toString();
                String buildNum = edtBuildingNum.getText().toString();

                SurveyParcelOwner benefitLand = new SurveyParcelOwner();
                benefitLand.CustomerID = buildNum;
                benefitLand.address = buildNum;
                benefitLand.PrefixName = prefix;
                benefitLand.FIRST_NAME = ftName;
                benefitLand.LAST_NAME = lstName ;
                benefitLand.fullName = prefix+" "+ftName+" "+lstName;
                benefitLand.Parcel_ID = getLandInfo().getAsString(PARCEL_ID);
                benefitLand.Parcel_Ser = getLandInfo().getAsString(PARCEL_SER);

                AddParcelBenefitLand(benefitLand,"");

                dlgAddParcelUse.dismiss();
            }
        });
        Button btnCancel = (Button)dlgAddParcelUse.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dlgAddParcelUse.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dlgAddParcelUse.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dlgAddParcelUse.show();
        dlgAddParcelUse.getWindow().setAttributes(lp);
    }

    private void showDialogAddLandUse(final int index)
    {
        dlgAddLandUse = new Dialog(mActivity);
        dlgAddLandUse.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dlgAddLandUse.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dlgAddLandUse.setContentView(R.layout.dlg_add_land_use);
        dlgAddLandUse.setCancelable(true);

        final EditText edtParcelRai = (EditText)dlgAddLandUse.findViewById(R.id.edtParcelRai);
        final EditText edtParcelNang = (EditText)dlgAddLandUse.findViewById(R.id.edtParcelNang);
        final EditText edtParcelVa = (EditText)dlgAddLandUse.findViewById(R.id.edtParcelVa);
        final Spinner spnTypeLandUse = (Spinner) dlgAddLandUse.findViewById(R.id.spnTypeLandUse);
        final ArrayAdapter<String> arAdapter = dbManager.getAdapter(mActivity,dbManager.getTypeLanduse());
        spnTypeLandUse.setAdapter( arAdapter );
        spnTypeLandUse.setSelection(0);

        if( index > -1 )
        {
            edtParcelRai.setText(ShareData.arLstLandUse.get(index).rai);
            edtParcelNang.setText(ShareData.arLstLandUse.get(index).nang);
            edtParcelVa.setText(ShareData.arLstLandUse.get(index).va);

            for( int n=0;n<arAdapter.getCount();n++ )
            {
                if( arAdapter.getItem(n).equals(ShareData.arLstLandUse.get(index).landUseName) )
                {
                    spnTypeLandUse.setSelection(n);
                }
            }
        }
        else
        {

            if( arLstLandInfo != null )
            {
                if( arLstLandInfo.size() > 0 )
                {
                    edtParcelRai.setText(arLstLandInfo.get(0).Parcel_Rai);
                    edtParcelNang.setText(arLstLandInfo.get(0).Parcel_Nang);
                    edtParcelVa.setText(arLstLandInfo.get(0).Parcel_Va);
                }
            }
        }

        Button btnSave = (Button)dlgAddLandUse.findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if( TextUtils.isEmpty(edtParcelRai.getText().toString()) ||
                        TextUtils.isEmpty(edtParcelNang.getText().toString()) ||
                        TextUtils.isEmpty(edtParcelVa.getText().toString()) )
                {
                    utils.createPopup(mActivity , "กรุณาระบุข้อมูลให้ครบ" , "");
                    return;
                }

                if( index > -1 )
                {
                    //Update
                    LandUse landUse = ShareData.arLstLandUse.get(index);
                    landUse.typeLandUse = DatabaseManger.getIdTypeLandUse(spnTypeLandUse.getSelectedItem().toString());
                    landUse.landUseName = spnTypeLandUse.getSelectedItem().toString();
                    landUse.rai = edtParcelRai.getText().toString();
                    landUse.nang = edtParcelNang.getText().toString();
                    landUse.va = edtParcelVa.getText().toString();
                    landUse.Parcel_ID = getLandInfo().getAsString(PARCEL_ID);
                    landUse.Parcel_Ser = getLandInfo().getAsString(PARCEL_SER);
                    ShareData.arLstLandUse.set(index , landUse);
                }
                else
                {
                    //Insert
                    LandUse landUse = new LandUse();
                    landUse.typeLandUse = DatabaseManger.getIdTypeLandUse(spnTypeLandUse.getSelectedItem().toString());
                    landUse.landUseName = spnTypeLandUse.getSelectedItem().toString();
                    landUse.rai = edtParcelRai.getText().toString();
                    landUse.nang = edtParcelNang.getText().toString();
                    landUse.va = edtParcelVa.getText().toString();
                    landUse.Parcel_ID = getLandInfo().getAsString(PARCEL_ID);
                    landUse.Parcel_Ser = getLandInfo().getAsString(PARCEL_SER);
                    landUse.Parcel_Tmb = getLandInfo().getAsString(PARCEL_TMB);
                    ShareData.arLstLandUse.add(landUse);
                }

                removeLandUse();
                addLandUse();

                dlgAddLandUse.dismiss();
            }
        });
        Button btnCancel = (Button)dlgAddLandUse.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dlgAddLandUse.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dlgAddLandUse.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dlgAddLandUse.show();
        dlgAddLandUse.getWindow().setAttributes(lp);
    }

    private ContentValues getLandInfo()
    {
        ContentValues cv = new ContentValues();
        if( arLstLandInfo.size() > 0 )
        {
            cv.put(PARCEL_ID,arLstLandInfo.get(0).Parcel_Id);
            cv.put(PARCEL_SER,arLstLandInfo.get(0).Parcel_Ser);
            cv.put(PARCEL_TMB,arLstLandInfo.get(0).Tmb);
        }
        else {
            cv.put(PARCEL_ID,"");
            cv.put(PARCEL_SER,"");
            cv.put(PARCEL_TMB,"");
        }

        return cv;
    }

    private void addParcelOwner(final SurveyParcelOwner surveyParcelOwner , final String tag)
    {
        Button btnEdit = (Button)vParcelOwner.findViewById(R.id.btnEdit);
        btnEdit.setVisibility(View.GONE);
        Button btnDelete = (Button)vParcelOwner.findViewById(R.id.btnDelete);
        btnDelete.setVisibility(View.GONE);

        TextView tvFullName = (TextView)vParcelOwner.findViewById(R.id.tvFullName);
        TextView tvAddress = (TextView)vParcelOwner.findViewById(R.id.tvAddress);

        chkLandUse = (CheckBox) vParcelOwner.findViewById(R.id.chkLandUse);
        chkLandUse.setOnCheckedChangeListener(onCheckChange);

        tvFullName.setText(surveyParcelOwner.fullName);
        tvAddress.setText(surveyParcelOwner.address);
    }

    private CompoundButton.OnCheckedChangeListener onCheckChange = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if( isChecked )
            {
                AddParcelBenefitLand(surveyParcelOwner , "");
            }
            else
            {
                LinearLayout llRootParcelOwner = (LinearLayout) vParcelOwner.findViewById(R.id.llRootParcelOwner);
                int index = ((ViewGroup) llRootParcelOwner.getParent()).indexOfChild(llRootParcelOwner);

                removeParcelBenefitLand(index);
            }
        }
    };
    private void removeLandUse()
    {
        llParcelLandUse.removeAllViews();
    }
    private void addLandUse()
    {
        for( int n=0;n<ShareData.arLstLandUse.size(); n++ )
        {
            LandUse landUse = ShareData.arLstLandUse.get(n);

            View vLandUse = inflater.inflate(R.layout.layout_land_use,null);
            if( landUse.typeLandUse.equals("-1") )
            {
                vLandUse.setVisibility(View.GONE);
            }
            else vLandUse.setVisibility(View.VISIBLE);

            Button btnDelete = (Button)vLandUse.findViewById(R.id.btnDelete);
            Button btnEdit = (Button)vLandUse.findViewById(R.id.btnEdit);

            TextView tvDetailLandUse = (TextView)vLandUse.findViewById(R.id.tvDetailLandUse);
            TextView tvTypeLandUse = (TextView)vLandUse.findViewById(R.id.tvTypeLandUse);

            String rai = "";
            String nang = "";
            String va = "";

            if( !TextUtils.isEmpty(landUse.rai) )
                rai += landUse.rai+" ไร่ ";

            if( !TextUtils.isEmpty(landUse.nang) )
                nang += landUse.nang+" งา ";

            if( !TextUtils.isEmpty(landUse.va) )
                va += landUse.va+" วา ";

            tvDetailLandUse.setText(rai + nang +va );
            tvTypeLandUse.setText(landUse.landUseName );
            btnDelete.setTag(n);
            btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(v.getTag() != null)
                    {
                        final int nIndex = Integer.valueOf(v.getTag().toString());
                        final LandUse landUse = ShareData.arLstLandUse.get(nIndex);
                        String Msg = "คุณต้องการลบ การทำประโยชน์ที่ดิน'"+landUse.landUseName+"' " +
                                "ใช่หรือไม่ ?";
                        utils.createPopup(mActivity, Msg, "", "ใช่", "ไม่ใช่",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        landUse.rai = "0";
                                        landUse.va = "0";
                                        landUse.nang = "0";
                                        landUse.typeLandUse = "-1";

                                        ShareData.arLstLandUse.set(nIndex,landUse);
                                        llParcelLandUse.getChildAt(nIndex).setVisibility(View.GONE);
                                    }
                                },
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                    }
                }
            });
            btnEdit.setTag(n);
            btnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(v.getTag() != null)
                    {
                        int nIndex = Integer.valueOf(v.getTag().toString());
                        showDialogAddLandUse(nIndex);
                    }
                }
            });
            llParcelLandUse.addView(vLandUse);
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Bitmap bmp = null;
        switch(requestCode) {
            case PICK_IMAGE_ID_1:
                bmp = ImagePicker.getImageFromResult(mActivity, resultCode, data);

                if( bmp != null && !bmp.toString().equals("")) {
                    iv1.setImageBitmap(bmp);
                    ShareData.arMapImageBitmap.put(PICK_IMAGE_ID_1,bmp);
                }
                break;
            case PICK_IMAGE_ID_2:
                bmp = ImagePicker.getImageFromResult(mActivity, resultCode, data);

                if( bmp != null && !bmp.toString().equals("")) {
                    iv2.setImageBitmap(bmp);
                    ShareData.arMapImageBitmap.put(PICK_IMAGE_ID_2,bmp);
                }
                break;
            case PICK_IMAGE_ID_3:
                bmp = ImagePicker.getImageFromResult(mActivity, resultCode, data);

                if( bmp != null && !bmp.toString().equals("")) {
                    iv3.setImageBitmap(bmp);
                    ShareData.arMapImageBitmap.put(PICK_IMAGE_ID_3,bmp);
                }
                break;

            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }
}
