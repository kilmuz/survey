package com.survay;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.Object.DetailParcelLandInfo;
import com.Object.ShareData;
import com.Object.utils;
import com.ObjectDatabase.DatabaseManger;
import com.UICustomize.ImagePicker;
import com.google.gson.Gson;
import com.model.BuildingInfo;
import com.model.LandInfo;
import com.model.PictureInfo;
import com.model.SurveyBuilding;
import com.model.SurveyBuildingOwner;
import com.model.SurveyParcelOwner;

import java.util.ArrayList;
import java.util.Map;

public class DetailBuildingActivity extends BaseOnActivity {
    private static final String ARG_ZBL = "zbl";
    private static final String ARG_MODE = "mode";
    private static final String ARG_INDEX = "index";
    private static final String ARG_BUILDING_INFO = "building_info";
    public static final int PICK_IMAGE_ID_1 = 231; // the number doesn't matter
    public static final int PICK_IMAGE_ID_2 = 232; // the number doesn't matter
    public static final int PICK_IMAGE_ID_3 = 233; // the number doesn't matter
    private static final String PARCEL_ID = "Parcel_Id"; // the number doesn't matter
    private static final String PARCEL_SER = "Parcel_Ser"; // the number doesn't matter
    private static final String PARCEL_TMB = "Parcel_Tmb"; // the number doesn't matter
    private static final String PARCEL_MOO = "Parcel_Moo"; // the number doesn't matter
    private static final String TAG_OWNER = "owner"; // the number doesn't matter

    // TODO: Rename and change types of parameters
    private String parcelZBL;
    private boolean isEdit = false;
    private int mIndex = -1;

    private View vParcelOwner = null;
    private View vLayoutInfoLand = null;
    private View vLayoutInfoParcel = null;

    private EditText edtLandScripType = null;
    private EditText edtParcelId = null;
    private EditText edtParcelNum = null;
    private EditText edtParcelSer = null;
    private EditText edtParcelRai = null;
    private EditText edtParcelNang = null;
    private EditText edtParcelVa = null;
    private EditText edtPrefix = null;
    private EditText edtftName = null;
    private EditText edtlstName = null;
    private EditText edtZBL = null;
    private EditText edtMoo = null;
    private EditText edtParcelTmb = null;
    private EditText edtHouseId = null;
    private EditText edtBuildNo = null;
    private EditText edtBuildingNumFloor = null;
    private EditText edtBuildingNumRoom = null;
    private EditText edtBuildingWidth = null;
    private EditText edtBuildingHeight = null;
    private EditText edtBuildingRent = null;
    private EditText edtBuildingMonth = null;
    private EditText edtBuildingYear = null;

    private RadioGroup radioTypeBuilding;

    private Spinner spnTypeBuilding = null;

    private TextView tvAddLandUse = null;
    private TextView tvAddParcel = null;
    private TextView tvAddParcelOwner = null;

    private LinearLayout llLandInfo = null;
    private LinearLayout llParcelOwner = null;
    private LinearLayout llParcelLandUse = null;
    private LinearLayout llParcelBenefitLand = null;
    private LayoutInflater inflater = null;

    private ArrayList<LandInfo> arLstLandInfo = null;
    //    private SurveyParcelOwner surveyParcelOwner = null;
    private SurveyBuildingOwner surveyBuildingOwner = null;

    private Switch swUse = null;
    private Switch swRent = null;

    private ImageView iv1 = null;
    private ImageView iv2 = null;
    private ImageView iv3 = null;

    private Button btnDelete1 = null;
    private Button btnDelete2 = null;
    private Button btnDelete3 = null;

    private Button btnSave = null;

    private DatabaseManger dbManager = null;
    private BuildingInfo buildInfo = new BuildingInfo();

    private CheckBox chkLandUse = null;


    private Activity mActivity = null;

    private Dialog dlgAddBuildingUse = null;
    private Dialog dlgAddParcelOwner = null;
    private Dialog dlgAddParcelUse = null;

    private PictureInfo pictureInfo = new PictureInfo();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_detail_building);

        mActivity = this;
        Bundle bundle = getIntent().getExtras();
        if( bundle != null )
        {
            if(bundle.containsKey(ARG_ZBL))
            {
                parcelZBL = bundle.getString(ARG_ZBL);
            }
            if( bundle.containsKey(ARG_MODE) )
            {
                isEdit = bundle.getBoolean(ARG_MODE);
            }
            if( bundle.containsKey(ARG_INDEX) )
            {
                mIndex = bundle.getInt(ARG_INDEX);
            }
            if( bundle.containsKey(ARG_BUILDING_INFO) )
            {
                String gBuildingInfo = bundle.getString(ARG_BUILDING_INFO);
                Gson gson = new Gson();
                buildInfo = gson.fromJson(gBuildingInfo,BuildingInfo.class);
            }

        }

        inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        initial();
        initialValue();
        initialEvent();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if( !isEdit )
        {
           resetData();
        }
    }

    private void initial()
    {
        vParcelOwner = (View)findViewById(R.id.layout_parcel_owner);
        vLayoutInfoLand = (View)findViewById(R.id.layout_info_land);
        vLayoutInfoParcel = (View)findViewById(R.id.layout_info_parcel);

        edtPrefix = (EditText)findViewById(R.id.edtPrefix);
        edtftName = (EditText)findViewById(R.id.edtftName);
        edtlstName = (EditText)findViewById(R.id.edtlstName);

        tvAddLandUse = (TextView)findViewById(R.id.tvAddLandUse);
        tvAddParcel = (TextView)findViewById(R.id.tvAddParcel);
        tvAddParcelOwner = (TextView)findViewById(R.id.tvParcelOwner);

        iv1 = (ImageView)findViewById(R.id.iv1);
        iv2 = (ImageView)findViewById(R.id.iv2);
        iv3 = (ImageView)findViewById(R.id.iv3);

        btnDelete1 = (Button)findViewById(R.id.btnDelete1);
        btnDelete2 = (Button)findViewById(R.id.btnDelete2);
        btnDelete3 = (Button)findViewById(R.id.btnDelete3);

        btnSave = (Button)findViewById(R.id.btnSave);

        llParcelBenefitLand = (LinearLayout)findViewById(R.id.llParcelBenefitLand);
        llLandInfo = (LinearLayout)findViewById(R.id.llLandInfo);
        llParcelLandUse = (LinearLayout)findViewById(R.id.llParcelLandUse);
        llParcelBenefitLand.setVisibility(View.VISIBLE);

    }
    private void initialValue()
    {
        if(TextUtils.isEmpty(parcelZBL)) return;

        dbManager = new DatabaseManger();
        arLstLandInfo = dbManager.getLandInfo(parcelZBL);
        if( arLstLandInfo != null )
        {
            if( arLstLandInfo.size() <= 0 )
            {
                utils.createPopup(mActivity, "ไม่พบข้อมูล", "", "ตกลง", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mActivity.finish();
                    }
                });
                return ;
            }

            LandInfo landInfo = arLstLandInfo.get(0);
            if( landInfo != null )
            {
                DetailParcelLandInfo detailLand = new DetailParcelLandInfo(DetailBuildingActivity.this , parcelZBL);
                llLandInfo.addView(detailLand.onCreateView());

                initialSurveyBuilding();
                initialSurveyParcelOwnerUse();
                initialParcelOwner();
            }
        }
        btnDelete1.setEnabled(false);
        btnDelete2.setEnabled(false);
        btnDelete3.setEnabled(false);

        if (mIndex < ShareData.arBuildingBitmap.size() && mIndex != -1) {
            pictureInfo = ShareData.arBuildingBitmap.get(mIndex);
            if (pictureInfo != null) {
                if (pictureInfo.picture1 != null) {
                    iv1.setImageBitmap(pictureInfo.picture1);
                    btnDelete1.setEnabled(false);
                }
                if (pictureInfo.picture2 != null) {
                    iv2.setImageBitmap(pictureInfo.picture2);
                    btnDelete2.setEnabled(false);
                }
                if (pictureInfo.picture3 != null) {
                    iv3.setImageBitmap(pictureInfo.picture3);
                    btnDelete3.setEnabled(false);
                }
            }
        }

    }
    private void initialSurveyBuilding()
    {
        if( dbManager == null || arLstLandInfo==null ) return;
        if( arLstLandInfo.size() <= 0 ) return;

        ArrayList<SurveyBuilding> arLstSurveySignBoard = new ArrayList<SurveyBuilding>();
        //            dbManager.getSurveyBuilding(
//                    getLandInfo().getAsString(PARCEL_ID),
//                    getLandInfo().getAsString(PARCEL_SER),
//                    String.valueOf(index));
        if( isEdit )
        {
            arLstSurveySignBoard = buildInfo.arLstSurveyBuilding;
            llParcelLandUse.removeAllViews();
            for( int n=0; n < arLstSurveySignBoard.size();n++ )
            {
                AddSurveyBuilding(arLstSurveySignBoard.get(n) , -1);
            }
        }
    }
    private void initialSurveyParcelOwnerUse()
    {
        if( dbManager == null || arLstLandInfo==null ) return;
        if( arLstLandInfo.size() <= 0 ) return;

//        ArrayList<SurveyBuildingOwner> arLstSurveyParcelOwner = dbManager.getSurveyBuildingOwner(
//                getLandInfo().getAsString(PARCEL_ID),
//                getLandInfo().getAsString(PARCEL_SER),
//                String.valueOf(index));


        ArrayList<SurveyBuildingOwner> arLstSurveyParcelOwner = new ArrayList<SurveyBuildingOwner>();
        if( isEdit )
        {
            arLstSurveyParcelOwner = buildInfo.arLstSurveyBuildingOwner;

            for( int n=0;n<arLstSurveyParcelOwner.size();n++ )
            {
                AddParcelBenefitLand(arLstSurveyParcelOwner.get(n) , "");
            }
        }
    }

    private void initialParcelOwner()
    {
        if( dbManager == null || arLstLandInfo==null ) return;
        if( arLstLandInfo.size() <= 0 ) return;

        ArrayList<SurveyParcelOwner> surveyParcelOwner = dbManager.getSurveyParcelOwner(
                getLandInfo().getAsString(PARCEL_ID),
                getLandInfo().getAsString(PARCEL_SER)
        );
        if( surveyParcelOwner == null ) return;
        if( surveyParcelOwner.size() == 0 ) return;


        surveyBuildingOwner = new SurveyBuildingOwner();

        surveyBuildingOwner.CustomerID = surveyParcelOwner.get(0).CustomerID;
        surveyBuildingOwner.Parcel_ID = surveyParcelOwner.get(0).Parcel_ID;
        surveyBuildingOwner.LAST_NAME = surveyParcelOwner.get(0).LAST_NAME;
        surveyBuildingOwner.Parcel_Tmb = getLandInfo().getAsString(PARCEL_TMB);
        surveyBuildingOwner.Parcel_Ser = getLandInfo().getAsString(PARCEL_SER);

        if( TextUtils.isEmpty(surveyParcelOwner.get(0).FIRST_NAME) )
            surveyBuildingOwner.fullName = surveyParcelOwner.get(0).fullName;
        else
            surveyBuildingOwner.FIRST_NAME = surveyParcelOwner.get(0).FIRST_NAME;

        surveyBuildingOwner.HN_ID = surveyParcelOwner.get(0).PrefixName;
        surveyBuildingOwner.address = surveyParcelOwner.get(0).address;

        addParcelOwner(surveyBuildingOwner , TAG_OWNER);
    }

    private void removeParcelBenefitLand(int index)
    {
        if( llParcelBenefitLand == null ) return;
        if( llParcelBenefitLand.getChildCount() <= 0 ) return;
        if( (llParcelBenefitLand.getChildCount()-1) < index ) return;
        if( (ShareData.arLstSurveyBuildingOwner.size() < index )) return;

        llParcelBenefitLand.removeViewAt(index);
        ShareData.arLstSurveyBuildingOwner.remove(index);
        tvAddParcel.setVisibility(View.VISIBLE);
    }
    private void AddSurveyBuilding( SurveyBuilding buildingUse , int index )
    {
        //for( int n=0;n<arLstSurveySignBoard.size();n++ )
        {
            SurveyBuilding surveyBoard = buildingUse;


            View vBuild = null;
            if( index == -1 )
            {
                vBuild = inflater.inflate(R.layout.layout_building_use , null);
            }
            else
            {
                vBuild = (View)llParcelLandUse.getChildAt(0);
            }

            final View vBuildingUse = vBuild;

            EditText edtBuildingWidth = (EditText)vBuildingUse.findViewById(R.id.edtBuildingWidth);
            EditText edtBuildingHeight = (EditText)vBuildingUse.findViewById(R.id.edtBuildingHeight);
            EditText edtBuildingNumRoom = (EditText)vBuildingUse.findViewById(R.id.edtBuildingNumRoom);
            EditText edtBuildingRent = (EditText)vBuildingUse.findViewById(R.id.edtBuildingRent);
            EditText edtBuildingNumFloor = (EditText)vBuildingUse.findViewById(R.id.edtBuildingNumFloor);
            EditText edtHouseId = (EditText)vBuildingUse.findViewById(R.id.edtHouseId);
            EditText edtBuildNo = (EditText)vBuildingUse.findViewById(R.id.edtBuildNo);

            Spinner spnTypeBuilding = (Spinner)vBuildingUse.findViewById(R.id.spnTypeBuilding);

            Button btnDelete = (Button)vBuildingUse.findViewById(R.id.btnDelete);
            Button btnEdit = (Button)vBuildingUse.findViewById(R.id.btnEdit);

            edtBuildingWidth.setText(surveyBoard.Building_Width);
            edtBuildingHeight.setText(surveyBoard.Building_Higth);
            edtBuildingNumRoom.setText(surveyBoard.Building_Room);
            edtBuildingRent.setText(surveyBoard.Building_Rent);
            edtBuildingNumFloor.setText(surveyBoard.Building_Floor);
            edtHouseId.setText(surveyBoard.Building_Num);
            edtBuildNo.setText(""+surveyBoard.Build_No);

            RadioButton chkUseA = (RadioButton) vBuildingUse.findViewById(R.id.chkUseA);
            RadioButton chkUseB = (RadioButton) vBuildingUse.findViewById(R.id.chkUseB);
            RadioButton chkUseC = (RadioButton) vBuildingUse.findViewById(R.id.chkUseC);

            if( surveyBoard.Building_UseA.equals("1") )
            {
                chkUseA.setChecked(true);
                chkUseA.setEnabled(false);

                chkUseA.setVisibility(View.VISIBLE);
                chkUseB.setVisibility(View.GONE);
                chkUseC.setVisibility(View.GONE);
            }
            else  if( surveyBoard.Building_UseB.equals("1") )
            {
                chkUseB.setChecked(true);
                chkUseB.setEnabled(false);

                chkUseA.setVisibility(View.GONE);
                chkUseB.setVisibility(View.VISIBLE);
                chkUseC.setVisibility(View.GONE);
            }
            else  if( surveyBoard.Building_UseC.equals("1") )
            {
                chkUseC.setChecked(true);
                chkUseC.setEnabled(false);

                chkUseA.setVisibility(View.GONE);
                chkUseB.setVisibility(View.GONE);
                chkUseC.setVisibility(View.VISIBLE);
            }

            String typeName = dbManager.getNameBuilding(surveyBoard.Building_Type);
//            String typeName = surveyBoard.Building_Type;
            final ArrayAdapter<String> arAdapter = dbManager.getAdapter(mActivity,dbManager.getTypeBuilding());
            spnTypeBuilding.setAdapter( arAdapter );
            spnTypeBuilding.setSelection(0);

            for( int i=0;i<arAdapter.getCount();i++ )
            {
                if( arAdapter.getItem(i).equals(typeName) )
                {
                    spnTypeBuilding.setSelection(i);
                }
            }



            btnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int index = llParcelLandUse.indexOfChild(vBuildingUse);
                    if( isEdit )
                        showDialogAddBuildingUse(mIndex);
                    else
                        showDialogAddBuildingUse(0);
                }
            });

            btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final int index = llParcelLandUse.indexOfChild(vBuildingUse);
                    final SurveyBuilding building = ShareData.arLstSurveyBuilding.get(index);

                    String nameBuilding = DatabaseManger.getNameBuilding(building.Building_Type);
                    String Msg = "คุณต้องการลบ การทำประโยชน์โรงเรือน'"+nameBuilding+"' " +
                            "ใช่หรือไม่ ?";
                    utils.createPopup(mActivity, Msg, "", "ใช่", "ไม่ใช่",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

//                                    building.Building_Type = "-1";

                                    ShareData.arLstSurveyBuilding.remove(index);
                                    llParcelLandUse.removeViewAt(index);
                                    tvAddLandUse.setVisibility(View.VISIBLE);
                                }
                            },
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                }
            });

            if( index == -1 ) {
                llParcelLandUse.addView(vBuildingUse);
                ShareData.arLstSurveyBuilding.add(buildingUse);
            }
            else {
                ShareData.arLstSurveyBuilding.set(0, buildingUse);
            }

            tvAddLandUse.setVisibility(View.INVISIBLE);
        }
    }
    private void AddParcelBenefitLand(SurveyBuildingOwner surveyParcelOwner , String tag)
    {
        if( dbManager == null || arLstLandInfo==null ) return;
        if( arLstLandInfo.size() <= 0 ) return;

        final View vParcelOwner = inflater.inflate(R.layout.layout_parcel_owner,null);
        vParcelOwner.setTag(tag);
        vParcelOwner.setVisibility(View.VISIBLE);

        Button btnDelete = (Button)vParcelOwner.findViewById(R.id.btnDelete);
        TextView tvFullName = (TextView)vParcelOwner.findViewById(R.id.tvFullName);
        TextView tvAddress = (TextView)vParcelOwner.findViewById(R.id.tvAddress);
        TextView tvTopic = (TextView)vParcelOwner.findViewById(R.id.tvTopic);
        View vTopic = (View)vParcelOwner.findViewById(R.id.vTopic);
        View vSpace = (View)vParcelOwner.findViewById(R.id.vSpace);
        CheckBox chkLand = (CheckBox) vParcelOwner.findViewById(R.id.chkLandUse);

        chkLand.setVisibility(View.GONE);
        tvTopic.setVisibility(View.GONE);
        vTopic.setVisibility(View.GONE);
        vSpace.setVisibility(View.GONE);

//        tvFullName.setText(surveyParcelOwner.HN_ID+" "+surveyParcelOwner.FIRST_NAME+" "+surveyParcelOwner.LAST_NAME);
//        tvAddress.setText("บ้านเลขที่ "+surveyParcelOwner.NUM);

        tvFullName.setText(surveyParcelOwner.fullName);
        tvAddress.setText(surveyParcelOwner.address);

        btnDelete.setTag(tag);
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(v.getTag().toString()))
                {
                    for( int n =0; n < llParcelBenefitLand.getChildCount(); n++ )
                    {
                        if( llParcelBenefitLand.getChildAt(n).getTag().equals( TAG_OWNER ) )
                        {
                            chkLandUse.setEnabled(true);
                            chkLandUse.setOnCheckedChangeListener(null);
                            chkLandUse.setChecked(false);
                            chkLandUse.setOnCheckedChangeListener(onCheckChange);

                            removeParcelBenefitLand(n);
                            break;
                        }
                    }
                }
                else
                {
                    LinearLayout llRootParcelOwner = (LinearLayout) vParcelOwner.findViewById(R.id.llRootParcelOwner);
                    int index = ((ViewGroup) llRootParcelOwner.getParent()).indexOfChild(llRootParcelOwner);

                    chkLandUse.setEnabled(true);
                    chkLandUse.setOnCheckedChangeListener(onCheckChange);
                    removeParcelBenefitLand(index);
                }
            }
        });

        if( chkLandUse != null )
        {
            chkLandUse.setEnabled(false);
            chkLandUse.setOnCheckedChangeListener(null);
        }

        tvAddParcel.setVisibility(View.INVISIBLE);
        llParcelBenefitLand.addView(vParcelOwner);
        ShareData.arLstSurveyBuildingOwner.add(surveyParcelOwner);


//        if (isEdit) {
//            pictureInfo = ShareData.arBuildingImageBitmap.get(parcelZBL+"_"+surveyParcelOwner.Building_No);
//            String Building_No = ShareData.arLstSurveyBuildingOwner.get(0).Building_No;
//            pictureInfo = ShareData.arBuildingImageBitmap.get(parcelZBL+"_"+Building_No);
//        }
    }

    private void initialEvent()
    {
        tvAddParcel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogAddParcel();
            }
        });

        tvAddLandUse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogAddBuildingUse(-1);
            }
        });

        iv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent chooseImageIntent = ImagePicker.getPickImageIntent(mActivity);
                startActivityForResult(chooseImageIntent, PICK_IMAGE_ID_1);
            }
        });

        iv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent chooseImageIntent = ImagePicker.getPickImageIntent(mActivity);
                startActivityForResult(chooseImageIntent, PICK_IMAGE_ID_2);
            }
        });
        iv3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent chooseImageIntent = ImagePicker.getPickImageIntent(mActivity);
                startActivityForResult(chooseImageIntent, PICK_IMAGE_ID_3);
            }
        });

        btnDelete1.setOnClickListener(onDeleteImage);
        btnDelete2.setOnClickListener(onDeleteImage);
        btnDelete3.setOnClickListener(onDeleteImage);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSave();

            }
        });
    }
    private void resetData()
    {
        ShareData.arLstSurveyBuilding = new ArrayList<SurveyBuilding>();
        ShareData.arLstSurveyBuildingOwner = new ArrayList<SurveyBuildingOwner>();
    }
    private View.OnClickListener onDeleteImage = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if( v.getId() == R.id.btnDelete1 )
                dialogDeleteImage(v.getId());
            else if( v.getId() == R.id.btnDelete2 )
                dialogDeleteImage(v.getId());
            else if( v.getId() == R.id.btnDelete3 )
                dialogDeleteImage(v.getId());
        }
    };
    private void dialogDeleteImage(final int Id)
    {
        utils.createPopup(DetailBuildingActivity.this, "คุณต้องการลบรูป ใช่หรือไม่"
                , ""
                , "ใช่"
                , "ไม่ใช่"
                , new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteImage(Id);
                    }
                }
                , new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
    }
    private void deleteImage(int Id)
    {
        for (Map.Entry<Integer, Bitmap> entry : ShareData.arMapImageBitmap.entrySet()) {
            if (entry.getKey() > 300) {
                if( Id == R.id.btnDelete1 )
                {
                    if( entry.getKey() == PICK_IMAGE_ID_1 )
                    {
                        ShareData.arMapImageBitmap.remove(entry);
                        iv1.setImageDrawable(getResources().getDrawable(R.drawable.image_placeholder));
                        btnDelete1.setEnabled(false);
                    }
                }
                else if( Id == R.id.btnDelete2 )
                {
                    if( entry.getKey() == PICK_IMAGE_ID_2 )
                    {
                        ShareData.arMapImageBitmap.remove(entry);
                        iv2.setImageDrawable(getResources().getDrawable(R.drawable.image_placeholder));
                        btnDelete2.setEnabled(false);
                    }
                }
                else if( Id == R.id.btnDelete3 )
                {
                    if( entry.getKey() == PICK_IMAGE_ID_3 )
                    {
                        ShareData.arMapImageBitmap.remove(entry);
                        iv3.setImageDrawable(getResources().getDrawable(R.drawable.image_placeholder));
                        btnDelete3.setEnabled(false);
                    }
                }
            } else if (entry.getKey() > 200) {
                if( Id == R.id.btnDelete1 )
                {
                    if( entry.getKey() == PICK_IMAGE_ID_1 )
                    {
                        ShareData.arMapImageBitmap.remove(entry);
                        iv1.setImageDrawable(getResources().getDrawable(R.drawable.image_placeholder));
                        btnDelete1.setEnabled(false);
                    }
                }
                else if( Id == R.id.btnDelete2 )
                {
                    if( entry.getKey() == PICK_IMAGE_ID_2 )
                    {
                        ShareData.arMapImageBitmap.remove(entry);
                        iv2.setImageDrawable(getResources().getDrawable(R.drawable.image_placeholder));
                        btnDelete2.setEnabled(false);
                    }
                }
                else if( Id == R.id.btnDelete3 )
                {
                    if( entry.getKey() == PICK_IMAGE_ID_3 )
                    {
                        ShareData.arMapImageBitmap.remove(entry);
                        iv3.setImageDrawable(getResources().getDrawable(R.drawable.image_placeholder));
                        btnDelete3.setEnabled(false);
                    }
                }
            } else if (entry.getKey() > 100) {
                if( Id == R.id.btnDelete1 )
                {
                    if( entry.getKey() == PICK_IMAGE_ID_1 )
                    {
                        ShareData.arMapImageBitmap.remove(entry);
                        iv1.setImageDrawable(getResources().getDrawable(R.drawable.image_placeholder));
                        btnDelete1.setEnabled(false);
                    }
                }
                else if( Id == R.id.btnDelete2 )
                {
                    if( entry.getKey() == PICK_IMAGE_ID_2 )
                    {
                        ShareData.arMapImageBitmap.remove(entry);
                        iv2.setImageDrawable(getResources().getDrawable(R.drawable.image_placeholder));
                        btnDelete2.setEnabled(false);
                    }
                }
                else if( Id == R.id.btnDelete3 )
                {
                    if( entry.getKey() == PICK_IMAGE_ID_3 )
                    {
                        ShareData.arMapImageBitmap.remove(entry);
                        iv3.setImageDrawable(getResources().getDrawable(R.drawable.image_placeholder));
                        btnDelete3.setEnabled(false);
                    }
                }
            }
        }
    }
    private void showDialogAddParcel()
    {
        dlgAddParcelUse = new Dialog(mActivity);
        dlgAddParcelUse.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dlgAddParcelUse.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dlgAddParcelUse.setContentView(R.layout.dlg_add_parcel_use);
        dlgAddParcelUse.setCancelable(true);

        final AutoCompleteTextView edtPrefix = (AutoCompleteTextView)dlgAddParcelUse.findViewById(R.id.edtPrefix);
        final AutoCompleteTextView edtftName = (AutoCompleteTextView)dlgAddParcelUse.findViewById(R.id.edtftName);
        final AutoCompleteTextView edtlstName = (AutoCompleteTextView)dlgAddParcelUse.findViewById(R.id.edtlstName);
        final EditText edtBuildingNum = (EditText)dlgAddParcelUse.findViewById(R.id.edtBuildingNum);
        final TextView tvTopic = (TextView)dlgAddParcelUse.findViewById(R.id.tvTopic);

        tvTopic.setText("เพิ่มผู้ทำประโยชน์โรงเรือน");

        ArrayAdapter<String> adapter = DatabaseManger.getParcelOwnerAdapter(DetailBuildingActivity.this);
        edtftName.setAdapter(adapter);
        //edtPrefix.setAdapter(adapter);
        edtlstName.setAdapter(adapter);

        Button btnSave = (Button)dlgAddParcelUse.findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if( TextUtils.isEmpty(edtPrefix.getText().toString()) ||
                        TextUtils.isEmpty(edtftName.getText().toString()) ||
                        TextUtils.isEmpty(edtlstName.getText().toString()) ||
                        TextUtils.isEmpty(edtBuildingNum.getText().toString())  )
                {
                    utils.createPopup(DetailBuildingActivity.this , "กรุณาระบุข้อมูลให้ครบ" , "");
                    return;
                }

                String prefix = edtPrefix.getText().toString();
                String ftName = edtftName.getText().toString();
                String lstName = edtlstName.getText().toString();
                String buildNum = edtBuildingNum.getText().toString();

                SurveyBuildingOwner benefitLand = new SurveyBuildingOwner();
                benefitLand.NUM = buildNum;
                benefitLand.HN_ID = prefix;
                benefitLand.CustomerID = buildNum;
                benefitLand.PrefixName = prefix;
                benefitLand.FIRST_NAME = ftName;
                benefitLand.LAST_NAME = lstName ;
                benefitLand.fullName = prefix+" "+ftName+" "+lstName;
                benefitLand.address = "บ้านเลขที่ "+buildNum;
                benefitLand.MOO = getLandInfo().getAsString(PARCEL_MOO);
                benefitLand.Parcel_ID = getLandInfo().getAsString(PARCEL_ID);
                benefitLand.Parcel_Ser = getLandInfo().getAsString(PARCEL_SER);
                benefitLand.Parcel_Tmb = getLandInfo().getAsString(PARCEL_TMB);

                AddParcelBenefitLand(benefitLand,"");
                dlgAddParcelUse.dismiss();
            }
        });
        Button btnCancel = (Button)dlgAddParcelUse.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dlgAddParcelUse.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dlgAddParcelUse.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dlgAddParcelUse.show();
        dlgAddParcelUse.getWindow().setAttributes(lp);
    }
    private void showDialogAddBuildingUse(final int index)
    {
        dlgAddBuildingUse = new Dialog(mActivity);
        dlgAddBuildingUse.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dlgAddBuildingUse.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dlgAddBuildingUse.setContentView(R.layout.dlg_add_building);
        dlgAddBuildingUse.setCancelable(true);

        final EditText edtBuildingWidth = (EditText)dlgAddBuildingUse.findViewById(R.id.edtBuildingWidth);
        final EditText edtBuildNo = (EditText)dlgAddBuildingUse.findViewById(R.id.edtBuildNo);
        final EditText edtHouseId = (EditText)dlgAddBuildingUse.findViewById(R.id.edtHouseId);
        final EditText edtBuildingHeight = (EditText)dlgAddBuildingUse.findViewById(R.id.edtBuildingHeight);
        final EditText edtBuildingNumRoom = (EditText)dlgAddBuildingUse.findViewById(R.id.edtBuildingNumRoom);
        final EditText edtBuildingRent = (EditText)dlgAddBuildingUse.findViewById(R.id.edtBuildingRent);
        final EditText edtBuildingNumFloor = (EditText)dlgAddBuildingUse.findViewById(R.id.edtBuildingNumFloor);
        final Spinner spnTypeBuilding = (Spinner)dlgAddBuildingUse.findViewById(R.id.spnTypeBuilding);
        final RadioGroup radioTypeBuilding = (RadioGroup)dlgAddBuildingUse.findViewById(R.id.rdTypeBuilding);

        final ArrayAdapter<String> arAdapter = dbManager.getAdapter(mActivity,dbManager.getTypeBuilding());
        spnTypeBuilding.setAdapter( arAdapter );
        spnTypeBuilding.setSelection(0);

        if( index > -1 )
        {
            // mode edit
            edtBuildingWidth.setText(ShareData.arLstSurveyBuilding.get(0).Building_Width);
            edtBuildNo.setText(""+ShareData.arLstSurveyBuilding.get(0).Build_No);
            edtBuildingHeight.setText(ShareData.arLstSurveyBuilding.get(0).Building_Higth);
            edtBuildingNumRoom.setText(ShareData.arLstSurveyBuilding.get(0).Building_Room);
            edtBuildingRent.setText(ShareData.arLstSurveyBuilding.get(0).Building_Rent);
            edtBuildingNumFloor.setText(ShareData.arLstSurveyBuilding.get(0).Building_Floor);
            edtHouseId.setText(ShareData.arLstSurveyBuilding.get(0).Building_Num);

            String building_type = ShareData.arLstSurveyBuilding.get(0).Building_Type;
            String name_building = DatabaseManger.getNameBuilding(building_type);
            for( int n=0;n<arAdapter.getCount();n++ )
            {
                if( arAdapter.getItem(n).equals(name_building))
                {
                    spnTypeBuilding.setSelection(n);
                }
            }



            String Building_UseA = ShareData.arLstSurveyBuilding.get(0).Building_UseA;
            String Building_UseB = ShareData.arLstSurveyBuilding.get(0).Building_UseB;
            String Building_UseC = ShareData.arLstSurveyBuilding.get(0).Building_UseC;

            RadioButton chkUseA = (RadioButton) dlgAddBuildingUse.findViewById(R.id.chkUseA);
            RadioButton chkUseB = (RadioButton) dlgAddBuildingUse.findViewById(R.id.chkUseB);
            RadioButton chkUseC = (RadioButton) dlgAddBuildingUse.findViewById(R.id.chkUseC);

            if( !TextUtils.isEmpty(Building_UseA) )
                chkUseA.setChecked(true);
            else if( !TextUtils.isEmpty(Building_UseB) )
                chkUseB.setChecked(true);
            else if( !TextUtils.isEmpty(Building_UseC) )
                chkUseC.setChecked(true);
        }
        else
        {
            for( int n=0;n<arAdapter.getCount();n++ )
            {
                if( arAdapter.getItem(n).equals("บ้านพักอาศัยไม้ชั้นเดียว"))
                {
                    spnTypeBuilding.setSelection(n);
                    break;
                }
            }
        }

        Button btnSave = (Button)dlgAddBuildingUse.findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if( TextUtils.isEmpty(edtBuildingWidth.getText().toString()) ||
                        TextUtils.isEmpty(edtBuildingHeight.getText().toString()) ||
                        TextUtils.isEmpty(edtBuildingRent.getText().toString()) ||
                        TextUtils.isEmpty(edtBuildingNumRoom.getText().toString()) ||
                        TextUtils.isEmpty(edtHouseId.getText().toString()) ||
                        TextUtils.isEmpty(edtBuildingNumFloor.getText().toString()) )
                {
                    utils.createPopup(DetailBuildingActivity.this , "กรุณาระบุข้อมูลให้ครบ" , "");
                    return;
                }

//                ShareData.arLstSurveyBuilding.clear();
                SurveyBuilding surveyBoard = new SurveyBuilding();
                surveyBoard.Parcel_ID = getLandInfo().getAsString(PARCEL_ID);
                surveyBoard.Parcel_Ser = getLandInfo().getAsString(PARCEL_SER);
                surveyBoard.Parcel_Tmb = getLandInfo().getAsString(PARCEL_TMB);

                surveyBoard.Building_Width = edtBuildingWidth.getText().toString();
                surveyBoard.Building_Higth = edtBuildingHeight.getText().toString();

                int selectedId = radioTypeBuilding.getCheckedRadioButtonId();

                if( selectedId == R.id.chkUseA )
                    surveyBoard.Building_UseA = "1";
                else if( selectedId == R.id.chkUseB )
                    surveyBoard.Building_UseB = "1";
                else if( selectedId == R.id.chkUseC )
                    surveyBoard.Building_UseC = "1";

                surveyBoard.Building_Rent = edtBuildingRent.getText().toString();
                surveyBoard.Building_Tax = "-1";

                surveyBoard.Building_Group = "1";
                surveyBoard.Building_Location = "1";

                surveyBoard.Building_Type = DatabaseManger.getIdTypeBuilding(spnTypeBuilding.getSelectedItem().toString());
                surveyBoard.Building_Room = edtBuildingNumRoom.getText().toString();
                surveyBoard.Building_Num = edtHouseId.getText().toString();//บ้านเลขที่
                surveyBoard.Building_Floor = edtBuildingNumFloor.getText().toString();
                surveyBoard.Building_Moo = getLandInfo().getAsString(PARCEL_MOO);

                surveyBoard.CreateBy = "";
                surveyBoard.CreateDate = "";
                surveyBoard.ModifyBy = "";
                surveyBoard.ModifyDate = "";
                surveyBoard.StatusLink = "";

//                ShareData.arLstSurveyBuilding.add(surveyBoard);

                AddSurveyBuilding(surveyBoard , index);

                dlgAddBuildingUse.dismiss();
            }
        });
        Button btnCancel = (Button)dlgAddBuildingUse.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dlgAddBuildingUse.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dlgAddBuildingUse.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dlgAddBuildingUse.show();
        dlgAddBuildingUse.getWindow().setAttributes(lp);
    }

    private ContentValues getLandInfo()
    {
        ContentValues cv = new ContentValues();
        if( arLstLandInfo.size() > 0 )
        {
            cv.put(PARCEL_ID,arLstLandInfo.get(0).Parcel_Id);
            cv.put(PARCEL_SER,arLstLandInfo.get(0).Parcel_Ser);
            cv.put(PARCEL_TMB,arLstLandInfo.get(0).Tmb);
            cv.put(PARCEL_MOO,arLstLandInfo.get(0).Moo);
        }
        else {
            cv.put(PARCEL_ID,"");
            cv.put(PARCEL_SER,"");
            cv.put(PARCEL_TMB,"");
            cv.put(PARCEL_MOO,"");
        }

        return cv;
    }

    private void addParcelOwner(final SurveyBuildingOwner surveyParcelOwner , final String tag)
    {
        Button btnEdit = (Button)vParcelOwner.findViewById(R.id.btnEdit);
        btnEdit.setVisibility(View.GONE);

        Button btnDelete = (Button)vParcelOwner.findViewById(R.id.btnDelete);
        if( tag.equals(TAG_OWNER) )
        {
            btnDelete.setVisibility(View.GONE);
        }

        TextView tvFullName = (TextView)vParcelOwner.findViewById(R.id.tvFullName);
        TextView tvAddress = (TextView)vParcelOwner.findViewById(R.id.tvAddress);

        chkLandUse = (CheckBox) vParcelOwner.findViewById(R.id.chkLandUse);
        chkLandUse.setText("ผู้ทำประโยชน์โรงเรือน");

        if( llParcelBenefitLand.getChildCount() > 0 )
        {
            chkLandUse.setEnabled(false);
            chkLandUse.setOnCheckedChangeListener(null);
        }
        else {
            chkLandUse.setEnabled(true);
            chkLandUse.setOnCheckedChangeListener(onCheckChange);
        }

//        tvFullName.setText(surveyParcelOwner.FIRST_NAME+" "+surveyBuildingOwner.LAST_NAME);
//        tvAddress.setText(surveyParcelOwner.HN_ID);
        tvFullName.setText(surveyParcelOwner.fullName);
        tvAddress.setText(surveyParcelOwner.address);

        /*if (isEdit) {
            if (pictureInfo == null)
                pictureInfo = ShareData.arBuildingNewImageBitmap.get(parcelZBL+"_"+surveyBuildingOwner.Parcel_ID);
            if (pictureInfo != null) {
                if (pictureInfo.picture1 != null) {
                    iv1.setImageBitmap(pictureInfo.picture1);
                    btnDelete1.setEnabled(false);
                }
                if (pictureInfo.picture2 != null) {
                    iv2.setImageBitmap(pictureInfo.picture2);
                    btnDelete2.setEnabled(false);
                }
                if (pictureInfo.picture3 != null) {
                    iv3.setImageBitmap(pictureInfo.picture3);
                    btnDelete3.setEnabled(false);
                }
            } else {
                pictureInfo = new PictureInfo();
            }
        }*/
    }

    private CompoundButton.OnCheckedChangeListener onCheckChange = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if( isChecked )
            {
                SurveyBuildingOwner surveyBuildingOwnerTmp = surveyBuildingOwner;
                surveyBuildingOwnerTmp.FIRST_NAME = surveyBuildingOwner.fullName;
                AddParcelBenefitLand(surveyBuildingOwnerTmp , TAG_OWNER);
            }
            else
            {
//                LinearLayout llRootParcelOwner = (LinearLayout) vParcelOwner.findViewById(R.id.llRootParcelOwner);
//                int index = ((ViewGroup) llRootParcelOwner.getParent()).indexOfChild(llRootParcelOwner);
                int index = -1;
                for( int n =0; n < llParcelBenefitLand.getChildCount(); n++ )
                {
                    if( llParcelBenefitLand.getChildAt(n).getTag().equals(TAG_OWNER) )
                    {
                        index = n;
                        break;
                    }
                }

                if( index > -1 )
                    removeParcelBenefitLand(index);
            }
        }
    };
    private void onSave()
    {
        if( ShareData.arLstSurveyBuildingOwner.size() <= 0 )
        {
            utils.createPopup(DetailBuildingActivity.this
                    , "กรุณาเพิ่ม ผู้ทำประโยชน์โรงเรือน"
                    , ""
                    , "ตกลง"
                    , new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

            return;
        }
        if( ShareData.arLstSurveyBuilding.size() <= 0 )
        {
            utils.createPopup(DetailBuildingActivity.this
                    , "กรุณาเพิ่ม การทำประโยชน์โรงเรือน"
                    , ""
                    , "ตกลง"
                    , new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

            return;
        }

        BuildingInfo buildingInfo = new BuildingInfo();

//        for( int n=0;n<ShareData.arLstSurveyBuilding.size(); n++ )
//        {
        if (ShareData.arLstSurveyBuilding.size() > 0) {
            SurveyBuilding surveyBuilding = ShareData.arLstSurveyBuilding.get(ShareData.arLstSurveyBuilding.size() - 1);
            buildingInfo.arLstSurveyBuilding.add(surveyBuilding);
        }
//        }
//        for( int n=0;n<ShareData.arLstSurveyBuildingOwner.size(); n++ )
//        {
        if (ShareData.arLstSurveyBuildingOwner.size() > 0) {
            SurveyBuildingOwner surveyBuildingOwner = ShareData.arLstSurveyBuildingOwner.get(ShareData.arLstSurveyBuildingOwner.size() - 1);
/*
        //for( int n=0;n<ShareData.arLstSurveyBuilding.size(); n++ )
        if(ShareData.arLstSurveyBuilding.size() > 0)
        {
            SurveyBuilding surveyBuilding = ShareData.arLstSurveyBuilding.get(0);
            buildingInfo.arLstSurveyBuilding.add(surveyBuilding);
        }
//        for( int n=0;n<ShareData.arLstSurveyBuildingOwner.size(); n++ )
        if( ShareData.arLstSurveyBuildingOwner.size() > 0 )
        {
            SurveyBuildingOwner surveyBuildingOwner = ShareData.arLstSurveyBuildingOwner.get(0);
*/
            buildingInfo.arLstSurveyBuildingOwner.add(surveyBuildingOwner);
        }
//        }
        if( !isEdit )
        {
            //add mode
            ShareData.arLstBuildingInfo.add(buildingInfo);
            ShareData.arBuildingBitmap.add(pictureInfo);
        }
        else
        {
            //edit mode
            ShareData.arLstBuildingInfo.set(mIndex,buildingInfo);
            ShareData.arBuildingBitmap.set(mIndex,pictureInfo);
//            ShareData.arBuildingImageBitmap.put(surveyBuildingOwner.Building_No,pictureInfo);
        }
//        ShareData.arBuildingNewImageBitmap.put(parcelZBL+"_"+surveyBuildingOwner.Parcel_ID,pictureInfo);

        finish();
        resetData();
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Bitmap bmp = null;
        switch(requestCode) {
            case PICK_IMAGE_ID_1:
                bmp = ImagePicker.getImageFromResult(mActivity, resultCode, data);

                if( bmp != null && !bmp.toString().equals("")) {
                    iv1.setImageBitmap(bmp);
//                    ShareData.arMapImageBitmap.put(PICK_IMAGE_ID_1,bmp);
                    pictureInfo.picture1 = bmp;
                    btnDelete1.setEnabled(true);
                }
                break;
            case PICK_IMAGE_ID_2:
                bmp = ImagePicker.getImageFromResult(mActivity, resultCode, data);

                if( bmp != null && !bmp.toString().equals("")) {
                    iv2.setImageBitmap(bmp);
//                    ShareData.arMapImageBitmap.put(PICK_IMAGE_ID_2,bmp);
                    pictureInfo.picture2 = bmp;
                    btnDelete2.setEnabled(true);
                }
                break;
            case PICK_IMAGE_ID_3:
                bmp = ImagePicker.getImageFromResult(mActivity, resultCode, data);

                if( bmp != null && !bmp.toString().equals("")) {
                    iv3.setImageBitmap(bmp);
//                    ShareData.arMapImageBitmap.put(PICK_IMAGE_ID_3,bmp);
                    pictureInfo.picture3 = bmp;
                    btnDelete3.setEnabled(true);
                }
                break;

            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }
}
