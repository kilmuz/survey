package com.survay;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.Object.ShareData;
import com.ObjectDatabase.DataBase;
import com.ObjectDatabase.DatabaseManger;
import com.adapter.BlockAdapter;
import com.model.BlockParcel;

public class ListZoneParcel extends AppCompatActivity {
    private RecyclerView recyclerView = null;
    private BlockAdapter blockAdapter = null;
    private ProgressDialog pDialog = null;
    private Bundle bundle = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_block_parcel);

        bundle = getIntent().getExtras();
        if( bundle == null ) return;
        if( !bundle.containsKey("blockParcel") ) return;

        recyclerView = (RecyclerView)findViewById(R.id.recyclerview);

        DataBase db = new DataBase(this);
        ShareData.setDB(db);

        DatabaseManger dbManager = new DatabaseManger();

        blockAdapter = new BlockAdapter(dbManager.getZoneParcel(bundle.getString("blockParcel")) , ListZoneParcel.this);
        blockAdapter.setListenerItemClick(new BlockAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, BlockParcel blockParcel) {
                ShareData.getInstance().PATH_PIC_SUB_FIRST = blockParcel.blockParcel;

                Intent intent = new Intent(ListZoneParcel.this, ListSubBlockParcel.class);
                intent.putExtra("blockParcel" , bundle.getString("blockParcel")+blockParcel
                        .blockParcel);
                startActivity(intent);
            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(ListZoneParcel.this));
        recyclerView.setAdapter(blockAdapter);
    }
}
