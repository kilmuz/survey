package com.survay;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.Object.ShareData;
import com.ObjectDatabase.DataBase;
import com.ObjectDatabase.DatabaseManger;
import com.adapter.BlockAdapter;
import com.model.BlockParcel;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class ListBlockParcel extends AppCompatActivity {
    private RecyclerView recyclerView = null;
    private BlockAdapter blockAdapter = null;
    private ProgressDialog pDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_block_parcel);

        recyclerView = (RecyclerView)findViewById(R.id.recyclerview);
        String dbPath = Environment.getExternalStorageDirectory().getAbsolutePath()
                +"/database/";

        File f = new File(dbPath,"survey");


        if (!f.exists())
        {
            DataBase db = new DataBase(this);
            ShareData.setDB(db);
            new ImportDatabase().execute("");
        }
        else
        {
            DataBase db = new DataBase(this);
            ShareData.setDB(db);

            DatabaseManger dbManager = new DatabaseManger();

            blockAdapter = new BlockAdapter(dbManager.getBlockParcelNew() , ListBlockParcel.this);
            blockAdapter.setListenerItemClick(new BlockAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position, BlockParcel blockParcel) {

                    Intent intent = new Intent(ListBlockParcel.this, ListZoneParcel.class);
                    intent.putExtra("blockParcel" , blockParcel.blockParcel);
                    startActivity(intent);
                }
            });
            recyclerView.setLayoutManager(new LinearLayoutManager(ListBlockParcel.this));
            recyclerView.setAdapter(blockAdapter);
        }
    }

    private void initialDB( String path  )
    {
        try {
            ArrayList<String> arLstColName = new ArrayList<String>();
            String[] paths = path.split("/");
            String fileName = paths[paths.length-1];
            fileName = fileName.substring(0,fileName.indexOf("."));

            BufferedReader br = new BufferedReader(new FileReader(path));
            String readLine = null;
            readLine = br.readLine();

            Cursor cursor = ShareData.getDB().returnDatabase().rawQuery("PRAGMA table_info("+fileName+")",null);

            if( cursor.moveToFirst() )
            {
                for (int i = 0; i < cursor.getCount(); ++i) {
                    String typeName = cursor.getString(1);
                    arLstColName.add(typeName);
                    cursor.moveToNext();
                }
            }

            try {
                int nCount = 0;
                Boolean isSpacial = false;

                isSpacial = fileName.equals("SurveyParcel");
                while ((readLine = br.readLine()) != null) {

                    String[] values = null;
                    String escaped = readLine.replace("\"", "");
                    values = escaped.split(",");
                    if( values == null || values.length <= 0 ) return;

                    ContentValues cv = new ContentValues();
                    for (int n=0;n<arLstColName.size(); n++)
                    {
                        if( isSpacial )
                        {
                            if( n < 10 )
                            {
                                if( values.length > n )
                                {
                                    if(TextUtils.isEmpty(values[n]))
                                        cv.put(arLstColName.get(n),"");
                                    else
                                    {
                                        cv.put(arLstColName.get(n),values[n]);
                                    }
                                }
                            }
                        }
                        else
                        {
                            if( values.length > n )
                            {
                                if(TextUtils.isEmpty(values[n]))
                                    cv.put(arLstColName.get(n),"");
                                else
                                    cv.put(arLstColName.get(n),values[n]);
                            }

                        }
                    }
                    ContentValues cvResult = ShareData.getDB().Insert(fileName,cv);
                    if( cvResult.getAsLong("result") > -1 )
                    {
                        Log.e("","insert"+fileName+"success");
                    }
                    else
                    {
                        Log.e("","insert"+fileName+"failure");
                    }
                    nCount++;
                }
            } catch (Exception e) {
               Log.e( "","path = " + path + " \nex = "+e.toString() );
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void initial()
    {
        new Handler(Looper.getMainLooper())
                .post(new Runnable() {
                    @Override
                    public void run() {

                        DatabaseManger dbManager = new DatabaseManger();

//                        blockAdapter = new BlockAdapter(dbManager.getBlockParcel() , ListBlockParcel.this);
                        blockAdapter = new BlockAdapter(dbManager.getBlockParcelNew() ,
                                ListBlockParcel.this);

                        blockAdapter.setListenerItemClick(new BlockAdapter.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position, BlockParcel blockParcel) {
                                Intent intent = new Intent(ListBlockParcel.this, ListSubBlockParcel.class);
                                intent.putExtra("blockParcel" , blockParcel.blockParcel);
                                startActivity(intent);
                            }
                        });
                        recyclerView.setLayoutManager(new LinearLayoutManager(ListBlockParcel.this));
                        recyclerView.setAdapter(blockAdapter);
                    }
                });


    }

    private void initialMasterDB()
    {
        //export_survey
        String path = Environment.getExternalStorageDirectory().toString()+"/import_survey";
        Log.d("Files", "Path: " + path);
        File f = new File(path);
        File file[] = f.listFiles();
        Log.d("Files", "Size: "+ file.length);
        for (int i=0; i < file.length; i++)
        {
            Log.d("Files", "FileName:" + file[i].getName());
            Log.d("Files", "FileName:" + file[i].getPath());

            if( file[i].getName().equals("SurveyParcel.csv") )
            {
                initialDB(file[i].getPath());
            }
            else
            {
                initialDB(file[i].getPath());
            }
        }
    }

    private class ImportDatabase extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            initialMasterDB();
            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {
            initial();
            pDialog.dismiss();

        }

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(ListBlockParcel.this);
            pDialog.setMessage("กรุณารอสักครู่ ระบบกำลัง Import ฐานข้อมูล");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected void onProgressUpdate(Void... values) {}
    }
}
