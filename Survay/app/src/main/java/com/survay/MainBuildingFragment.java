package com.survay;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.Object.DetailParcelLandInfo;
import com.Object.ShareData;
import com.Object.utils;
import com.ObjectDatabase.DatabaseManger;
import com.google.gson.Gson;
import com.model.BuildingInfo;
import com.model.LandInfo;
import com.model.PictureInfo;
import com.model.SurveyBuilding;
import com.model.SurveyBuildingOwner;

import java.util.ArrayList;
import java.util.HashMap;

public class MainBuildingFragment extends Fragment {
    private static final String ARG_ZBL = "zbl";
    private static final String ARG_MODE = "mode";
    private static final String ARG_INDEX = "index";
    private static final String ARG_BUILDING_INFO = "building_info";

    // TODO: Rename and change types of parameters
    private String parcelZBL;

    private View vRoot = null;
    private View vLayoutInfoParcel = null;

    private TextView tvAddParcel = null;
    private LinearLayout llParcelBenefitLand = null;
    private LinearLayout llLandInfo = null;
    private LayoutInflater inflater = null;

    private ArrayList<LandInfo> arLstLandInfo = null;

    private DatabaseManger dbManager = null;
    private Activity mActivity = null;


    public MainBuildingFragment() {
        // Required empty public constructor
    }

    public static MainBuildingFragment newInstance(String param1) {
        MainBuildingFragment fragment = new MainBuildingFragment();
        Bundle args = new Bundle();
        args.putString(ARG_ZBL, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            parcelZBL = getArguments().getString(ARG_ZBL);
            mActivity = getActivity();
        }

        ShareData.arLstSurveyBuildingOwner = new ArrayList<SurveyBuildingOwner>();
        ShareData.arLstSurveyBuilding = new ArrayList<SurveyBuilding>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.inflater = inflater;
        vRoot = inflater.inflate(R.layout.fragment_main_building, container, false);

        initial();
        initialValue();
        initialEvent();

        return vRoot;
    }

    private void initial()
    {
        tvAddParcel = (TextView)vRoot.findViewById(R.id.tvAddParcel);

        llLandInfo = (LinearLayout)vRoot.findViewById(R.id.llLandInfo);
        llParcelBenefitLand = (LinearLayout)vRoot.findViewById(R.id.llParcelBenefitLand);
        llParcelBenefitLand.setVisibility(View.VISIBLE);

    }
    private void initialValue()
    {
        if(TextUtils.isEmpty(parcelZBL)) return;

        dbManager = new DatabaseManger();
        arLstLandInfo = dbManager.getLandInfo(parcelZBL);
        if( arLstLandInfo != null )
        {
            DetailParcelLandInfo detailParcelLandInfo = new DetailParcelLandInfo(mActivity, parcelZBL);
            llLandInfo.addView(detailParcelLandInfo.onCreateView());

            ShareData.arLstBuildingInfo = DatabaseManger.getBuildingInfo(arLstLandInfo.get(0).Parcel_Id,
                    arLstLandInfo.get(0).Parcel_Ser,
                    ""
            );

            for ( int n = 0; n < ShareData.arLstBuildingInfo.size(); n++ ) {
                for (HashMap.Entry<String,PictureInfo> obj : ShareData.arBuildingImageBitmap.entrySet()) {
                    SurveyBuilding surveyBuilding = ShareData.arLstBuildingInfo.get(n).arLstSurveyBuilding.get(0);
                    String[] key = obj.getKey().split("_");
                    if (key[0].equals(parcelZBL) && key[1].equals(surveyBuilding.Build_No+"")) {
                        ShareData.arBuildingBitmap.add(obj.getValue());
                    }
                }
                if (ShareData.arBuildingBitmap.size() <= n) {
                    ShareData.arBuildingBitmap.add(new PictureInfo());
                }
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        onCreateListBenefit();
    }

    private void onCreateListBenefit()
    {
        if( llParcelBenefitLand == null ) return;
        if( ShareData.arLstBuildingInfo == null ) return;
        if( ShareData.arLstBuildingInfo.size() == 0 ) return;

        llParcelBenefitLand.removeAllViews();
        ArrayList<BuildingInfo> arLstBuildingInfo = ShareData.arLstBuildingInfo;
        for( int n = 0; n < arLstBuildingInfo.size(); n++ )
        {
            final View vParcelOwner = inflater.inflate(R.layout.item_list_benefit,null);
            vParcelOwner.setVisibility(View.VISIBLE);

            Button btnDelete = (Button)vParcelOwner.findViewById(R.id.btnDelete);
            Button btnEdit = (Button)vParcelOwner.findViewById(R.id.btnEdit);
            TextView tvNameBenefit = (TextView)vParcelOwner.findViewById(R.id.tvNameBenefit);
            TextView tvTypeBenefit = (TextView)vParcelOwner.findViewById(R.id.tvTypeBenefit);

            ArrayList<SurveyBuilding> arLstSurveyBuilding = arLstBuildingInfo.get(n).arLstSurveyBuilding;
            for( int i = 0; i < arLstSurveyBuilding.size(); i++ )
            {
                String buildingType = DatabaseManger.getNameBuilding(arLstSurveyBuilding.get(i).Building_Type);
                tvTypeBenefit.setText(buildingType);
            }

            ArrayList<SurveyBuildingOwner> arLstSurveyBuildingOwner = arLstBuildingInfo.get(n).arLstSurveyBuildingOwner;
            for( int z = 0; z < arLstSurveyBuildingOwner.size(); z++ )
            {
                tvNameBenefit.setText(arLstSurveyBuildingOwner.get(z).fullName);
            }

            btnDelete.setTag(n);
            btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    LinearLayout llRootParcelOwner = (LinearLayout) vParcelOwner.findViewById(R.id.llRoot);
//                    final int index = ((ViewGroup) llRootParcelOwner.getParent()).indexOfChild(llRootParcelOwner);

                    if( v.getTag() != null ) {
                        if (!TextUtils.isEmpty(v.getTag().toString())) {
                            final int index = Integer.valueOf(v.getTag().toString());
                            BuildingInfo buildingInfo = ShareData.arLstBuildingInfo.get(index);
                            String nameBuilding = DatabaseManger.getNameBuilding(
                                    buildingInfo.arLstSurveyBuilding.get(0).Building_Type);

                            String Msg = "คุณต้องการลบ การทำประโยชน์โรงเรือน'"+nameBuilding+"' " +"ใช่หรือไม่ ?";
                            utils.createPopup(mActivity, Msg, "", "ใช่", "ไม่ใช่",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                            ShareData.arLstBuildingInfo.remove(index);
                                            if( ShareData.arLstBuildingInfo.size() <= 0 )
                                                llParcelBenefitLand.removeAllViews();
                                            else
                                                onCreateListBenefit();
                                        }
                                    },
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                        }
                    }

                }
            });

            btnEdit.setTag(n);
            btnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int index =  -1;
                    if( v.getTag() != null )
                    {
                        if( !TextUtils.isEmpty(v.getTag().toString()) )
                        {
                            index = Integer.valueOf(v.getTag().toString());
                            BuildingInfo buildingInfo = ShareData.arLstBuildingInfo.get(index);
                            Gson gson = new Gson();
                            String gBuildingInfo = gson.toJson(buildingInfo);

                            Intent i = new Intent(mActivity , DetailBuildingActivity.class);
                            i.putExtra(ARG_ZBL,parcelZBL);
                            i.putExtra(ARG_MODE,true);
                            i.putExtra(ARG_INDEX,index);
                            i.putExtra(ARG_BUILDING_INFO , gBuildingInfo);
                            startActivity(i);
                        }
                    }
                }
            });

            llParcelBenefitLand.addView(vParcelOwner);


        }
    }
    private void initialEvent()
    {
        tvAddParcel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mActivity , DetailBuildingActivity.class);
                i.putExtra(ARG_ZBL,parcelZBL);
                startActivity(i);
            }
        });
    }
}
