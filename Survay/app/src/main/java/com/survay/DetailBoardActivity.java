package com.survay;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.Object.DetailParcelLandInfo;
import com.Object.ShareData;
import com.Object.utils;
import com.ObjectDatabase.DatabaseManger;
import com.UICustomize.ImagePicker;
import com.google.gson.Gson;
import com.model.LandInfo;
import com.model.PictureInfo;
import com.model.SignBoardInfo;
import com.model.SurveyParcelOwner;
import com.model.SurveySignBoard;
import com.model.SurveySignBoardOwner;

import java.util.ArrayList;

public class DetailBoardActivity extends BaseOnActivity {

    private static final String ARG_ZBL = "zbl";
    private static final String ARG_MODE = "mode";
    private static final String ARG_INDEX = "index";
    private static final String ARG_SIGN_BORD_INFO = "sign_bord";
    public static final int PICK_IMAGE_ID_1 = 331; // the number doesn't matter
    public static final int PICK_IMAGE_ID_2 = 332; // the number doesn't matter
    public static final int PICK_IMAGE_ID_3 = 333; // the number doesn't matter
    private static final String PARCEL_ID = "Parcel_Id"; // the number doesn't matter
    private static final String PARCEL_SER = "Parcel_Ser"; // the number doesn't matter
    private static final String PARCEL_TMB = "Parcel_Tmb"; // the number doesn't matter
    private static final String TAG = "DetailParcelActivity"; // the number doesn't matter
    private static final String TAG_OWNER = "owner"; // the number doesn't matter


    // TODO: Rename and change types of parameters
    private String parcelZBL;


    private View vParcelOwner = null;

    private EditText edtSignText = null;
    private EditText edtSignWidth = null;
    private EditText edtSignHeight = null;
    private EditText edtSignSide = null;
    private Spinner spnSignType = null;

    private TextView tvAddLandUse = null;
    private TextView tvAddParcel = null;
    private TextView tvAddParcelOwner = null;

    private LinearLayout llLandInfo = null;
    private LinearLayout llParcelOwner = null;
    private LinearLayout llParcelBenefitLand = null;
    private LinearLayout llParcelLandUse = null;
    private LayoutInflater inflater = null;

    private ArrayList<LandInfo> arLstLandInfo = null;
//    private SurveyParcelOwner surveyParcelOwner = null;
    private SurveySignBoardOwner surveySignBoardOwner = null;

    private Switch swUse = null;
    private Switch swRent = null;

    private ImageView iv1 = null;
    private ImageView iv2 = null;
    private ImageView iv3 = null;


    private Button btnDelete1 = null;
    private Button btnDelete2 = null;
    private Button btnDelete3 = null;

    private DatabaseManger dbManager = null;
    private Button btnSave = null;

    private CheckBox chkLandUse = null;
    private  boolean isEdit=false;
    private int mIndex = -1;

    private SignBoardInfo signBoardInfo = null;
    private Activity mActivity = null;

    private Dialog dlgAddSignUse= null;
    private Dialog dlgAddParcelOwner = null;
    private Dialog dlgAddParcelUse = null;

    private PictureInfo pictureInfo = new PictureInfo();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_detail_board);

        mActivity = this;
        Bundle bundle = getIntent().getExtras();
        if( bundle != null )
        {
            if(bundle.containsKey(ARG_ZBL))
            {
                parcelZBL = bundle.getString(ARG_ZBL);
            }
            if( bundle.containsKey(ARG_MODE) )
            {
                isEdit = bundle.getBoolean(ARG_MODE);
            }
            if( bundle.containsKey(ARG_INDEX) )
            {
                mIndex = bundle.getInt(ARG_INDEX);
            }
            if( bundle.containsKey(ARG_SIGN_BORD_INFO) )
            {
                String gSignBoardInfo = bundle.getString(ARG_SIGN_BORD_INFO);
                Gson gson = new Gson();
                signBoardInfo = gson.fromJson(gSignBoardInfo,SignBoardInfo.class);
            }
        }

        inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        initial();
        initialValue();
        initialEvent();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if( !isEdit )
        {
            resetData();
        }
    }
    private void resetData()
    {
        ShareData.arLstSurveySignBoard = new ArrayList<SurveySignBoard>();
        ShareData.arLstSurveySignBoardOwner = new ArrayList<SurveySignBoardOwner>();
    }
    private View.OnClickListener onDeleteImage = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if( v.getId() == R.id.btnDelete1 )
                dialogDeleteImage(v.getId());
            else if( v.getId() == R.id.btnDelete2 )
                dialogDeleteImage(v.getId());
            else if( v.getId() == R.id.btnDelete3 )
                dialogDeleteImage(v.getId());
        }
    };
    private void dialogDeleteImage(final int Id)
    {
        utils.createPopup(DetailBoardActivity.this, "คุณต้องการลบรูป ใช่หรือไม่"
                , ""
                , "ใช่"
                , "ไม่ใช่"
                , new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteImage(Id);
                    }
                }
                , new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
    }
    private void deleteImage(int Id)
    {
        /*
        for (Map.Entry<Integer, Bitmap> entry : ShareData.arMapImageBitmap.entrySet()) {
            if (entry.getKey() > 300) {
                if( Id == R.id.btnDelete1 )
                {
                    if( entry.getKey() == PICK_IMAGE_ID_1 )
                    {
                        ShareData.arMapImageBitmap.remove(entry);
                        iv1.setImageDrawable(getResources().getDrawable(R.drawable.image_placeholder));
                        btnDelete1.setEnabled(false);
                    }
                }
                else if( Id == R.id.btnDelete2 )
                {
                    if( entry.getKey() == PICK_IMAGE_ID_2 )
                    {
                        ShareData.arMapImageBitmap.remove(entry);
                        iv2.setImageDrawable(getResources().getDrawable(R.drawable.image_placeholder));
                        btnDelete2.setEnabled(false);
                    }
                }
                else if( Id == R.id.btnDelete3 )
                {
                    if( entry.getKey() == PICK_IMAGE_ID_3 )
                    {
                        ShareData.arMapImageBitmap.remove(entry);
                        iv3.setImageDrawable(getResources().getDrawable(R.drawable.image_placeholder));
                        btnDelete3.setEnabled(false);
                    }
                }
            } else if (entry.getKey() > 200) {
                if( Id == R.id.btnDelete1 )
                {
                    if( entry.getKey() == PICK_IMAGE_ID_1 )
                    {
                        ShareData.arMapImageBitmap.remove(entry);
                        iv1.setImageDrawable(getResources().getDrawable(R.drawable.image_placeholder));
                        btnDelete1.setEnabled(false);
                    }
                }
                else if( Id == R.id.btnDelete2 )
                {
                    if( entry.getKey() == PICK_IMAGE_ID_2 )
                    {
                        ShareData.arMapImageBitmap.remove(entry);
                        iv2.setImageDrawable(getResources().getDrawable(R.drawable.image_placeholder));
                        btnDelete2.setEnabled(false);
                    }
                }
                else if( Id == R.id.btnDelete3 )
                {
                    if( entry.getKey() == PICK_IMAGE_ID_3 )
                    {
                        ShareData.arMapImageBitmap.remove(entry);
                        iv3.setImageDrawable(getResources().getDrawable(R.drawable.image_placeholder));
                        btnDelete3.setEnabled(false);
                    }
                }
            } else if (entry.getKey() > 100) {
                if( Id == R.id.btnDelete1 )
                {
                    if( entry.getKey() == PICK_IMAGE_ID_1 )
                    {
                        ShareData.arMapImageBitmap.remove(entry);
                        iv1.setImageDrawable(getResources().getDrawable(R.drawable.image_placeholder));
                        btnDelete1.setEnabled(false);
                    }
                }
                else if( Id == R.id.btnDelete2 )
                {
                    if( entry.getKey() == PICK_IMAGE_ID_2 )
                    {
                        ShareData.arMapImageBitmap.remove(entry);
                        iv2.setImageDrawable(getResources().getDrawable(R.drawable.image_placeholder));
                        btnDelete2.setEnabled(false);
                    }
                }
                else if( Id == R.id.btnDelete3 )
                {
                    if( entry.getKey() == PICK_IMAGE_ID_3 )
                    {
                        ShareData.arMapImageBitmap.remove(entry);
                        iv3.setImageDrawable(getResources().getDrawable(R.drawable.image_placeholder));
                        btnDelete3.setEnabled(false);
                    }
                }
            }
        }*/
        if( Id == R.id.btnDelete1 ) {
            iv1.setImageDrawable(getResources().getDrawable(R.drawable.image_placeholder));
            btnDelete1.setEnabled(false);
            pictureInfo.picture1 = null;
        }
        if( Id == R.id.btnDelete2 ) {
            iv2.setImageDrawable(getResources().getDrawable(R.drawable.image_placeholder));
            btnDelete2.setEnabled(false);
            pictureInfo.picture2 = null;
        }
        if( Id == R.id.btnDelete3 ) {
            iv3.setImageDrawable(getResources().getDrawable(R.drawable.image_placeholder));
            btnDelete3.setEnabled(false);
            pictureInfo.picture3 = null;
        }

    }
    private void initial()
    {
        vParcelOwner = (View)findViewById(R.id.layout_parcel_owner);

        btnSave = (Button)findViewById(R.id.btnSave);

        tvAddLandUse = (TextView)findViewById(R.id.tvAddLandUse);
        tvAddParcel = (TextView)findViewById(R.id.tvAddParcel);
        tvAddParcelOwner = (TextView)findViewById(R.id.tvParcelOwner);

        iv1 = (ImageView)findViewById(R.id.iv1);
        iv2 = (ImageView)findViewById(R.id.iv2);
        iv3 = (ImageView)findViewById(R.id.iv3);

        btnDelete1 = (Button)findViewById(R.id.btnDelete1);
        btnDelete2 = (Button)findViewById(R.id.btnDelete2);
        btnDelete3 = (Button)findViewById(R.id.btnDelete3);

        llParcelBenefitLand = (LinearLayout)findViewById(R.id.llParcelBenefitLand);
        llParcelLandUse = (LinearLayout)findViewById(R.id.llParcelLandUse);
        llLandInfo = (LinearLayout)findViewById(R.id.llLandInfo);

        llParcelBenefitLand.setVisibility(View.VISIBLE);

    }
    private void initialValue()
    {
        if(TextUtils.isEmpty(parcelZBL)) return;

        dbManager = new DatabaseManger();
        arLstLandInfo = dbManager.getLandInfo(parcelZBL);
        if( arLstLandInfo != null )
        {
            if( arLstLandInfo.size() <= 0 )
            {
                utils.createPopup(mActivity, "ไม่พบข้อมูล", "", "ตกลง", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mActivity.finish();
                    }
                });
                return ;
            }

            LandInfo landInfo = arLstLandInfo.get(0);
            if( landInfo != null )
            {
                DetailParcelLandInfo detailLand = new DetailParcelLandInfo(DetailBoardActivity.this , parcelZBL);
                llLandInfo.addView(detailLand.onCreateView());

                initialSurveySingBoard();
                initialSurveyParcelOwnerUse();
                initialParcelOwner();
            }
        }

        btnDelete1.setEnabled(false);
        btnDelete2.setEnabled(false);
        btnDelete3.setEnabled(false);



        if (mIndex < ShareData.arSignBoardBitmap.size() && mIndex != -1) {
            pictureInfo = ShareData.arSignBoardBitmap.get(mIndex);

            if (pictureInfo != null) {
                if (pictureInfo.picture1 != null) {
                    iv1.setImageBitmap(pictureInfo.picture1);
                    btnDelete1.setEnabled(false);
                }
                if (pictureInfo.picture2 != null) {
                    iv2.setImageBitmap(pictureInfo.picture2);
                    btnDelete2.setEnabled(false);
                }
                if (pictureInfo.picture3 != null) {
                    iv3.setImageBitmap(pictureInfo.picture3);
                    btnDelete3.setEnabled(false);
                }
            } else {
                pictureInfo = new PictureInfo();
            }

        }
    }
    private void initialSurveySingBoard()
    {
        if( dbManager == null || arLstLandInfo==null ) return;
        if( arLstLandInfo.size() <= 0 ) return;

//        ArrayList<SurveySignBoard> arLstSurveySignBoard = dbManager.getSurveySignBoard(
//                getLandInfo().getAsString(PARCEL_ID),
//                getLandInfo().getAsString(PARCEL_SER));

        if( isEdit )
        {
            ArrayList<SurveySignBoard> arLstSurveySignBoard = signBoardInfo.arLstSurveySignBoard;
            for( int i = 0; i < arLstSurveySignBoard.size(); i++ )
            {
                AddSignBoard(arLstSurveySignBoard.get(i) , -1);
            }
        }
    }
    private void initialSurveyParcelOwnerUse()
    {
        if( dbManager == null || arLstLandInfo==null ) return;
        if( arLstLandInfo.size() <= 0 ) return;

//        ArrayList<SurveySignBoardOwner> arLstSurveyParcelOwner = dbManager.getSurveySignBoardOwner(
//                getLandInfo().getAsString(PARCEL_ID),
//                getLandInfo().getAsString(PARCEL_SER));
//        ArrayList<SurveySignBoardOwner> arLstSurveyParcelOwner = dbManager.getSurveySignBoardOwner( edtSignNo.getText().toString());

        if( isEdit )
        {
            ArrayList<SurveySignBoardOwner> arLstSurveyParcelOwner = signBoardInfo.arLstSurveySignBoardOwner;
            for( int n=0;n<arLstSurveyParcelOwner.size();n++ )
            {
                AddParcelBenefitLand(arLstSurveyParcelOwner.get(n) , "");
            }
        }
    }

    private void initialParcelOwner()
    {
        if( dbManager == null || arLstLandInfo==null ) return;
        if( arLstLandInfo.size() <= 0 ) return;

        ArrayList<SurveyParcelOwner> surveyParcelOwner = dbManager.getSurveyParcelOwner(
                getLandInfo().getAsString(PARCEL_ID),getLandInfo().getAsString(PARCEL_SER)
        );

        if( surveyParcelOwner == null ) return;
        if( surveyParcelOwner.size() <= 0 ) return;

        surveySignBoardOwner = new SurveySignBoardOwner();
        surveySignBoardOwner.CustomerID = surveyParcelOwner.get(0).CustomerID;
        surveySignBoardOwner.Parcel_ID = surveyParcelOwner.get(0).Parcel_ID;
        surveySignBoardOwner.LAST_NAME = surveyParcelOwner.get(0).LAST_NAME;
        surveySignBoardOwner.Parcel_Tmb = getLandInfo().getAsString(PARCEL_TMB);

        if( TextUtils.isEmpty(surveyParcelOwner.get(0).FIRST_NAME) ) {
            surveySignBoardOwner.fullName = surveyParcelOwner.get(0).fullName;
            surveySignBoardOwner.FIRST_NAME = surveyParcelOwner.get(0).fullName;
        }
        else {
            surveySignBoardOwner.FIRST_NAME = surveyParcelOwner.get(0).FIRST_NAME;
        }
        surveySignBoardOwner.HN_ID = surveyParcelOwner.get(0).PrefixName;
        surveySignBoardOwner.address = surveyParcelOwner.get(0).address;

        addParcelOwner(surveySignBoardOwner , TAG_OWNER);
    }

    private void removeParcelBenefitLand(int index)
    {
        if( llParcelBenefitLand == null ) return;
        if( llParcelBenefitLand.getChildCount() <= 0 ) return;
        if( (llParcelBenefitLand.getChildCount()-1) < index ) return;
        if( (ShareData.arLstSurveySignBoardOwner.size() < index )) return;

        //set CheckBox = false
        if( ShareData.arLstSurveySignBoardOwner.get(index).CustomerID.equals(surveySignBoardOwner.CustomerID))
        {
            {
                chkLandUse.setOnCheckedChangeListener(null);
                chkLandUse.setChecked(false);
                chkLandUse.setOnCheckedChangeListener(onCheckChange);
            }
        }

        setEnableAddParcel(true);
        llParcelBenefitLand.removeViewAt(index);
        ShareData.arLstSurveySignBoardOwner.remove(index);
    }
    private void AddParcelBenefitLand(SurveySignBoardOwner surveyParcelOwner , String tag)
    {
        if( dbManager == null || arLstLandInfo==null ) return;
        if( arLstLandInfo.size() <= 0 ) return;

        final View vParcelOwner = inflater.inflate(R.layout.layout_parcel_owner,null);
        vParcelOwner.setTag(tag);
        vParcelOwner.setVisibility(View.VISIBLE);

        Button btnDelete = (Button)vParcelOwner.findViewById(R.id.btnDelete);
        TextView tvFullName = (TextView)vParcelOwner.findViewById(R.id.tvFullName);
        TextView tvAddress = (TextView)vParcelOwner.findViewById(R.id.tvAddress);
        TextView tvTopic = (TextView)vParcelOwner.findViewById(R.id.tvTopic);
        View vTopic = (View)vParcelOwner.findViewById(R.id.vTopic);
        View vSpace = (View)vParcelOwner.findViewById(R.id.vSpace);
        CheckBox chkLand = (CheckBox) vParcelOwner.findViewById(R.id.chkLandUse);

        chkLand.setVisibility(View.GONE);
        tvTopic.setVisibility(View.GONE);
        vTopic.setVisibility(View.GONE);
        vSpace.setVisibility(View.GONE);

        tvFullName.setText(surveyParcelOwner.fullName);
        tvAddress.setText(surveyParcelOwner.address);

        btnDelete.setTag(tag);
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(v.getTag().toString()))
                {
                    for( int n =0; n < llParcelBenefitLand.getChildCount(); n++ )
                    {
                        if( llParcelBenefitLand.getChildAt(n).getTag().equals( TAG_OWNER ) )
                        {
                            chkLandUse.setOnCheckedChangeListener(null);
                            chkLandUse.setChecked(false);
                            chkLandUse.setOnCheckedChangeListener(onCheckChange);

                            removeParcelBenefitLand(n);
                            break;
                        }
                    }
                }
                else
                {
                    LinearLayout llRootParcelOwner = (LinearLayout) vParcelOwner.findViewById(R.id.llRootParcelOwner);
                    int index = ((ViewGroup) llRootParcelOwner.getParent()).indexOfChild(llRootParcelOwner);

                    removeParcelBenefitLand(index);
                }
            }
        });

        setEnableAddParcel(false);

        llParcelBenefitLand.addView(vParcelOwner);
        ShareData.arLstSurveySignBoardOwner.add(surveyParcelOwner);

//        if (isEdit) {
//            pictureInfo = ShareData.arSignBoardImageBitmap.get(parcelZBL+"_"+surveyParcelOwner.Sign_No);
//        }
    }
    private void AddSignBoard( SurveySignBoard signBoard , int index )
    {
        //for( int n=0;n<arLstSurveySignBoard.size();n++ )
        {
            View vBuild = null;
            if( index == -1 )
            {
                vBuild = inflater.inflate(R.layout.layout_sign_use , null);
            }
            else
            {
                vBuild = (View)llParcelLandUse.getChildAt(0);
            }

            final View vSignBoardUse = vBuild;

            EditText edtSignHeight = (EditText)vSignBoardUse.findViewById(R.id.edtSignHeight);
            EditText edtSignWidth = (EditText)vSignBoardUse.findViewById(R.id.edtSignWidth);
            EditText edtSignSide = (EditText)vSignBoardUse.findViewById(R.id.edtSignSide);
            EditText edtSignText = (EditText)vSignBoardUse.findViewById(R.id.edtSignText);
            EditText edtSignNo = (EditText)vSignBoardUse.findViewById(R.id.edtSignNo);
            Button btnEdit = (Button)vSignBoardUse.findViewById(R.id.btnEdit);
            Button btnDelete = (Button)vSignBoardUse.findViewById(R.id.btnDelete);

            btnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //if(v.getTag() != null)
                    {
//                        int index = llParcelLandUse.indexOfChild(vSignBoardUse);
                        if( isEdit )
                            showDialogAddSignBoardUse(mIndex);
                        else
                            showDialogAddSignBoardUse(0);

                        setEnableLandUse(false);
                    }
                }
            });

            btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final int index = llParcelLandUse.indexOfChild(vSignBoardUse);
                    final SurveySignBoard signBoard = ShareData.arLstSurveySignBoard.get(index);

                    String nameBoard = DatabaseManger.getNameBuilding(signBoard.Sign_Type);
                    String Msg = "คุณต้องการลบ การทำประโยชน์ของป้าย '"+nameBoard+"' " +
                            "ใช่หรือไม่ ?";
                    utils.createPopup(mActivity, Msg, "", "ใช่", "ไม่ใช่",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    setEnableLandUse(true);

                                    ShareData.arLstSurveySignBoard.remove(index);
                                    llParcelLandUse.removeViewAt(index);
                                }
                            },
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                }
            });

            Spinner spnSignType = (Spinner)vSignBoardUse.findViewById(R.id.spnSignType);

            SurveySignBoard surveyBoard = signBoard;
            edtSignHeight.setText(surveyBoard.Sign_Higth);
            edtSignWidth.setText(surveyBoard.Sign_Width);
            edtSignSide.setText(surveyBoard.Sign_Side);
            edtSignText.setText(surveyBoard.Sign_txt);
            edtSignNo.setText(surveyBoard.Sign_No);

            String typeName = dbManager.getTypeSignBoard(surveyBoard.Sign_Type);
            final ArrayAdapter<String> arAdapter = dbManager.getAdapter(mActivity,dbManager.getTypeSignBoard());
            spnSignType.setAdapter( arAdapter );
            spnSignType.setSelection(0);

            spnSignType.setEnabled(false);
            edtSignHeight.setEnabled(false);
            edtSignWidth.setEnabled(false);
            edtSignSide.setEnabled(false);
            edtSignText.setEnabled(false);
            edtSignNo.setEnabled(false);

            for( int i=0;i<arAdapter.getCount();i++ )
            {
                if( arAdapter.getItem(i).equals(typeName) )
                {
                    spnSignType.setSelection(i);
                }
            }

            if( index == -1 ) {
                setEnableLandUse(false);

                llParcelLandUse.addView(vSignBoardUse);
                ShareData.arLstSurveySignBoard.add(signBoard);
            }
            else {
                ShareData.arLstSurveySignBoard.set(0, surveyBoard);
            }
        }
    }

    private void initialEvent()
    {
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSave();

            }
        });
        tvAddParcel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogAddParcel();
            }
        });
        tvAddLandUse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogAddSignBoardUse(-1);
            }
        });

        iv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent chooseImageIntent = ImagePicker.getPickImageIntent(mActivity);
                startActivityForResult(chooseImageIntent, PICK_IMAGE_ID_1);
            }
        });

        iv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent chooseImageIntent = ImagePicker.getPickImageIntent(mActivity);
                startActivityForResult(chooseImageIntent, PICK_IMAGE_ID_2);
            }
        });
        iv3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent chooseImageIntent = ImagePicker.getPickImageIntent(mActivity);
                startActivityForResult(chooseImageIntent, PICK_IMAGE_ID_3);
            }
        });

        btnDelete1.setOnClickListener(onDeleteImage);
        btnDelete2.setOnClickListener(onDeleteImage);
        btnDelete3.setOnClickListener(onDeleteImage);

    }
    private void showDialogAddParcel()
    {
        dlgAddParcelUse = new Dialog(mActivity);
        dlgAddParcelUse.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dlgAddParcelUse.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dlgAddParcelUse.setContentView(R.layout.dlg_add_parcel_use);
        dlgAddParcelUse.setCancelable(true);


        final AutoCompleteTextView edtPrefix = (AutoCompleteTextView)dlgAddParcelUse.findViewById(R.id.edtPrefix);
        final AutoCompleteTextView edtftName = (AutoCompleteTextView)dlgAddParcelUse.findViewById(R.id.edtftName);
        final AutoCompleteTextView edtlstName = (AutoCompleteTextView)dlgAddParcelUse.findViewById(R.id.edtlstName);
        final EditText edtBuildingNum = (EditText)dlgAddParcelUse.findViewById(R.id.edtBuildingNum);
        final TextView tvTopic = (TextView)dlgAddParcelUse.findViewById(R.id.tvTopic);

        tvTopic.setText("เพิ่มผู้ทำประโยชน์ป้าย");
        ArrayAdapter<String> adapter = DatabaseManger.getParcelOwnerAdapter(DetailBoardActivity.this);
        edtftName.setAdapter(adapter);
//        edtPrefix.setAdapter(adapter);
        edtlstName.setAdapter(adapter);

        Button btnSave = (Button)dlgAddParcelUse.findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if( TextUtils.isEmpty(edtPrefix.getText().toString()) ||
                        TextUtils.isEmpty(edtftName.getText().toString()) ||
                        TextUtils.isEmpty(edtlstName.getText().toString()) ||
                        TextUtils.isEmpty(edtBuildingNum.getText().toString())  )
                {
                    utils.createPopup(DetailBoardActivity.this , "กรุณาระบุข้อมูลให้ครบ" , "");
                    return;
                }

                String prefix = edtPrefix.getText().toString();
                String ftName = edtftName.getText().toString();
                String lstName = edtlstName.getText().toString();
                String buildNum = edtBuildingNum.getText().toString();

                SurveySignBoardOwner benefitLand = new SurveySignBoardOwner();
                benefitLand.NUM = buildNum;
                benefitLand.MOO = arLstLandInfo.get(0).Moo;
                benefitLand.HN_ID = prefix;
                benefitLand.CustomerID = buildNum;
                benefitLand.PrefixName = prefix;
                benefitLand.FIRST_NAME = ftName;
                benefitLand.LAST_NAME = lstName ;
                benefitLand.fullName = prefix+" "+ftName+" "+lstName ;
                benefitLand.address = "บ้านเลขที่ "+buildNum;;

                AddParcelBenefitLand(benefitLand,"");

                dlgAddParcelUse.dismiss();
            }
        });
        Button btnCancel = (Button)dlgAddParcelUse.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dlgAddParcelUse.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dlgAddParcelUse.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dlgAddParcelUse.show();
        dlgAddParcelUse.getWindow().setAttributes(lp);
    }

    private void showDialogAddSignBoardUse(final int index)
    {
        dlgAddSignUse = new Dialog(mActivity);
        dlgAddSignUse.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dlgAddSignUse.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dlgAddSignUse.setContentView(R.layout.dlg_add_sign);
        dlgAddSignUse.setCancelable(true);

        final EditText edtSignNo = (EditText)dlgAddSignUse.findViewById(R.id.edtSignNo);
        final EditText edtSignText = (EditText)dlgAddSignUse.findViewById(R.id.edtSignText);
        final EditText edtSignWidth = (EditText)dlgAddSignUse.findViewById(R.id.edtSignWidth);
        final EditText edtSignHeight = (EditText)dlgAddSignUse.findViewById(R.id.edtSignHeight);
        final EditText edtSignSide = (EditText)dlgAddSignUse.findViewById(R.id.edtSignSide);
        final Spinner spnSignType = (Spinner) dlgAddSignUse.findViewById(R.id.spnSignType);

        final ArrayAdapter<String> arAdapter = dbManager.getAdapter(mActivity,dbManager.getTypeSignBoard());
        spnSignType.setAdapter( arAdapter );
        spnSignType.setSelection(0);

        if( isEdit )
        {
            //Mode Edit
            SurveySignBoard surveySignBoard = ShareData.arLstSurveySignBoard.get(0);
            edtSignNo.setText(surveySignBoard.Sign_No);
            edtSignText.setText(surveySignBoard.Sign_txt);
            edtSignWidth.setText(surveySignBoard.Sign_Width);
            edtSignHeight.setText(surveySignBoard.Sign_Higth);
            edtSignSide.setText(surveySignBoard.Sign_Side);

            String typeName = dbManager.getTypeSignBoard(surveySignBoard.Sign_Type);
            for( int i=0;i<arAdapter.getCount();i++ )
            {
                if( arAdapter.getItem(i).equals(typeName) )
                {
                    spnSignType.setSelection(i);
                }
            }
        }

        Button btnSave = (Button)dlgAddSignUse.findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if( TextUtils.isEmpty(edtSignText.getText().toString()) ||
                        TextUtils.isEmpty(edtSignWidth.getText().toString()) ||
                        TextUtils.isEmpty(edtSignHeight.getText().toString()) ||
                        TextUtils.isEmpty(edtSignSide.getText().toString())  )
                {
                    utils.createPopup(DetailBoardActivity.this , "กรุณาระบุข้อมูลให้ครบ" , "");
                    return;
                }

                SurveySignBoard surveyBoard = new SurveySignBoard();
                surveyBoard.Parcel_ID = getLandInfo().getAsString(PARCEL_ID);
                surveyBoard.Parcel_Ser = getLandInfo().getAsString(PARCEL_SER);
                surveyBoard.Parcel_Tmb = getLandInfo().getAsString(PARCEL_TMB);
                surveyBoard.Sign_Higth = edtSignHeight.getText().toString();
                surveyBoard.Sign_Width = edtSignWidth.getText().toString();
                surveyBoard.Sign_Side = edtSignSide.getText().toString();
                surveyBoard.Sign_txt = edtSignText.getText().toString();
                surveyBoard.Sign_Type = DatabaseManger.getIdTypeSignBoard(spnSignType
                        .getSelectedItem().toString());

                AddSignBoard(surveyBoard , index);

                dlgAddSignUse.dismiss();
            }
        });
        Button btnCancel = (Button)dlgAddSignUse.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dlgAddSignUse.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dlgAddSignUse.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dlgAddSignUse.show();
        dlgAddSignUse.getWindow().setAttributes(lp);
    }

    private ContentValues getLandInfo()
    {
        ContentValues cv = new ContentValues();
        if( arLstLandInfo.size() > 0 )
        {
            cv.put(PARCEL_ID,arLstLandInfo.get(0).Parcel_Id);
            cv.put(PARCEL_SER,arLstLandInfo.get(0).Parcel_Ser);
            cv.put(PARCEL_TMB,arLstLandInfo.get(0).Tmb);
        }
        else {
            cv.put(PARCEL_ID,"");
            cv.put(PARCEL_SER,"");
            cv.put(PARCEL_TMB,"");
        }

        return cv;
    }

    private void addParcelOwner(final SurveySignBoardOwner surveyParcelOwner , final String tag)
    {
        Button btnEdit = (Button)vParcelOwner.findViewById(R.id.btnEdit);
        btnEdit.setVisibility(View.GONE);

        Button btnDelete = (Button)vParcelOwner.findViewById(R.id.btnDelete);
        btnDelete.setVisibility(View.GONE);

        TextView tvFullName = (TextView)vParcelOwner.findViewById(R.id.tvFullName);
        TextView tvAddress = (TextView)vParcelOwner.findViewById(R.id.tvAddress);

        chkLandUse = (CheckBox) vParcelOwner.findViewById(R.id.chkLandUse);
        chkLandUse.setText("ผู้ทำประโยชน์ป้าย");
        chkLandUse.setOnCheckedChangeListener(onCheckChange);

        tvFullName.setText(surveyParcelOwner.fullName);
        tvAddress.setText(surveyParcelOwner.address);

        if( llParcelBenefitLand.getChildCount() <= 0 )
            setEnableCheckBox(true);
        else
            setEnableCheckBox(false);

    }

    private CompoundButton.OnCheckedChangeListener onCheckChange = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if( isChecked )
            {
//                AddParcelBenefitLand(surveyParcelOwner , "");

                SurveySignBoardOwner benefitLand = new SurveySignBoardOwner();
                benefitLand.NUM = surveySignBoardOwner.NUM;
                benefitLand.MOO = arLstLandInfo.get(0).Moo;
                benefitLand.HN_ID = surveySignBoardOwner.HN_ID;
                benefitLand.CustomerID = surveySignBoardOwner.CustomerID;
                benefitLand.PrefixName = surveySignBoardOwner.PrefixName;
                benefitLand.FIRST_NAME = surveySignBoardOwner.FIRST_NAME;
                benefitLand.LAST_NAME = surveySignBoardOwner.LAST_NAME ;
                benefitLand.fullName = surveySignBoardOwner.fullName ;
                benefitLand.address = surveySignBoardOwner.address;
                AddParcelBenefitLand(benefitLand , TAG_OWNER);
            }
            else
            {
                int index = -1;
                for( int n =0; n < llParcelBenefitLand.getChildCount(); n++ )
                {
                    if( llParcelBenefitLand.getChildAt(n).getTag().equals(TAG_OWNER) )
                    {
                        index = n;
                        break;
                    }
                }

                if( index > -1 )
                    removeParcelBenefitLand(index);
            }
        }
    };
    private void onSave()
    {
        if( ShareData.arLstSurveySignBoardOwner.size() <= 0 )
        {
            utils.createPopup(DetailBoardActivity.this
                    , "กรุณาเพิ่ม ผู้ทำประโยชน์ป้าย"
                    , ""
                    , "ตกลง"
                    , new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

            return;
        }
        if( ShareData.arLstSurveySignBoard.size() <= 0 )
        {
            utils.createPopup(DetailBoardActivity.this
                    , "กรุณาเพิ่ม การทำประโยชน์ป้าย"
                    , ""
                    , "ตกลง"
                    , new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

            return;
        }

        SignBoardInfo signBoardInfo = new SignBoardInfo();
//        for( int n=0;n<ShareData.arLstSurveySignBoard.size(); n++ )
//        {
        if (ShareData.arLstSurveySignBoard.size() > 0) {
            SurveySignBoard surveySignBoard = ShareData.arLstSurveySignBoard.get(ShareData.arLstSurveySignBoard.size() - 1);
            signBoardInfo.arLstSurveySignBoard.add(surveySignBoard);
        }
//        }
//        for( int n=0;n<ShareData.arLstSurveySignBoardOwner.size(); n++ )
//        {
        if (ShareData.arLstSurveySignBoardOwner.size() > 0) {
            SurveySignBoardOwner surveySignBoardOwner = ShareData.arLstSurveySignBoardOwner.get(ShareData.arLstSurveySignBoardOwner.size()-1);

/*        if( ShareData.arLstSurveySignBoard.size() > 0 )
        {
            SurveySignBoard surveySignBoard = ShareData.arLstSurveySignBoard.get(0);
            signBoardInfo.arLstSurveySignBoard.add(surveySignBoard);
        }
//        for( int n=0;n<ShareData.arLstSurveySignBoardOwner.size(); n++ )
        if( ShareData.arLstSurveySignBoardOwner.size() > 0 )
        {
            SurveySignBoardOwner surveySignBoardOwner = ShareData.arLstSurveySignBoardOwner.get(0);
*/
            signBoardInfo.arLstSurveySignBoardOwner.add(surveySignBoardOwner);
        }
//        }

        if( !isEdit )
        {
            //add mode
            ShareData.arLstSignBoardInfo.add(signBoardInfo);
            ShareData.arSignBoardBitmap.add(pictureInfo);
        }
        else
        {
            //edit mode
            ShareData.arLstSignBoardInfo.set(mIndex,signBoardInfo);
            ShareData.arSignBoardBitmap.set(mIndex,pictureInfo);
//            ShareData.arSignBoardImageBitmap.put(parcelZBL+"_"+surveySignBoardOwner.Sign_No,pictureInfo);
        }
//        ShareData.arSignBoardNewImageBitmap.put(parcelZBL+"_"+surveySignBoardOwner.Parcel_ID,pictureInfo);

        finish();
        resetData();
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Bitmap bmp = null;
        switch(requestCode) {
            case PICK_IMAGE_ID_1:
                bmp = ImagePicker.getImageFromResult(mActivity, resultCode, data);

                if( bmp != null && !bmp.toString().equals("")) {
                    iv1.setImageBitmap(bmp);
//                    ShareData.arMapImageBitmap.put(PICK_IMAGE_ID_1,bmp);
                    pictureInfo.picture1 = bmp;
                    btnDelete1.setEnabled(true);

                }
                break;
            case PICK_IMAGE_ID_2:
                bmp = ImagePicker.getImageFromResult(mActivity, resultCode, data);

                if( bmp != null && !bmp.toString().equals("")) {
                    iv2.setImageBitmap(bmp);
//                    ShareData.arMapImageBitmap.put(PICK_IMAGE_ID_2,bmp);
                    pictureInfo.picture2 = bmp;
                    btnDelete2.setEnabled(true);
                }
                break;
            case PICK_IMAGE_ID_3:
                bmp = ImagePicker.getImageFromResult(mActivity, resultCode, data);

                if( bmp != null && !bmp.toString().equals("")) {
                    iv3.setImageBitmap(bmp);
//                    ShareData.arMapImageBitmap.put(PICK_IMAGE_ID_3,bmp);
                    pictureInfo.picture3 = bmp;
                    btnDelete3.setEnabled(true);
                }
                break;

            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }
    private void setEnableLandUse(boolean isEnable)
    {
        tvAddLandUse.setEnabled(isEnable);
        if( isEnable )
        {
            tvAddLandUse.setVisibility(View.VISIBLE);
        }
        else
        {
            tvAddLandUse.setVisibility(View.INVISIBLE);
        }
    }

    private void setEnableAddParcel(boolean isEnable)
    {
        setEnableCheckBox(isEnable);

        tvAddParcel.setEnabled(isEnable);
        if( isEnable )
            tvAddParcel.setVisibility(View.VISIBLE);
        else
            tvAddParcel.setVisibility(View.INVISIBLE);
    }
    private void setEnableCheckBox(boolean isEnable)
    {
        if( chkLandUse != null )
            chkLandUse.setEnabled(isEnable);
    }
}
