package com.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.model.SubBlockParcel;
import com.survay.R;

import java.util.ArrayList;

/**
 * Created by user on 2/6/2016.
 */
public class SubBlockAdapter extends RecyclerView.Adapter<SubBlockAdapter.ViewHolder> {

    private OnItemClickListener mOnItemClickListener;
    private Activity mActivity = null;
    private ArrayList<SubBlockParcel> m_arLstContact = new ArrayList<SubBlockParcel>();

    Boolean mIsSelectCustom = false;

    public interface OnItemClickListener {
        public void onItemClick(View view, int position, SubBlockParcel contact);
    }


    public void setListenerItemClick( OnItemClickListener onItemListener )
    {
        mOnItemClickListener = onItemListener;
    }

    public SubBlockAdapter(ArrayList<SubBlockParcel> items, Activity activity) {

        mActivity = activity;
        m_arLstContact =items;
    }

    public void setItems(ArrayList<SubBlockParcel> items)
    {
        m_arLstContact =items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View v = null;
        v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_block_parcel, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int i) {

        if( i == 0 )
            viewHolder.vHeader.setVisibility(View.VISIBLE);
        else
            viewHolder.vHeader.setVisibility(View.GONE);

        if( m_arLstContact.get(i).status == 1 )
        {
            viewHolder.tvBlockParcel.setTextColor(mActivity.getResources().getColor(R.color.green));
            viewHolder.tvFullName.setTextColor(mActivity.getResources().getColor(R.color.green));
        }
        else
        {
            viewHolder.tvBlockParcel.setTextColor(mActivity.getResources().getColor(R.color.Black));
            viewHolder.tvFullName.setTextColor(mActivity.getResources().getColor(R.color.Black));
        }

        viewHolder.tvBlockParcel.setText( m_arLstContact.get(i).blockParcel);
        viewHolder.tvFullName.setText( m_arLstContact.get(i).nameOwner);

        viewHolder.llRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( mOnItemClickListener != null )
                {
                    {
                        mOnItemClickListener.onItemClick(viewHolder.llRoot , i , m_arLstContact.get(i));
                    }
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return m_arLstContact.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        private final TextView tvBlockParcel;
        private final TextView tvFullName;
        private final View vHeader;
        private final LinearLayout llRoot;

        ViewHolder(View v) {
            super(v);
            tvBlockParcel = (TextView) v.findViewById(R.id.tvBlockParcel);
            tvFullName = (TextView) v.findViewById(R.id.tvFullName);
            vHeader = (View) v.findViewById(R.id.vHeader);
            llRoot = (LinearLayout) v.findViewById(R.id.llRoot);
        }
    }
}
