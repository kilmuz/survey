package com.Object;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.ObjectDatabase.DatabaseManger;
import com.model.LandInfo;
import com.survay.R;

import java.util.ArrayList;

/**
 * Created by rungrawee.w on 16/08/2016.
 */
public class DetailParcelLandInfo {

    Activity activity = null;
    LayoutInflater inflater = null;

    String parcelZBL = "";

    public DetailParcelLandInfo(Activity activity , String parcelZbl) {
        this.activity = activity;
        this.parcelZBL = parcelZbl;

        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public View onCreateView()
    {
        LinearLayout llRoot = new LinearLayout(activity);
        llRoot.setOrientation(LinearLayout.VERTICAL);
        llRoot.setLayoutParams( new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));

        View vInfoLand = inflater.inflate(R.layout.layout_info_land,null);
        View vInfoParcel = inflater.inflate(R.layout.layout_info_parcel,null);

        EditText edtLandScripType = (EditText)vInfoLand.findViewById(R.id.edtLandScripType);
        EditText edtParcelId = (EditText)vInfoLand.findViewById(R.id.edtParcelId);
        EditText edtParcelNum = (EditText)vInfoLand.findViewById(R.id.edtParcelNum);
        EditText edtParcelSer = (EditText)vInfoLand.findViewById(R.id.edtParcelSer);
        EditText edtParcelRai = (EditText)vInfoLand.findViewById(R.id.edtParcelRai);
        EditText edtParcelNang = (EditText)vInfoLand.findViewById(R.id.edtParcelNang);
        EditText edtParcelVa = (EditText)vInfoLand.findViewById(R.id.edtParcelVa);

        EditText edtZBL = (EditText)vInfoParcel.findViewById(R.id.edtZBL);
        EditText edtMoo = (EditText)vInfoParcel.findViewById(R.id.edtMoo);
        EditText edtParcelTmb = (EditText)vInfoParcel.findViewById(R.id.edtParcelTmb);

        DatabaseManger dbManager = new DatabaseManger();
        ArrayList<LandInfo> arLstLandInfo = dbManager.getLandInfo(parcelZBL);

        if(TextUtils.isEmpty(parcelZBL)) return null;
        if( arLstLandInfo != null )
        {
            if( arLstLandInfo.size() <= 0 )
            {
                return null;
            }

            com.model.LandInfo landInfo = arLstLandInfo.get(0);
            if( landInfo != null )
            {
                edtLandScripType.setText(landInfo.LandScripType);
                edtParcelId.setText(landInfo.Parcel_Id);
                edtParcelNum.setText(landInfo.Parcel_Num);
                edtParcelSer.setText(landInfo.Parcel_Ser);
                edtParcelRai.setText(landInfo.Parcel_Rai);
                edtParcelNang.setText(landInfo.Parcel_Nang);
                edtParcelVa.setText(landInfo.Parcel_Va);

                edtZBL.setText(landInfo.ZBL);
                edtMoo.setText(landInfo.Moo);
                edtParcelTmb.setText(DatabaseManger.getNameDisSub(landInfo.Tmb));
            }
        }

        llRoot.addView(vInfoLand);
        llRoot.addView(vInfoParcel);
        return llRoot;
    }

}
