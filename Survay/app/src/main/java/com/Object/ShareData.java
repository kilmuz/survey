package com.Object;


import android.graphics.Bitmap;

import com.ObjectDatabase.DataBase;
import com.model.BuildingInfo;
import com.model.LandUse;
import com.model.ParcelInfo;
import com.model.PictureInfo;
import com.model.Profile;
import com.model.SignBoardInfo;
import com.model.SubBlockParcel;
import com.model.SurveyBuilding;
import com.model.SurveyBuildingOwner;
import com.model.SurveyParcelOwner;
import com.model.SurveySignBoard;
import com.model.SurveySignBoardOwner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by user on 22/4/2558.
 */
public class ShareData
{
    static ShareData instance;

    static Profile mProfile = null;
    public static ArrayList<LandUse> arLstLandUse = new ArrayList<LandUse>();
    public static ArrayList<SurveyParcelOwner> arLstParcelOwner = new ArrayList<SurveyParcelOwner>();
    public static ArrayList<SurveySignBoard> arLstSurveySignBoard = new ArrayList<SurveySignBoard>();
    public static ArrayList<SurveySignBoardOwner> arLstSurveySignBoardOwner = new ArrayList<SurveySignBoardOwner>();
    public static ArrayList<SurveyBuilding> arLstSurveyBuilding = new ArrayList<SurveyBuilding>();
    public static ArrayList<SurveyBuildingOwner> arLstSurveyBuildingOwner = new ArrayList<SurveyBuildingOwner>();
    public static ArrayList<SubBlockParcel> arLstSubBlockParcel = new ArrayList<SubBlockParcel>();
    public static Map<Integer, Bitmap> arMapImageBitmap = new HashMap<>();
    public static Map<String, PictureInfo> arSignBoardImageBitmap = new HashMap<>();
    public static Map<String, PictureInfo> arBuildingImageBitmap = new HashMap<>();

    public static ArrayList<PictureInfo> arSignBoardBitmap = new ArrayList<PictureInfo>();

    public static ArrayList<PictureInfo> arBuildingBitmap = new ArrayList<PictureInfo>();

    public static ArrayList<BuildingInfo> arLstBuildingInfo = new ArrayList<BuildingInfo>();
    public static ArrayList<SignBoardInfo> arLstSignBoardInfo = new ArrayList<SignBoardInfo>();
    public static ArrayList<ParcelInfo> arLstParcelInfo = new ArrayList<ParcelInfo>();

    public static String PATH_PIC_SUB_FIRST = "";
    public static String PATH_PIC_SUB_SECOND = "";

    static DataBase mDB = null;
    static
    {
        instance = new ShareData();
    }
    public static ShareData getInstance()
    {
        return ShareData.instance;
    }

    public static void setDB( DataBase db )
    {
        mDB = db;
    }
    public static DataBase getDB()
    {
        return mDB;
    }



}
