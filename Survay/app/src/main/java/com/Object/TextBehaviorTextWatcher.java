package com.Object;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by user on 3/23/2016.
 */
public class TextBehaviorTextWatcher implements TextWatcher {
    View view = null;
    Boolean isEdiging = false;

    public void TextBehaviorTextWatcher(View v)
    {
        this.view = v;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if(isEdiging) return;
        isEdiging = true;

        if( s.toString().trim().length() > 0 )
        {
            String str = s.toString().replaceAll( "[^\\d]", "" );
            double s1 = Double.parseDouble(str);

            NumberFormat nf2 = NumberFormat.getInstance(Locale.ENGLISH);
            ((DecimalFormat)nf2).applyPattern("$ ###,###.###");
            s.replace(0, s.length(), nf2.format(s1));
        }

        isEdiging = false;
    }
}
