package com.Object;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.IBinder;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.Settings.Secure;
import android.support.v4.content.CursorLoader;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.survay.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class utils
{
    public static ArrayList<String> nameOfEvent = new ArrayList<String>();
    public static ArrayList<String> startDates = new ArrayList<String>();
    public static ArrayList<String> endDates = new ArrayList<String>();
    public static ArrayList<String> descriptions = new ArrayList<String>();
    static int  countUpdate=0;
    public static String SEND_TO_SERVER = "send to server";
    public static String SEND_TO_SQLITE = "send to SQLITE";
    public static int TYPE_BUILDING = 1;
    public static int TYPE_SIGN_BOARD = 2;
    public static int TYPE_LAND_USE = 3;
    public static int MODE_INSERT = 0;
    public static int MODE_UPDATE = 1;
    public static int MODE_DELETE = 2;

    public static void closeKeyboard(Context c, IBinder windowToken) {
        InputMethodManager mgr = (InputMethodManager) c.getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(windowToken, 0);
    }

    public static String getConvertTimeStampToDateTime( long TimeStamp )
    {
        java.util.Date time=new java.util.Date((long)TimeStamp); //convert timestamp to date

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(TimeStamp);
        Date date = calendar.getTime();
        String language = "th";
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MMM/yyyy" , new Locale(language.toLowerCase(Locale.getDefault())) );

        String sDate = dateFormat.format(date);
        String[] split = sDate.split("/");
        int nYears = Integer.valueOf(split[2]);
        nYears += 543;

        sDate = split[0]+" "+split[1]+" "+String.valueOf(nYears).substring(2,4);
        try
        {
            return sDate;
        }
        catch (Exception ex)
        {
            return ex.toString();
        }
    }
    public static Bitmap takeScreenShot(Activity activity)
    {
        View view = activity.getWindow().getDecorView();
        view.setDrawingCacheEnabled(true);
        view.buildDrawingCache();
        Bitmap b1 = view.getDrawingCache();
        Rect frame = new Rect();
        activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(frame);
        int statusBarHeight = frame.top;
        int width = activity.getWindowManager().getDefaultDisplay().getWidth();
        int height = activity.getWindowManager().getDefaultDisplay().getHeight();

        Bitmap b = Bitmap.createBitmap(b1, 0, statusBarHeight, width, height - statusBarHeight);
        view.destroyDrawingCache();
        return b;
    }
    public static Bitmap fastBlur(Bitmap sentBitmap, int radius) {
        Bitmap bitmap = sentBitmap.copy(sentBitmap.getConfig(), true);

        if (radius < 1) {
            return (null);
        }

        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        int[] pix = new int[w * h];
        Log.e("pix", w + " " + h + " " + pix.length);
        bitmap.getPixels(pix, 0, w, 0, 0, w, h);

        int wm = w - 1;
        int hm = h - 1;
        int wh = w * h;
        int div = radius + radius + 1;

        int r[] = new int[wh];
        int g[] = new int[wh];
        int b[] = new int[wh];
        int rsum, gsum, bsum, x, y, i, p, yp, yi, yw;
        int vmin[] = new int[Math.max(w, h)];

        int divsum = (div + 1) >> 1;
        divsum *= divsum;
        int dv[] = new int[256 * divsum];
        for (i = 0; i < 256 * divsum; i++) {
            dv[i] = (i / divsum);
        }

        yw = yi = 0;

        int[][] stack = new int[div][3];
        int stackpointer;
        int stackstart;
        int[] sir;
        int rbs;
        int r1 = radius + 1;
        int routsum, goutsum, boutsum;
        int rinsum, ginsum, binsum;

        for (y = 0; y < h; y++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            for (i = -radius; i <= radius; i++) {
                p = pix[yi + Math.min(wm, Math.max(i, 0))];
                sir = stack[i + radius];
                sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);
                rbs = r1 - Math.abs(i);
                rsum += sir[0] * rbs;
                gsum += sir[1] * rbs;
                bsum += sir[2] * rbs;
                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }
            }
            stackpointer = radius;

            for (x = 0; x < w; x++) {

                r[yi] = dv[rsum];
                g[yi] = dv[gsum];
                b[yi] = dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (y == 0) {
                    vmin[x] = Math.min(x + radius + 1, wm);
                }
                p = pix[yw + vmin[x]];

                sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[(stackpointer) % div];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi++;
            }
            yw += w;
        }
        for (x = 0; x < w; x++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            yp = -radius * w;
            for (i = -radius; i <= radius; i++) {
                yi = Math.max(0, yp) + x;

                sir = stack[i + radius];

                sir[0] = r[yi];
                sir[1] = g[yi];
                sir[2] = b[yi];

                rbs = r1 - Math.abs(i);

                rsum += r[yi] * rbs;
                gsum += g[yi] * rbs;
                bsum += b[yi] * rbs;

                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }

                if (i < hm) {
                    yp += w;
                }
            }
            yi = x;
            stackpointer = radius;
            for (y = 0; y < h; y++) {
                // Preserve alpha channel: ( 0xff000000 & pix[yi] )
                pix[yi] = ( 0xff000000 & pix[yi] ) | ( dv[rsum] << 16 ) | ( dv[gsum] << 8 ) | dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (x == 0) {
                    vmin[y] = Math.min(y + r1, hm) * w;
                }
                p = x + vmin[y];

                sir[0] = r[p];
                sir[1] = g[p];
                sir[2] = b[p];

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[stackpointer];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi += w;
            }
        }

        Log.e("pix", w + " " + h + " " + pix.length);
        bitmap.setPixels(pix, 0, w, 0, 0, w, h);

        return (bitmap);
    }
    public static boolean isNetworkAvailable(Context context)
    {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }
    public static boolean getStatusWifi( Context context )
    {
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        return wifiManager.isWifiEnabled();
    }
    public static void toggleWiFi(boolean status,Context context)
    {
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        if (status == true && !wifiManager.isWifiEnabled())
        {
            wifiManager.setWifiEnabled(true);
        }
        else if (status == false && wifiManager.isWifiEnabled())
        {
            try
            {
                wifiManager.setWifiEnabled(false);
            }
            catch( Exception ex )
            {
                Log.e( "","ex = " + ex.toString() );
            }
        }
    }
    public static String getUDID(Context context)
    {
        String UDID = "";

        try
        {
            TelephonyManager telephonyManager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
            UDID = telephonyManager.getDeviceId();
        }
        catch (Exception ex)
        {
            Log.e( "utils-->getUDID","ex = " + ex.toString() );

            UDID = ex.toString();
        }

        return UDID;
    }
    public static String getOSVersion()
    {
        String osVersion = "";

        try
        {
            osVersion =  android.os.Build.VERSION.RELEASE;
        }
        catch( Exception ex )
        {
            Log.e("utils-->getOSVersion","ex = "+ex.toString());
            osVersion = ex.toString();
        }

        return osVersion;
    }
    public static  String getDeviceToken(Context context)
    {
        String android_id = "";

        try {
            android_id = Secure.getString(context.getContentResolver(),Secure.ANDROID_ID);
        }
        catch(Exception ex)
        {
            Log.e( "utils-->getDeviceToken","ex = "+ex.toString() );

            android_id = ex.toString();
        }

        return android_id;
    }
    public static int ColorOrange()
    {
        return Color.argb(255, 255, 100, 0);
    }
    public static int ColorDarkGreen()
    {
        return Color.rgb(0, 180, 30);
        //return Color.argb(255,2, 137, 89);
    }
    public static Typeface GetFontFamily(Context context)
    {
        return Typeface.createFromAsset(context.getAssets(), "fonts/th_sarabun_new.ttf");
    }
    public static boolean isBefore(Date dateCurrent , Date dateCompare)
    {
        Boolean bIsResult = false;
        //SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try
        {
            //Date date1 = sdf.parse("2009-12-31");
            //Date date2 = sdf.parse("2010-01-31");

            //System.out.println(sdf.format(date1));
            //System.out.println(sdf.format(date2));

            if(dateCurrent.compareTo(dateCompare)>0){
                System.out.println("Date1 is after Date2");
                bIsResult = true;
            }else if(dateCurrent.compareTo(dateCompare)<0){
                System.out.println("Date1 is before Date2");
                bIsResult = false;
            }else if(dateCurrent.compareTo(dateCompare)==0){
                System.out.println("Date1 is equal to Date2");
                bIsResult = true;
            }else{
                System.out.println("How to get here?");
                bIsResult = false;
            }
        }
        catch( Exception ex )
        {
            Log.e("utils-->getCompareDate","Ex = " + ex.toString());
            bIsResult = false;
        }

        return bIsResult;
    }
    public static String convertBCE2BE(String sDate ) //แปรง คศ เป็น พศ ต้องส่งมาเป็น วันวันเดือนเดือนปี หรือ ปีเดือนวันเท่านั้น, ประเภทเช่น - หรือ /
    {
        int nYears = -1;
        String[] split = null;
        String sOutput = "";

        if( sDate.compareTo("/") > 0 )
        {
            split = sDate.split("/");
            if( split[0].length() == 2 ) //วัน
            {
                //Integer.valueOf(split[2]);
                //nYears -= (543);
                sOutput = split[2]+"-"+split[1]+"-"+split[0];
            }
            else if( split[0].length() == 4 ) //ปี
            {
                //nYears = Integer.valueOf(split[0]);
                //nYears -= (543);
                sOutput = split[0]+"-"+split[1]+"-"+split[2];
            }
        }
        else if( sDate.compareTo("-") > 0 )
        {
            split = sDate.split("-");
            if( split[0].length() == 2 ) //วัน
            {
                //Integer.valueOf(split[2]);
                //nYears -= (543);
                sOutput = split[2]+"-"+split[1]+"-"+split[0];
            }
            else if( split[0].length() == 4 ) //ปี
            {
                //nYears = Integer.valueOf(split[0]);
                //nYears -= (543);
                sOutput = split[0]+"-"+split[1]+"-"+split[2];
            }
        }

        //Output format YYYY-MM-DD 2014-09-01;

        return sOutput;
    }
    public static String getDateSlash()
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Date date = new Date();
        return dateFormat.format(date);
    }
    public static String getDate()
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public static String getDateTime()
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public static String getTime()
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }
    public static String getDate(String dateInString)
    {
        String sDate = "";
        try
        {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            //String dateInString = "2015-11-17 23:59:59";
            Date date = formatter.parse(dateInString);
            date.toString();
            Log.e("", "getMonth = " + date.getMonth());
            Log.e("", "getDate = " + date.getDate());
            Log.e("", "getYear = " + date.getYear());
            Log.e("", "" + date.toString());


            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(date.getTime());
            Date date1 = calendar.getTime();

            //jul 20, 2015

            SimpleDateFormat dateFormat = new SimpleDateFormat("MMM yyyy,dd");

            sDate = dateFormat.format(date);
            Log.e("",""+sDate);
        }
        catch (Exception ex)
        {
            Log.e("",""+ex.toString());
        }

        return  sDate;
    }
    public static String parseDateStrFromFormatToDisplay(String dateInString, String format) {
        String sDate = "";
        try {
            SimpleDateFormat formatter = new SimpleDateFormat(format);
            //String dateInString = "2015-11-17";
            Date date = formatter.parse(dateInString);

            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(date.getTime());
            //Date date1 = calendar.getTime();
            //20 July 2015

            Log.e("utils", "ParseDateFromFormat => " + format);
            Log.e("utils", "dateInString => " + dateInString);
            Log.e("utils", "getMonth = " + (calendar.get(Calendar.MONTH) + 1));
            Log.e("utils", "getDay = " + calendar.get(Calendar.DAY_OF_MONTH));
            Log.e("utils", "getYear = " + calendar.get(Calendar.YEAR));
            Log.e("utils", "" + date.toString());

            SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");
            sDate = dateFormat.format(date);
            Log.e("utils","ParseDateFromFormat => "+sDate);
        }
        catch (Exception ex) {
            Log.e("utils", "ParseDateFromFormat Error => " + ex.toString());
        }
        return  sDate;
    }

    public static void createPopup(Context context,String MSG , String sTitle)
    {
        try
        {
            AlertDialog helpBuilder = new AlertDialog.Builder(context).create();
            helpBuilder.setTitle(sTitle);
            helpBuilder.setMessage(MSG);
            helpBuilder.setCancelable(false);
            helpBuilder.setButton(DialogInterface.BUTTON_POSITIVE, "ใช่", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            helpBuilder.show();

            helpBuilder.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(context.getResources().getColor(R.color.blue_button));
            helpBuilder.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(context.getResources().getColor(R.color.blue_button));
        }
        catch(  Exception ex)
        {
            Log.e( "utils-->createPopUp","ex = " + ex.toString() );
        }
    }

    public static void createPopup(Context context,String MSG , String sTitle, String strTextPositive ,
                                   final DialogInterface.OnClickListener onClickListener) {
        try
        {
            AlertDialog helpBuilder = new AlertDialog.Builder(context).create();
            helpBuilder.setTitle(sTitle);
            helpBuilder.setMessage(MSG);
            helpBuilder.setCancelable(false);
            helpBuilder.setButton(DialogInterface.BUTTON_POSITIVE, strTextPositive, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    onClickListener.onClick(dialog, which);
                    dialog.dismiss();
                }
            });
            helpBuilder.show();

            helpBuilder.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(context.getResources().getColor(R.color.blue_button));
            helpBuilder.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(context.getResources().getColor(R.color.blue_button));
        }
        catch(  Exception ex)
        {
            Log.e( "utils-->createPopUp","ex = " + ex.toString() );
        }
    }

    public static void createPopup(final Context context,String MSG , String sTitle,
                                   String strTextPositive , String strTextNegative ,
                                   final DialogInterface.OnClickListener onClickPositiveListener ,
                                   final DialogInterface.OnClickListener onClickNegativeListener ) {
        try
        {
            AlertDialog helpBuilder = new AlertDialog.Builder(context).create();
            helpBuilder.setTitle(sTitle);
            helpBuilder.setMessage(MSG);
            helpBuilder.setCancelable(false);
            helpBuilder.setButton(DialogInterface.BUTTON_POSITIVE, strTextPositive, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    onClickPositiveListener.onClick(dialog, which);
                    hideKeyboard((Activity)context);
                    dialog.dismiss();
                }
            });
            helpBuilder.setButton(DialogInterface.BUTTON_NEGATIVE, strTextNegative, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    onClickNegativeListener.onClick(dialog, which);
                    dialog.dismiss();
                }
            });
            helpBuilder.show();

            helpBuilder.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(context.getResources().getColor(R.color.blue_button));
            helpBuilder.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(context.getResources().getColor(R.color.blue_button));
        }
        catch(  Exception ex)
        {
            Log.e( "utils-->createPopUp","ex = " + ex.toString() );
        }
    }
    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void printContentValues(ContentValues vals)
    {
        Set<Map.Entry<String, Object>> s=vals.valueSet();
        Iterator itr = s.iterator();

        while(itr.hasNext())
        {
            Map.Entry me = (Map.Entry)itr.next();
            String key = me.getKey().toString();
            Object value =  me.getValue();

            Log.d("utils-->", "Key:"+key+", values:"+(String)(value == null?null:value.toString()));
        }
    }

    public static byte[] getCompressImage(String url)
    {
        try
        {
            Bitmap image = BitmapFactory.decodeFile(url);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            image.compress(Bitmap.CompressFormat.JPEG, 20, out);

            return out.toByteArray();
        }
        catch(Exception ex){}

        return null;
    }

    public static void blackButton(View button)
    {
        button.setOnTouchListener(new OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {

                switch (event.getAction())
                {
                    case MotionEvent.ACTION_DOWN: {
                        ImageButton view = (ImageButton)v;
                        //overlay is black with transparency of 0x77 (119)
                        view.getDrawable().setColorFilter(0x77000000,PorterDuff.Mode.SRC_ATOP);
                        view.invalidate();
                        break;
                    }
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL:
                    {
                        ImageButton view = (ImageButton) v;
                        //clear the overlay
                        view.getDrawable().clearColorFilter();
                        view.invalidate();
                        break;
                    }
                }

                return false;
            }
        });
    }

    public static <T> ArrayList<T> getViewsFromViewGroup(View root, Class<T> clazz)
    {
        for (View view : getAllViewsFromRoots(root))
            if (clazz.isInstance(view))
                blackButton(view);

        return null;
    }

    private static ArrayList<View> getAllViewsFromRoots(View...roots)
    {
        ArrayList<View> result = new ArrayList<View>();
        for (View root : roots)
            getAllViews(result, root);

        return result;
    }

    private static void getAllViews(ArrayList<View> allviews, View parent)
    {
        allviews.add(parent);
        if (parent instanceof ViewGroup)
        {
            ViewGroup viewGroup = (ViewGroup)parent;
            for (int i = 0; i < viewGroup.getChildCount(); i++)
                getAllViews(allviews, viewGroup.getChildAt(i));
        }
    }
    public static float getFactor( Activity context )
    {
        float fFactor = 0.0f;
        int nDensity = -1;
        nDensity = getDensity( context );
        switch(nDensity)
        {
            case DisplayMetrics.DENSITY_LOW:
            {
                fFactor = 0.75f;
            }break;
            case DisplayMetrics.DENSITY_MEDIUM:
            {
                fFactor = 1.0f;

            }break;
            case DisplayMetrics.DENSITY_HIGH:
            {
                fFactor = 1.5f;

            }break;
            case DisplayMetrics.DENSITY_TV:
            {
                fFactor = 1.33f;

            }break;
            case DisplayMetrics.DENSITY_XHIGH:
            {
                fFactor = 2.0f;

            }break;
            case DisplayMetrics.DENSITY_XXHIGH:
            {
                fFactor = 3.0f;

            }break;
        }

        return fFactor;
    }
    public static int getHeightPixels(Activity context)
    {
        int HeightPixels = -1;
        DisplayMetrics metrics=new DisplayMetrics();
        try
        {
            context.getWindowManager().getDefaultDisplay().getMetrics(metrics);
            HeightPixels = metrics.heightPixels;
        }
        catch (Exception e) {
            Log.e( "utils-->getHeightPixels","e.toString() = " + e.toString() );
        }

        return HeightPixels;
    }
    public static int getWidthPixels(Activity context)
    {
        int WidthPixels = -1;
        DisplayMetrics metrics=new DisplayMetrics();
        try
        {
            context.getWindowManager().getDefaultDisplay().getMetrics(metrics);
            WidthPixels = metrics.widthPixels;
        }
        catch (Exception e) {
            Log.e( "utils-->getWidthPixels","e.toString() = " + e.toString() );
        }

        return WidthPixels;
    }
    public static int getDensity( Activity context )
    {
        DisplayMetrics metrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(metrics);
	   	/*
	   	0.75 - ldpi
	   	1.0 - mdpi
	   	1.5 - hdpi
	   	2.0 - xhdpi
	   	3.0 - xxhdpi
	   	4.0 - xxxhdpi  
	   	 */
        return metrics.densityDpi;
    }
    public static float pxToDP(float px, Context context)
    {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        Float fDPI = getDPI(metrics);
        float dp = px / (metrics.densityDpi / fDPI);

        return dp;
    }
    public static int dpToPx(int dp , Context context)
    {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        Float fDPI = getDPI(metrics);

        return Math.round((float) dp * (metrics.densityDpi / fDPI));
    }
    public static float getDPI( DisplayMetrics displayMetric  )
    {
        Float fSizeText = 0.0f;

        switch(displayMetric.densityDpi)
        {
            case DisplayMetrics.DENSITY_LOW:
            {
                fSizeText = 120.0f;
            }break;
            case DisplayMetrics.DENSITY_MEDIUM:
            {
                fSizeText = 160.0f;
            }break;
            case DisplayMetrics.DENSITY_HIGH:
            {
                fSizeText = 240.0f;
            }break;
            case DisplayMetrics.DENSITY_XHIGH:
            {
                fSizeText = 320.0f;
            }break;
            case DisplayMetrics.DENSITY_XXHIGH:
            {
                fSizeText = 480.0f;
            }break;
            case DisplayMetrics.DENSITY_XXXHIGH:
            {
                fSizeText = 640.0f;
            }break;
        }
        return fSizeText;
    }
    public static Typeface getFont(Activity context)
    {
        return Typeface.createFromAsset(context.getAssets(),"fonts/THSarabunNew.ttf");
    }
    public static ArrayList<String> readCalendarEvent(Context context)
    {
        Cursor cursor = context.getContentResolver()
                .query(Uri.parse("content://com.android.calendar/events"),
                        new String[]{"calendar_id", "title", "description",
                                "dtstart", "dtend", "eventLocation"}, null,
                        null, null);

        cursor.moveToFirst();
        // fetching calendars name
        String CNames[] = new String[cursor.getCount()];

        // fetching calendars id
        nameOfEvent.clear();
        startDates.clear();
        endDates.clear();
        descriptions.clear();
        for (int i = 0; i < CNames.length; i++) {

            nameOfEvent.add(cursor.getString(1));
            if( cursor.getString(3) != null )
            {
                startDates.add(getDate(Long.parseLong(cursor.getString(3))));
            }

            if( cursor.getString(4) != null )
            {
                endDates.add(getDate(Long.parseLong(cursor.getString(4))));
            }

            descriptions.add(cursor.getString(2));
            CNames[i] = cursor.getString(1);
            cursor.moveToNext();

        }
        return nameOfEvent;
    }

    public static String getDate(long milliSeconds)
    {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    public static String ConvertUnixTimeToyyyyMMdd(long lUnix)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(lUnix);
        Date date = calendar.getTime();

        //jul 20, 2015

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        String sDate = dateFormat.format(date);

        return sDate;
    }
    public static String getDifferenceDate( Date dDate )
    {
        String timeStamp = new SimpleDateFormat("yyyy/MM/dd").format(Calendar.getInstance().getTime());
        String sDtOld = new SimpleDateFormat("yyyy/MM/dd").format(dDate);

        Log.e( "","timeStamp = " + timeStamp );

        String dateStart = timeStamp;
        String dateStop = sDtOld;
        String sResult = "";

        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");

        Date d1 = null;
        Date d2 = null;

        int nMonth = Calendar.getInstance().getTime().getMonth() - dDate.getMonth();
        int nDate = Calendar.getInstance().getTime().getDate() - dDate.getDate();
        int nYear = Calendar.getInstance().getTime().getYear() - dDate.getYear();

        Log.e("", "nDate = " + nDate);
        Log.e("", "nMonth = " + nMonth);
        Log.e("", "nYear = " + nYear);

        try
        {
            d1 = format.parse(dateStart);
            d2 = format.parse(dateStop);

            //in milliseconds
            long diff = d2.getTime() - d1.getTime();

            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long diffDays = diff / (24 * 60 * 60 * 1000);

            //System.out.print(diffDays + " days, ");
            Log.e( "","days = " + diffDays );

            sResult = String.valueOf(diffDays);
            //System.out.print(diffHours + " hours, ");
            //System.out.print(diffMinutes + " minutes, ");
            //System.out.print(diffSeconds + " seconds.");

        }
        catch (Exception e)
        {
            Log.e( "utils-->getDifferenceDate","e = " + e.toString() );
        }
        //*/

        return sResult;
    }

    public static long daysBetween(Calendar startDate, Calendar endDate)
    {
        Calendar date = (Calendar) startDate.clone();
        long daysBetween = 0;
        while (date.before(endDate))
        {
            date.add(Calendar.DAY_OF_MONTH, 1);
            daysBetween++;
        }
        return daysBetween;
    }
    public static String getFloat( int nDecimal , String sFloat )
    {
        String sConvert = "";
        DecimalFormat df = new DecimalFormat("0.00");
        df.setMaximumFractionDigits(2);

        try
        {
            sConvert = df.format(sFloat);
        }
        catch (Exception e)
        {
            sConvert = "Exception df.format = " + e.toString();
            Log.e( "utils.getFloat","Exception df.format = " + e.toString() );
        }

        return sConvert;
    }

    public static void CreateDir( String sDir )
    {
        File fDir = new File( sDir );
        if(!fDir.exists())
        {
            fDir.mkdirs();
        }
    }
    public static Bitmap getBitmap(String sNameFile )
    {
        File f = new File( sNameFile );
        //File f = null;

        Bitmap bmp = null;

        try
        {
            bmp = BitmapFactory.decodeStream(new FileInputStream(f));
        }
        catch( Exception  ex)
        {
            Log.e( "getBitmapFromPath--> Exception","ex = " + ex.toString() );
        }

        return bmp;
    }
    public static BitmapDrawable getBitmapFromPath(String sNameFile )
    {
        File f = new File( sNameFile );
        //File f = null;

        Bitmap bmp = null;
        BitmapDrawable background = null;

        try
        {
            bmp = BitmapFactory.decodeStream(new FileInputStream(f));

            background = new BitmapDrawable(bmp);
        }
        catch( Exception  ex)
        {
            Log.e( "getBitmapFromPath--> Exception","ex = " + ex.toString() );
        }

        return background;
    }
    public static String encodeFileToBase64Binary(String fileName) throws IOException
    {

        File file = new File(fileName);
        byte[] bytes = loadFile(file);
        String encodedImage = Base64.encodeToString(bytes, Base64.NO_WRAP);

        return encodedImage;
    }
    public static String encodeFileToBase64(String sPath)
    {
        String encode = "";
        try
        {
            Matrix matrix = new Matrix();
            ExifInterface newExif = new ExifInterface(sPath);

            if (newExif.getAttribute(ExifInterface.TAG_ORIENTATION).equals("6"))
                matrix.postRotate(90);
            else if (newExif.getAttribute(ExifInterface.TAG_ORIENTATION).equals("5"))
                matrix.postRotate(270);
            else if ((newExif.getAttribute(ExifInterface.TAG_ORIENTATION).equals("4")))
                matrix.postRotate(180);

            Bitmap bmpOri = utils.getBitmap(sPath);
            Bitmap bmp = Bitmap.createBitmap(bmpOri, 0, 0, bmpOri.getWidth(), bmpOri.getHeight(), matrix, true);

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            encode = Base64.encodeToString(byteArray, Base64.NO_WRAP);
        }
        catch (Exception ex)
        {
            Log.e("","Exception = "+ex.toString());
        }

        return encode;
    }
    private static byte[] loadFile(File file) throws IOException
    {
        InputStream is = new FileInputStream(file);
        long length = file.length();
        if (length > Integer.MAX_VALUE) {
            // File is too large
        }
        byte[] bytes = new byte[(int)length];
        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length
                && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
            offset += numRead;
        }

        if (offset < bytes.length) {
            throw new IOException("Could not completely read file "+file.getName());
        }

        is.close();
        return bytes;
    }

    public static void ToastErrorConnection(Context c)
    {
        Toast.makeText(c,"can't connect to server. ",Toast.LENGTH_SHORT).show();
    }

    public static JSONArray ConvertCursorToJsonArray(Cursor crs )
    {
        JSONArray arr = new JSONArray();

        crs.moveToFirst();
        while (!crs.isAfterLast())
        {
            int nColumns = crs.getColumnCount();
            JSONObject row = new JSONObject();
            for (int i = 0 ; i < nColumns ; i++)
            {
                String colName = crs.getColumnName(i);
                //if( !colName.equals("req_no") )
                {
                    if (colName != null)
                    {
                        String val = "";
                        try
                        {
                            switch (crs.getType(i))
                            {
                                case Cursor.FIELD_TYPE_BLOB   : row.put(colName, crs.getBlob(i).toString()); break;
                                case Cursor.FIELD_TYPE_FLOAT  : row.put(colName, crs.getDouble(i))         ; break;
                                case Cursor.FIELD_TYPE_INTEGER: row.put(colName, crs.getLong(i))           ; break;
                                case Cursor.FIELD_TYPE_NULL   : row.put(colName, "")                       ; break;
                                case Cursor.FIELD_TYPE_STRING : row.put(colName, crs.getString(i).toString().toLowerCase().equals("null")?"":crs.getString(i))         ; break;
                            }
                        }
                        catch (JSONException e)
                        {
                            Log.e( "Utils-->ConvertCursorToJsonArray","e= "+e.toString() );
                        }
                    }
                }
            }



            arr.put(row);
            if (!crs.moveToNext())
                break;
        }
        return arr;
    }


    public static String convertInputStreamToString(InputStream inputStream) throws IOException{
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }
    public static TreeMap<String,String> stringToMap(String queryString)
    {
        TreeMap<String, String> params = new TreeMap<String,String>();
        String[] keyValues = queryString.split("&");
        for(String keyValue : keyValues)
        {
            String[] kv = keyValue.split("=");
            params.put(kv[0], kv[1]);
        }
        return params;
    }
    public static String getTimeStamp()
    {
        long unixTime = System.currentTimeMillis();

        try
        {
            return String.valueOf(unixTime);
        }
        catch (Exception ex)
        {
            return ex.toString();
        }
    }
    @SuppressLint("NewApi")
    public static String getRealPathFromURI_API19(Context context, Uri uri)
    {
        String filePath = "";
        String wholeID = DocumentsContract.getDocumentId(uri);

        try
        {
            // Split at colon, use second item in the array
            String id = wholeID.split(":")[1];

            String[] column = { MediaStore.Images.Media.DATA };

            // where id is equal to
            String sel = MediaStore.Images.Media._ID + "=?";

            Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    column, sel, new String[]{ id }, null);

            int columnIndex = cursor.getColumnIndex(column[0]);

            if (cursor.moveToFirst()) {
                filePath = cursor.getString(columnIndex);
            }
            cursor.close();
        }
        catch(Exception ex)
        {
            Log.e( "utils-->getRealPathFromURI_API19","ex = "+ex.toString() );
        }

        return filePath;
    }


    @SuppressLint("NewApi")
    public static String getRealPathFromURI_API11to18(Context context, Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        String result = null;

        CursorLoader cursorLoader = new CursorLoader(
                context,
                contentUri, proj, null, null, null);
        Cursor cursor = cursorLoader.loadInBackground();

        if(cursor != null){
            int column_index =
                    cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            result = cursor.getString(column_index);
        }
        return result;
    }

    public static String getRealPathFromURI_BelowAPI11(Context context, Uri contentUri){
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
        int column_index
                = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public static boolean isPackageInstalled(String packagename, Context context) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(packagename, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }
    public static void setCustomFont(View view, String font, Context context ) {
        if(font == null) {
            return;
        }
        Typeface tf = get(font, context);
        if(tf != null) {
            if (view instanceof TextView)
            {
                ((TextView)view).setTypeface(tf);
            }
            else if (view instanceof Button)
            {
                ((Button)view).setTypeface(tf);
            }
            else if (view instanceof EditText)
            {
                ((EditText)view).setTypeface(tf);
            }
        }
    }

    public static Typeface getTypeface(String font, Context context)
    {
        return  get(font, context);
    }

    private static Hashtable<String, Typeface> fontCache = new Hashtable<String, Typeface>();

    public static Typeface get(String name, Context context) {
        Typeface tf = fontCache.get(name);

        if(tf == null) {
            try {
                tf = Typeface.createFromAsset(context.getAssets(), name);
            }
            catch (Exception e) {
                return null;
            }
            fontCache.put(name, tf);
        }
        return tf;
    }

    public static String getFormatPic(int nRunningPic , int type , String zbl)
    {
        String format = "_"+zbl+"_"+String.valueOf(nRunningPic);
        if( type==TYPE_BUILDING )
            return "b"+format;
        else if( type == TYPE_LAND_USE )
            return "l"+format;
        else if( type == TYPE_SIGN_BOARD )
            return "s"+format;

        return "";
    }
}

