package ObjectGoldStar;

import android.content.Context;

import java.text.DecimalFormat;

/**
 * Created by PEX2 on 8/14/2015.
 */
public class FormatNumber {
    public static String formatNum(Context context, Double num ){
        String formatter = "";
        DecimalFormat formatNum;
        formatNum = new DecimalFormat("###,###,###,##0.00");
        formatter = formatNum.format(num);
        return formatter;
    }
    public static String formatNumInt(Context context, Integer num){
        String formatter = "";
        DecimalFormat formatNum;
        formatNum = new DecimalFormat("###,###,###,###");
        formatter = formatNum.format(num);
        return formatter;
    }
}
