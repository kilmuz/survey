
package com.ObjectDatabase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteQueryBuilder;
import android.os.Environment;
import android.util.Log;

import java.util.ArrayList;


public class DataBase  extends SQLiteAssetHelper
{
	private static final String DATABASE_NAME = "survey";
	private static final int DATABASE_VERSION = 1; 
	private static SQLiteDatabase db ;
	private String dbPath="";
	private Context _context;
	 
	public   DataBase(Context context)
	{
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		_context = context;
		dbPath = Environment.getExternalStorageDirectory().getAbsolutePath()+"/database/"+DATABASE_NAME;
		if (db == null) 
		{
			try  
			{
				getWritableDatabase().close();
			}
			catch (Exception e) 
			{
				Log.e("DataBase-->DataBase","Exception = " + e.toString());
			}
			try
			{
				db = context.openOrCreateDatabase(dbPath, Context.MODE_PRIVATE, null);
			}
			catch (Exception ex)
			{
				Log.e("DataBase-->DataBase","openOrCreateDatabase = " + ex.toString());
			}

		}
		
		if (!db.isOpen()) 
		{
			db = context.openOrCreateDatabase(dbPath, Context.MODE_PRIVATE, null);
		}
			 
	}
	public static SQLiteDatabase getDb(Context context)
	{
		String dbPath =Environment.getExternalStorageDirectory().getAbsolutePath()+"/database/"+DATABASE_NAME;
		 if (db == null) {
			   db = context.openOrCreateDatabase(dbPath, Context.MODE_PRIVATE, null);
			}
			if (!db.isOpen()) {
				db = context.openOrCreateDatabase(dbPath, Context.MODE_PRIVATE, null);
			}
		return db;
	}
	public static void CloseDB()
	{
		if(db !=null | db.isOpen()) 
		{
			db.close();
			db = null;
		}
	}
	 
	public boolean ExecuteSQL(String value) 
	{
		try
		{
	    	db.execSQL(value);
	    	return true;
		}
		catch(SQLiteException e)
		{
			Log.e( "Database-->ExecuteSQL","e = " + e.toString() );
			return false;
		}
	}
	public Cursor Select(String[] SelectColumn,String Table,String Condition,String Sort)
	{
		SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

		String [] sqlSelect = SelectColumn;
		String sqlTables = Table;

		qb.setTables(sqlTables);


		Cursor c = qb.query(db, sqlSelect, Condition, null, null, null, Sort);
			 
		  
		return c;

	}
	public ArrayList<String> SelectLocationAddress(String SelectColumn,String Table,String Condition , String Sort)
	{
		SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

		String [] sqlSelect = new String[1];
		String sqlTables = Table;
		sqlSelect[0] = SelectColumn;
		qb.setTables(sqlTables);

		ArrayList<String> al = new ArrayList<String>();
		
		Cursor c =  null;
		try
		{ 
			c = qb.query(db, sqlSelect, Condition, null, null, null, Sort);

			if( c != null )
			{
				while (c.moveToNext())
				{
					if( c.isNull(0) )
						al.add("เลือก");
					else
						al.add(c.getString(0));
				}
			}
		}
		catch (Exception e) 
		{
			Log.e( "","e = " + e.toString() );
		}
		finally
		{
			if(c != null)
				c.close();
		}

		return al;
	}
	public ArrayList<String> Select(String SelectColumn,String Table,String Condition , String Sort)
	{
		SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
		ArrayList<String> al = new ArrayList<String>();

		String [] sqlSelect = new String[1];
		String sqlTables = Table;
		sqlSelect[0] = SelectColumn;
		qb.setTables(sqlTables);
		
		Cursor c =  null;
		try
		{ 
			c = qb.query(db, sqlSelect, Condition, null, null, null, Sort);

			if( c != null )
			{
				while (c.moveToNext())
				{
					if( c.getString(0).trim().length() > 0 )
						al.add(c.getString(0));
				}
			}
		}
		catch (Exception e) 
		{
			Log.e( "","e = " + e.toString() );
		}
		finally
		{
			if(c != null)
				c.close();
		}

		

		return al;
	}
	public Cursor Select(String Statement)
	{
		Cursor c = null;
		try
		{
			c = db.rawQuery(Statement, null);
		}
		catch(Exception ex)
		{
			Log.e("", "ex = " + ex.toString());
		}
		finally
		{
			if(c != null)
				c.close();
		}

		 return c;
		 
	}
	public ContentValues Insert(String table,ContentValues values)
	{
		ContentValues cv =new ContentValues();
		long lResult = -1;
		try
		{
			lResult = db.insert(table, null, values);
			cv.put("result",lResult);
			cv.put("message","pass");

		}
		catch(SQLiteException e)
		{
			e.printStackTrace();
			cv.put("result",-1);
			cv.put("message", e.toString());
		}

		return cv;
	}


	public ContentValues Update(String table,ContentValues values,String whereClause,String[] whereArgs)
	{
		ContentValues cv =new ContentValues();
		long lResult = -1;

		try{
			lResult = db.update(table, values, whereClause, whereArgs);
			cv.put("result",lResult);
			cv.put("message","pass");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			cv.put("result", -1 );
			cv.put("message", e.toString());
		}

		return cv;
	}
	public ContentValues Delete(String table,String whereClause,String[] whereArgs)
	{
		ContentValues cv =new ContentValues();

		try
		{
			long lResult = db.delete(table, whereClause, whereArgs) ;
			cv.put("result",lResult);
			cv.put("message","pass");
		}
		catch(SQLiteException e)
		{
			cv.put("result", -1 );
			cv.put("message", e.toString());
		}

		return cv;
	}
}

