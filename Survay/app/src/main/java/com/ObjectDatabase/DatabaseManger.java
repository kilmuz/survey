package com.ObjectDatabase;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ArrayAdapter;

import com.Object.ShareData;
import com.model.BlockParcel;
import com.model.BuildingInfo;
import com.model.LandInfo;
import com.model.LandUse;
import com.model.ParcelInfo;
import com.model.SignBoardInfo;
import com.model.SubBlockParcel;
import com.model.SurveyBuilding;
import com.model.SurveyBuildingOwner;
import com.model.SurveyParcelOwner;
import com.model.SurveySignBoard;
import com.model.SurveySignBoardOwner;
import com.survay.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * Created by user on 3/7/2016.
 */
public class DatabaseManger {
    static String Tag = "DatabaseManager";
    static SQLiteDatabase mSqliteDB = null;

    public DatabaseManger()
    {
        DataBase db = ShareData.getDB();
        mSqliteDB = db.returnDatabase();
    }
    public static ArrayList<String> getTypeSignBoard()
    {
        ArrayList<String> arrayList = new ArrayList<String>();
        Cursor c = mSqliteDB.rawQuery("select * from TypeSignBoard", null);
        if (c != null) {
            try {
                if (c.getCount() > 0) {
                    if (c.moveToFirst()) {
                        for (int n = 0; n < c.getCount(); n++) {
                            arrayList.add(c.getString(c.getColumnIndex("TYPE_NAME")));
                            c.moveToNext();
                        }
                    }
                }
            } catch (Exception ex) {
                Log.e(Tag, "getTypeSignBoard()-->ex = " + ex.toString());
            } finally {
                if (c != null)
                    c.close();
            }
        }
        return arrayList;
    }
    public static String getTypeSignBoard(String TYPE_ID)
    {
       String typeName = "";
        Cursor c = mSqliteDB.rawQuery("select * from TypeSignBoard where TYPE_ID = '"+TYPE_ID+"'", null);
        if (c != null) {
            try {
                if (c.getCount() > 0) {
                    if (c.moveToFirst()) {
                        for (int n = 0; n < c.getCount(); n++) {
                            typeName = (c.getString(c.getColumnIndex("TYPE_NAME")));
                            c.moveToNext();
                        }
                    }
                }
            } catch (Exception ex) {
                Log.e(Tag, "getTypeSignBoard()-->ex = " + ex.toString());
            } finally {
                if (c != null)
                    c.close();
            }
        }
        return typeName;
    }
    public static String getIdTypeSignBoard(String TYPE_NAME)
    {
        String IdType = "";
        Cursor c = mSqliteDB.rawQuery("select * from TypeSignBoard where TYPE_NAME = '"+TYPE_NAME+"'", null);
        if (c != null) {
            try {
                if (c.getCount() > 0) {
                    if (c.moveToFirst()) {
                        for (int n = 0; n < c.getCount(); n++) {
                            IdType = (c.getString(c.getColumnIndex("TYPE_ID")));
                            c.moveToNext();
                        }
                    }
                }
            } catch (Exception ex) {
                Log.e(Tag, "getTypeSignBoard()-->ex = " + ex.toString());
            } finally {
                if (c != null)
                    c.close();
            }
        }
        return IdType;
    }
    public static ArrayList<String> getIdDisSub(String tmbName)
    {
        ArrayList<String> arrayList = new ArrayList<String>();
        Cursor c = mSqliteDB.rawQuery("select * from DistSub where DistrictSub = '"+tmbName+"'", null);
        if (c != null) {
            try {
                if (c.getCount() > 0) {
                    if (c.moveToFirst()) {
                        for (int n = 0; n < c.getCount(); n++) {
                            arrayList.add(c.getString(c.getColumnIndex("DistSub_id")));
                            c.moveToNext();
                        }
                    }
                }
            } catch (Exception ex) {
                Log.e(Tag, "getIdDisSub()-->ex = " + ex.toString());
            } finally {
                if (c != null)
                    c.close();
            }
        }
        return arrayList;
    }
    public static String getNameDisSub(String DistSub_id)
    {
       String DistrictSub = "";
        Cursor c = mSqliteDB.rawQuery("select * from DistSub where DistSub_id = '"+DistSub_id+"'", null);
        if (c != null) {
            try {
                if (c.getCount() > 0) {
                    if (c.moveToFirst()) {
                        for (int n = 0; n < c.getCount(); n++) {
                            DistrictSub = (c.getString(c.getColumnIndex("DistrictSub")));
                            c.moveToNext();
                        }
                    }
                }
            } catch (Exception ex) {
                Log.e(Tag, "getNameDisSub()-->ex = " + ex.toString());
            } finally {
                if (c != null)
                    c.close();
            }
        }
        return DistrictSub;
    }
    public static ArrayList<String> getTypeLanduse()
    {
        ArrayList<String> arrayList = new ArrayList<String>();
        Cursor c = mSqliteDB.rawQuery("select * from TypeLandUse", null);
        if (c != null) {
            try {
                if (c.getCount() > 0) {
                    if (c.moveToFirst()) {
                        for (int n = 0; n < c.getCount(); n++) {
                            arrayList.add(c.getString(c.getColumnIndex("LandUse_Name")));
                            c.moveToNext();
                        }
                    }
                }
            } catch (Exception ex) {
                Log.e(Tag, "TypeLanduse()-->ex = " + ex.toString());
            } finally {
                if (c != null)
                    c.close();
            }
        }
        return arrayList;
    }
    public static String getNameTypeLandUse(String LandUse_id )
    {
        String LandUse_Name = "";
        Cursor c = mSqliteDB.rawQuery("select * from TypeLandUse where LandUse_id = '"+LandUse_id+"'", null);
        if (c != null) {
            try {
                if (c.getCount() > 0) {
                    if (c.moveToFirst()) {
                        for (int n = 0; n < c.getCount(); n++) {
                            LandUse_Name = (c.getString(c.getColumnIndex("LandUse_Name")));
                            c.moveToNext();
                        }
                    }
                }
            } catch (Exception ex) {
                Log.e(Tag, "TypeLanduse()-->ex = " + ex.toString());
            } finally {
                if (c != null)
                    c.close();
            }
        }
        return LandUse_Name;
    }
    public static String getIdTypeLandUse(String LandUse_Name )
    {
        String LandUse_id = "";
        Cursor c = mSqliteDB.rawQuery("select * from TypeLandUse where LandUse_Name = '"+LandUse_Name+"'", null);
        if (c != null) {
            try {
                if (c.getCount() > 0) {
                    if (c.moveToFirst()) {
                        for (int n = 0; n < c.getCount(); n++) {
                            LandUse_id = (c.getString(c.getColumnIndex("LandUse_id")));
                            c.moveToNext();
                        }
                    }
                }
            } catch (Exception ex) {
                Log.e(Tag, "TypeLandUse()-->ex = " + ex.toString());
            } finally {
                if (c != null)
                    c.close();
            }
        }
        return LandUse_id;
    }
    public static ArrayList<String> getTypeBuilding()
    {
        ArrayList<String> arrayList = new ArrayList<String>();
        Cursor c = mSqliteDB.rawQuery("select * from TypeBuilding", null);
        if (c != null) {
            try {
                if (c.getCount() > 0) {
                    if (c.moveToFirst()) {
                        for (int n = 0; n < c.getCount(); n++) {
                            arrayList.add(c.getString(c.getColumnIndex("HouseScripName")));
                            c.moveToNext();
                        }
                    }
                }
            } catch (Exception ex) {
                Log.e(Tag, "getTypeBuilding()-->ex = " + ex.toString());
            } finally {
                if (c != null)
                    c.close();
            }
        }
        return arrayList;
    }
    public static String getNameSignBoard(String TYPE_ID)
    {
        String TYPE_NAME = "";
        Cursor c = mSqliteDB.rawQuery("select * from TypeSignBoard where TYPE_ID = '"+TYPE_ID+"'", null);
        if (c != null) {
            try {
                if (c.getCount() > 0) {
                    if (c.moveToFirst()) {
                        for (int n = 0; n < c.getCount(); n++) {
                            TYPE_NAME = (c.getString(c.getColumnIndex("TYPE_NAME")));
                            c.moveToNext();
                        }
                    }
                }
            } catch (Exception ex) {
                Log.e(Tag, "getTypeSignBoard()-->ex = " + ex.toString());
            } finally {
                if (c != null)
                    c.close();
            }
        }
        return TYPE_NAME;
    }
    public static String getNameBuilding(String HouseScripID)
    {
        String HouseScripName = "";
        Cursor c = mSqliteDB.rawQuery("select * from TypeBuilding where HouseScripID = '"+HouseScripID+"'", null);
        if (c != null) {
            try {
                if (c.getCount() > 0) {
                    if (c.moveToFirst()) {
                        for (int n = 0; n < c.getCount(); n++) {
                            HouseScripName = (c.getString(c.getColumnIndex("HouseScripName")));
                            c.moveToNext();
                        }
                    }
                }
            } catch (Exception ex) {
                Log.e(Tag, "getTypeBuilding()-->ex = " + ex.toString());
            } finally {
                if (c != null)
                    c.close();
            }
        }
        return HouseScripName;
    }
    public static String getIdTypeBuilding(String HouseScripName)
    {
        String HouseScripID = "";
        Cursor c = mSqliteDB.rawQuery("select * from TypeBuilding where HouseScripName = '"+HouseScripName+"'", null);
        if (c != null) {
            try {
                if (c.getCount() > 0) {
                    if (c.moveToFirst()) {
                        for (int n = 0; n < c.getCount(); n++) {
                            HouseScripID = (c.getString(c.getColumnIndex("HouseScripID")));
                            c.moveToNext();
                        }
                    }
                }
            } catch (Exception ex) {
                Log.e(Tag, "getIdTypeBuilding()-->ex = " + ex.toString());
            } finally {
                if (c != null)
                    c.close();
            }
        }
        return HouseScripID;
    }

    public static ArrayList<BlockParcel> getBlockParcelNew()
    {
        ArrayList<BlockParcel> arLstBlockParcel = new ArrayList<BlockParcel>();
        String sql = "SELECT DISTINCT  substr(zbl,1,2)as zbl FROM SurveyParcel";

        Cursor c = mSqliteDB.rawQuery(sql, null);
        if (c != null) {
            try {
                if (c.getCount() > 0) {
                    if (c.moveToFirst()) {
                        for (int n = 0; n < c.getCount(); n++) {

                            String zbl = c.getString(c.getColumnIndex("zbl"));
                            BlockParcel block = new BlockParcel();
                            block.blockParcel = zbl;
                            arLstBlockParcel.add(block);
                            c.moveToNext();
                        }
                    }
                }

            } catch (Exception ex) {
                Log.e(Tag, "getLandInfo()-->ex = " + ex.toString());
            } finally {
                if (c != null)
                    c.close();
            }
        }

        return arLstBlockParcel;
    }

    public static ArrayList<BlockParcel> getZoneParcel( String parcelBlock )
    {
        ArrayList<BlockParcel> arLstBlockParcel = new ArrayList<BlockParcel>();
//        String sql = "SELECT DISTINCT  substr(zbl,1,2)as zbl FROM SurveyParcel";
        String sql = "SELECT DISTINCT substr(ZBL,3,1) as ZBL FROM SurveyParcel WHERE ZBL LIKE '"+parcelBlock+"%'";

        Cursor c = mSqliteDB.rawQuery(sql, null);
        if (c != null) {
            try {
                if (c.getCount() > 0) {
                    if (c.moveToFirst()) {
                        for (int n = 0; n < c.getCount(); n++) {

                            String zbl = c.getString(c.getColumnIndex("ZBL"));
                            BlockParcel block = new BlockParcel();
                            block.blockParcel = zbl;
                            arLstBlockParcel.add(block);
                            c.moveToNext();
                        }
                    }
                }

            } catch (Exception ex) {
                Log.e(Tag, "getLandInfo()-->ex = " + ex.toString());
            } finally {
                if (c != null)
                    c.close();
            }
        }

        return arLstBlockParcel;
    }

    public static ArrayList<BlockParcel> getBlockParcel()
    {
        ArrayList<String> arLstBlock = new ArrayList<>();
        String sql = "select zbl from SurveyParcel";

        Cursor c = mSqliteDB.rawQuery(sql, null);
        if (c != null) {
            try {
                if (c.getCount() > 0) {
                    if (c.moveToFirst()) {
                        for (int n = 0; n < c.getCount(); n++) {

                            String zbl = c.getString(c.getColumnIndex("ZBL"));
                            arLstBlock.add(zbl);
                            c.moveToNext();
                        }
                    }
                }

            } catch (Exception ex) {
                Log.e(Tag, "getLandInfo()-->ex = " + ex.toString());
            } finally {
                if (c != null)
                    c.close();
            }
        }

        ArrayList<String> arLstBlockParcel = new ArrayList<String>();
        ArrayList<BlockParcel> arLstObjBlockParcel = new ArrayList<BlockParcel>();
        for( int n =0;n<arLstBlock.size();n++ )
        {
            if( n == 0 )
            {
                arLstBlockParcel.add(""+arLstBlock.get(n).charAt(2));
                BlockParcel blockParcel = new BlockParcel();
                blockParcel.blockParcel = ""+arLstBlock.get(n).charAt(2);

                arLstObjBlockParcel.add(blockParcel);
            }
            else
            {
                boolean isAdd = true;
                for( int i = 0; i <arLstBlockParcel.size(); i++ )
                {
                    if( arLstBlock.get(n).charAt(2) == arLstBlockParcel.get(i).charAt(0) )
                    {
                        isAdd = false;
                        break;
                    }
                }

                if( isAdd )
                {
                    BlockParcel blockParcel = new BlockParcel();
                    blockParcel.blockParcel = ""+arLstBlock.get(n).charAt(2);

                    arLstBlockParcel.add(""+arLstBlock.get(n).charAt(2));
                    arLstObjBlockParcel.add(blockParcel);
                }

            }
        }

        return arLstObjBlockParcel;
    }

    public static ArrayList<SubBlockParcel> getSubBlockParcel(String prefixBlockParcel)
    {

        ArrayList<SubBlockParcel> arLstObjBlockParcel = new ArrayList<SubBlockParcel>();
        ArrayList<SubBlockParcel> arLstMyObjBlockParcel = new ArrayList<SubBlockParcel>();

        //String sql = "Select * from SurveyParcel where zbl LIKE '%"+prefixBlockParcel+"%'";
//        String sql = " SELECT SurveyParcel.ZBL, "+
//           " SurveyParcelOwner.FullName, "+
//           " SurveyParcelOwner.Parcel_ID, "+
//           " SurveyParcel.status, "+
//           " SurveyParcel.Parcel_Tmb, "+
//           " SurveyParcelOwner.Parcel_Ser "+
//        " FROM SurveyParcel "+
//        " LEFT JOIN SurveyParcelOwner ON SurveyParcelOwner.Parcel_ID = SurveyParcel.Parcel_ID"+
//        " AND SurveyParcelOwner.Parcel_Ser = SurveyParcel.Parcel_Ser "+
//        " WHERE zbl LIKE '%"+prefixBlockParcel+"%'";

        String sql = " SELECT SurveyParcel.ZBL, "+
                " SurveyParcelOwner.FullName, "+
                " SurveyParcelOwner.Parcel_ID, "+
                " SurveyParcel.status, "+
                " SurveyParcel.Parcel_Tmb, "+
                " SurveyParcelOwner.Parcel_Ser "+
                " FROM SurveyParcel "+
                " LEFT JOIN SurveyParcelOwner ON SurveyParcelOwner.Parcel_ID = SurveyParcel.Parcel_ID"+
                " AND SurveyParcelOwner.Parcel_Ser = SurveyParcel.Parcel_Ser "+
                " WHERE zbl LIKE '"+prefixBlockParcel+"%'and SurveyParcelOwner.Parcel_Ser <> \"\" and SurveyParcelOwner.Parcel_ID <>\"\"";

//        select zbl from SurveyParcel where zbl  LIKE '01%'
        Log.e( "DataBaseManger" , "getSubBlockParcel = "+sql );
        Cursor c = mSqliteDB.rawQuery(sql, null);
        if (c != null) {
            try {
                if (c.getCount() > 0) {
                    if (c.moveToFirst()) {
                        for (int n = 0; n < c.getCount(); n++) {

                            String zbl = c.getString(c.getColumnIndex("ZBL"));
                            String FullName = c.getString(c.getColumnIndex("FullName"));
                            String Parcel_ID = c.getString(c.getColumnIndex("Parcel_ID"));
                            String Parcel_Ser = c.getString(c.getColumnIndex("Parcel_Ser"));
                            String Parcel_Tmb = c.getString(c.getColumnIndex("Parcel_Tmb"));
                            int status = c.getInt(c.getColumnIndex("status"));

                            SubBlockParcel blockParcel = new SubBlockParcel();
                            blockParcel.blockParcel = "" + zbl;
                            blockParcel.nameOwner = "" + FullName;
                            blockParcel.Parcel_ID = "" + Parcel_ID;
                            blockParcel.Parcel_Ser = "" + Parcel_Ser;
                            blockParcel.Parcel_Tmb = "" + Parcel_Tmb;
                            blockParcel.status = status;

                            if (TextUtils.isEmpty(FullName) || TextUtils.isEmpty(Parcel_ID) || TextUtils.isEmpty(Parcel_Ser)) {
                                c.moveToNext();
                                continue;
                            } else {
                                arLstObjBlockParcel.add(blockParcel);
                                c.moveToNext();
                            }
                        }
                    }
                }
            } catch (Exception ex) {
                Log.e(Tag, "getSubBlockParcel()-->ex = " + ex.toString());
            } finally {
                if (c != null)
                    c.close();
            }
        }

        for (int count=0; count< arLstObjBlockParcel.size(); count++ ) {
            SubBlockParcel blockParcel = arLstObjBlockParcel.get(count);
            for (int z=count+1; z< arLstObjBlockParcel.size(); z++ ) {
                SubBlockParcel blockParcel2 = arLstObjBlockParcel.get(z);
                if (blockParcel.blockParcel.equals(blockParcel2.blockParcel)) {
                    blockParcel.nameOwner += ","+blockParcel2.nameOwner;
                    blockParcel.Parcel_ID += ","+blockParcel2.Parcel_ID;
                    blockParcel.Parcel_Ser += ","+blockParcel2.Parcel_Ser;
                    arLstObjBlockParcel.remove(z);
                    z--;
                }
            }
        }
        Log.d("Block Parcel","ArryList :: "+ arLstObjBlockParcel);

        return arLstObjBlockParcel;
    }

    public static ArrayList<LandInfo> getLandInfo(String zbl)
    {
        ArrayList<LandInfo> arLstLandInfo = new ArrayList<LandInfo>();

        String sql = "SELECT SurveyParcel.Parcel_Num, " +
                "       SurveyParcel.Parcel_Rai, " +
                "       SurveyParcel.Parcel_Nang, " +
                "       SurveyParcel.Parcel_Va, " +
                "       SurveyParcel.Parcel_Ser, " +
                "       SurveyParcel.ZBL, " +
                "       SurveyParcel.Parcel_ID, " +
                "       SurveyParcel.Parcel_Moo, " +
                "       SurveyParcel.Parcel_Tmb, " +
                "       TypeLandDoc.LandScripName, " +
                "       DistSub.DistrictSub " +
                " FROM SurveyParcel " +
                " INNER JOIN TypeLandDoc ON TypeLandDoc.LandScripID = SurveyParcel.Parcel_Doc " +
                " INNER JOIN DistSub ON DistSub.DistSub_Id = SurveyParcel.Parcel_Tmb " +
                " Where SurveyParcel.ZBL == '"+zbl+"'" +
                " ORDER BY SurveyParcel.ZBL ASC";

        Log.e(Tag,String.format("getLandInfo[%s] , sql = %s",zbl,sql));
        Cursor c = mSqliteDB.rawQuery(sql, null);
        if (c != null) {
            try {
                if(c.getCount() > 0 )
                {
                    if( c.moveToFirst() )
                    {
                        LandInfo landInfo = new LandInfo();
                        landInfo.LandScripType = c.getString(c.getColumnIndex("LandScripName"));
                        landInfo.Moo = c.getString(c.getColumnIndex("Parcel_Moo"));
                        landInfo.Parcel_Nang = c.getString(c.getColumnIndex("Parcel_Nang"));
                        landInfo.Parcel_Num = c.getString(c.getColumnIndex("Parcel_Num"));
                        landInfo.Parcel_Rai = c.getString(c.getColumnIndex("Parcel_Rai"));
                        landInfo.Parcel_Va = c.getString(c.getColumnIndex("Parcel_Va"));
                        landInfo.Parcel_Ser = c.getString(c.getColumnIndex("Parcel_Ser"));
                        landInfo.Parcel_Id = c.getString(c.getColumnIndex("Parcel_ID"));
                        landInfo.ZBL = c.getString(c.getColumnIndex("ZBL"));
                        landInfo.Tmb = c.getString(c.getColumnIndex("Parcel_Tmb"));

                        arLstLandInfo.add(landInfo);
                        return arLstLandInfo;
                    }
                    else
                        return null;
                }

            } catch (Exception ex) {
                Log.e(Tag, "getLandInfo()-->ex = " + ex.toString());
            } finally {
                if (c != null)
                    c.close();
            }
        }

        return arLstLandInfo;
    }

    public static ArrayList<LandUse> getSurveyParcelLandUse(String parcel_id, String parcel_ser , String parcel_tmb)
    {
        ArrayList<LandUse> arLstLandInfo = new ArrayList<LandUse>();

        String sql = "SELECT Parcel_ID, " +
                "       Parcel_Ser, " +
                "       Parcel_Tmb, " +
                "       Parcel_Use, " +
                "       Parcel_Rai, " +
                "       Parcel_Nang, " +
                "       Parcel_Va, " +
                "       Parcel_Tax, " +
                "       LandUse_Name " +
                " FROM SurveyParcelLandUse " +
                " INNER JOIN TypeLandUse ON TypeLandUse.LandUse_id = SurveyParcelLandUse.Parcel_Use " +
                " Where SurveyParcelLandUse.Parcel_ID = '"+parcel_id+"' and " +
                " SurveyParcelLandUse.Parcel_Ser = '"+parcel_ser+"' and " +
                " SurveyParcelLandUse.Parcel_Tmb = '"+parcel_tmb+"'";

        Log.e(Tag,String.format("getSurveyParcelLandUse[parcel_id = %s , parcel_ser = %s , parcel_tmb = %s] , sql = %s"
                ,parcel_id,parcel_ser,parcel_tmb,sql));

        Cursor c = mSqliteDB.rawQuery(sql, null);
        if (c != null) {
            try {
                if(c.getCount() > 0 )
                {
                    if( c.moveToFirst() )
                    {
                        for( int n=0; n < c.getCount(); n++ )
                        {
                            LandUse landInfo = new LandUse();
                            landInfo.Parcel_Tmb = c.getString(c.getColumnIndex("Parcel_Tmb"));

                            landInfo.typeLandUse = c.getString(c.getColumnIndex("Parcel_Use"));
                            landInfo.landUseName = c.getString(c.getColumnIndex("LandUse_Name"));
                            landInfo.nang = c.getString(c.getColumnIndex("Parcel_Nang"));
                            landInfo.rai = c.getString(c.getColumnIndex("Parcel_Rai"));
                            landInfo.va = c.getString(c.getColumnIndex("Parcel_Va"));
                            landInfo.Parcel_Ser = c.getString(c.getColumnIndex("Parcel_Ser"));
                            landInfo.Parcel_ID = c.getString(c.getColumnIndex("Parcel_ID"));
                            landInfo.isMaster = true;

                            arLstLandInfo.add(landInfo);
                            c.moveToNext();
                        }

                        return arLstLandInfo;
                    }
                    else
                        return null;
                }

            } catch (Exception ex) {
                Log.e(Tag, "getLandInfo()-->ex = " + ex.toString());
            } finally {
                if (c != null)
                    c.close();
            }
        }

        return arLstLandInfo;
    }

    public static ArrayList<SurveyParcelOwner> getSurveyParcelOwner(String parcel_id , String
            parcel_ser)
    {
        ArrayList<SurveyParcelOwner> arLstSurveyParcelOwner = new ArrayList<SurveyParcelOwner>();


        String sql = " SELECT * "+
                " FROM SurveyParcelOwner "+
                " WHERE SurveyParcelOwner.Parcel_ID = '"+parcel_id+"' "+
                " AND SurveyParcelOwner.Parcel_Ser = '"+parcel_ser+"' ";

        Log.e(Tag,"sql = "+sql);
        Cursor c = mSqliteDB.rawQuery(sql, null);
        if (c != null) {
            try {
                if(c.getCount() > 0 )
                {
                    if( c.moveToFirst() )
                    {
                        for( int i=0; i < c.getCount();i++ )
                        {
                            SurveyParcelOwner surveyParcelOwner = new SurveyParcelOwner();
                            String Adds = "";
                            String fullName = "";
                            String CustomerID = "";
                            String parcelId = "";
                            String parcelSer = "";
                            for( int n=0; n < c.getCount(); n++ )
                            {
                                fullName += c.getString(c.getColumnIndex("FullName"));
                                fullName += ",";

                                Adds = c.getString(c.getColumnIndex("Adds"));
                                CustomerID = c.getString(c.getColumnIndex("CustomerID"));
                                parcelId = c.getString(c.getColumnIndex("Parcel_ID"));
                                parcelSer = c.getString(c.getColumnIndex("Parcel_Ser"));

                                c.moveToNext();
                            }
                            fullName = fullName.substring(0,fullName.length()-1); //remove ,
                            surveyParcelOwner.fullName = fullName;
                            surveyParcelOwner.address = Adds;
                            surveyParcelOwner.CustomerID = CustomerID;
                            surveyParcelOwner.Parcel_Ser = parcelSer;
                            surveyParcelOwner.Parcel_ID = parcelId;

                            arLstSurveyParcelOwner.add(surveyParcelOwner);
                        }

                        return arLstSurveyParcelOwner;
                    }
                    else
                        return null;

                }
            } catch (Exception ex) {
                Log.e(Tag, "getLandInfo()-->ex = " + ex.toString());
            } finally {
                if (c != null)
                    c.close();
            }
        }

        return arLstSurveyParcelOwner;
    }
    public static ArrayList<SurveyParcelOwner> getSurveyParcelOwnerUse(String parcel_id , String parcel_ser)
    {
        ArrayList<SurveyParcelOwner> arrSurveyParcelOwner = new ArrayList<SurveyParcelOwner>();

        String sql = " SELECT * "+
                " FROM SurveyParcelOwnerUse "+
                " WHERE SurveyParcelOwnerUse.Parcel_ID = '"+parcel_id+"' "+
                " AND SurveyParcelOwnerUse.Parcel_Ser = '"+parcel_ser+"' ";

        Log.e(Tag,"sql = "+sql);
        Cursor c = mSqliteDB.rawQuery(sql, null);
        if (c != null) {
            try {
                if( c.moveToFirst() )
                {
                    for( int n=0; n < c.getCount(); n++ )
                    {
                        String Adds = "";
                        String fullName = "";
                        String ftName = "";
                        String lstName = "";
                        String CustomerID = "";
                        String parcelId = "";
                        String parcelSer = "";
                        String prefixName = "";
                        SurveyParcelOwner surveyParcelOwner = new SurveyParcelOwner();

                        fullName = c.getString(c.getColumnIndex("FullName"));
                        Adds = c.getString(c.getColumnIndex("Adds"));
                        prefixName = c.getString(c.getColumnIndex("PrefixName"));
                        ftName = c.getString(c.getColumnIndex("FirstName"));
                        lstName = c.getString(c.getColumnIndex("LastName"));
                        CustomerID = c.getString(c.getColumnIndex("CustomerID"));
                        parcelId = c.getString(c.getColumnIndex("Parcel_ID"));
                        parcelSer = c.getString(c.getColumnIndex("Parcel_Ser"));

                        surveyParcelOwner.PrefixName = prefixName;
                        surveyParcelOwner.FIRST_NAME = ftName;
                        surveyParcelOwner.LAST_NAME = lstName;
                        surveyParcelOwner.fullName = fullName;
                        surveyParcelOwner.address = Adds;
                        surveyParcelOwner.CustomerID = CustomerID;
                        surveyParcelOwner.Parcel_Ser = parcelSer;
                        surveyParcelOwner.Parcel_ID = parcelId;

                        arrSurveyParcelOwner.add(surveyParcelOwner);
                        c.moveToNext();
                    }

                    return arrSurveyParcelOwner;
                }
                else
                    return arrSurveyParcelOwner;

            } catch (Exception ex) {
                Log.e(Tag, "getLandInfo()-->ex = " + ex.toString());
            } finally {
                if (c != null)
                    c.close();
            }
        }

        return arrSurveyParcelOwner;
    }

    public static ArrayList<SurveySignBoardOwner> getSurveySignBoardOwner(String Parcel_ID ,
                                                                          String Parcel_Ser)
    {
        ArrayList<SurveySignBoardOwner> arrSurveyParcelOwner = new ArrayList<SurveySignBoardOwner>();

//        String sql = " SELECT * "+
//                " FROM SurveySignBoardOwner "+
//                " WHERE SurveySignBoardOwner.Sign_No = '"+Sign_count+"' ";

        String sql = " SELECT * "+
                " FROM SurveySignBoard "+
                " INNER JOIN SurveySignBoardOwner on SurveySignBoard.Sign_No = " +
                "SurveySignBoardOwner" +
                ".Sign_No "+
                " WHERE SurveySignBoard.Parcel_ID = '"+Parcel_ID+"'" + " and SurveySignBoard" +
                ".Parcel_Ser = '"+Parcel_Ser+"'";

        Log.e(Tag,"sql = "+sql);
        Cursor c = mSqliteDB.rawQuery(sql, null);
        if (c != null) {
            try {
                if( c.moveToFirst() )
                {
                    for( int n=0; n < c.getCount(); n++ )
                    {
                        String Sign_No = "";
                        String HN_ID = "";
                        String FIRST_NAME = "";
                        String LAST_NAME = "";
                        String NUM = "";
                        String MOO = "";
                        String CustomerID = "";

                        Sign_No = c.getString(c.getColumnIndex("Sign_No"));
                        HN_ID = c.getString(c.getColumnIndex("HN_ID"));
                        FIRST_NAME = c.getString(c.getColumnIndex("FIRST_NAME"));
                        LAST_NAME = c.getString(c.getColumnIndex("LAST_NAME"));
                        NUM = c.getString(c.getColumnIndex("NUM"));
                        CustomerID = c.getString(c.getColumnIndex("CustomerID"));
                        MOO = c.getString(c.getColumnIndex("MOO"));

                        SurveySignBoardOwner surveyParcelOwner = new SurveySignBoardOwner();
                        surveyParcelOwner.Sign_No = Sign_No;
                        surveyParcelOwner.HN_ID = HN_ID;
                        surveyParcelOwner.FIRST_NAME = FIRST_NAME;
                        surveyParcelOwner.LAST_NAME = LAST_NAME;
                        surveyParcelOwner.fullName = HN_ID+" "+FIRST_NAME+" "+LAST_NAME;
                        surveyParcelOwner.NUM = NUM;
                        surveyParcelOwner.CustomerID = CustomerID;
                        surveyParcelOwner.MOO = MOO;

                        arrSurveyParcelOwner.add(surveyParcelOwner);
                        c.moveToNext();
                    }

                    return arrSurveyParcelOwner;
                }
                else
                    return arrSurveyParcelOwner;

            } catch (Exception ex) {
                Log.e(Tag, "getSurveySignBoardOwner()-->ex = " + ex.toString());
            } finally {
                if (c != null)
                    c.close();
            }
        }

        return arrSurveyParcelOwner;
    }

    public static ArrayList<SurveyBuildingOwner> getSurveyBuildingOwner(String Parcel_ID , String
            Parcel_Ser , String Build_count )
    {
        ArrayList<SurveyBuildingOwner> arrSurveyParcelOwner = new ArrayList<SurveyBuildingOwner>();

        String sql = " SELECT * "+
                " FROM SurveyBuilding "+
                " INNER JOIN SurveyBuildingOwner on SurveyBuilding.Build_No = SurveyBuildingOwner.Build_No"+
                " WHERE SurveyBuilding.Parcel_ID = '"+Parcel_ID+"'" + " and SurveyBuilding.Parcel_Ser = '"+Parcel_Ser+"'";


        Log.e(Tag,"sql = "+sql);
        Cursor c = mSqliteDB.rawQuery(sql, null);
        if (c != null) {
            try {
                if( c.moveToFirst() )
                {
                    String Build_No = "-1";
                    String HN_ID = "";
                    String FIRST_NAME = "";
                    String LAST_NAME = "";
                    String CustomerID = "";
                    String Parcel_Tmb = "";
                    String Build_NUM = "";

                    for( int n=0; n < c.getCount(); n++ )
                    {


                        HN_ID = c.getString(c.getColumnIndex("HN_ID"));
                        Build_No = c.getString(c.getColumnIndex("Build_No"));
                        FIRST_NAME = c.getString(c.getColumnIndex("FIRST_NAME"));
                        LAST_NAME = c.getString(c.getColumnIndex("LAST_NAME"));
                        CustomerID = c.getString(c.getColumnIndex("CustomerID"));
                        Build_NUM = c.getString(c.getColumnIndex("Building_Num"));

                        SurveyBuildingOwner surveyParcelOwner = new SurveyBuildingOwner();

                        surveyParcelOwner.HN_ID = HN_ID;
                        surveyParcelOwner.FIRST_NAME = FIRST_NAME;
                        surveyParcelOwner.LAST_NAME = LAST_NAME;

                        surveyParcelOwner.fullName = HN_ID+" "+FIRST_NAME+" "+LAST_NAME;
                        surveyParcelOwner.address = "บ้านเลขที่ "+CustomerID;

                        surveyParcelOwner.CustomerID = CustomerID;
                        surveyParcelOwner.Parcel_ID = Parcel_ID;
                        surveyParcelOwner.Parcel_Ser = Parcel_Ser;
                        surveyParcelOwner.Parcel_Tmb = Parcel_Tmb;
                        surveyParcelOwner.Building_No = Build_No;
                        surveyParcelOwner.NUM = Build_NUM;

                        arrSurveyParcelOwner.add(surveyParcelOwner);
                        c.moveToNext();
                    }

                    return arrSurveyParcelOwner;
                }
                else
                    return arrSurveyParcelOwner;

            } catch (Exception ex) {
                Log.e(Tag, "getSurveySignBoardOwner()-->ex = " + ex.toString());
            } finally {
                if (c != null)
                    c.close();
            }
        }

        return arrSurveyParcelOwner;
    }

    public static ArrayList<SurveySignBoard> getSurveySignBoard(String parcel_id , String parcel_ser)
    {
        ArrayList<SurveySignBoard> arrSurveySignBoard = new ArrayList<SurveySignBoard>();

        String sql = " SELECT * "+
                " FROM SurveySignBoard "+
                " WHERE SurveySignBoard.Parcel_ID = '"+parcel_id+"' "+
                " AND SurveySignBoard.Parcel_Ser = '"+parcel_ser+"' ";

        Log.e(Tag,"sql = "+sql);
        Cursor c = mSqliteDB.rawQuery(sql, null);
        if (c != null) {
            try {
                if( c.moveToFirst() )
                {
                    for( int n=0; n < c.getCount(); n++ )
                    {
                        String Sign_No = "";
                        String Parcel_ID = "";
                        String Parcel_Ser = "";
                        String Parcel_Tmb = "";
                        String Sign_Type = "";
                        String Sign_txt = "";
                        String Sign_Width = "";
                        String Sign_Higth = "";
                        String Sign_Side = "";
                        String Sign_Tax = "";
                        String CreateBy = "";
                        String CreateDate = "";
                        String ModifyBy = "";
                        String ModifyDate = "";
                        String StatusLink = "";

                        Sign_No = c.getString(c.getColumnIndex("Sign_No"));
                        Parcel_ID = c.getString(c.getColumnIndex("Parcel_ID"));
                        Parcel_Ser = c.getString(c.getColumnIndex("Parcel_Ser"));
                        Parcel_Tmb = c.getString(c.getColumnIndex("Parcel_Tmb"));
                        Sign_Type = c.getString(c.getColumnIndex("Sign_Type"));
                        Sign_txt = c.getString(c.getColumnIndex("Sign_txt"));
                        Sign_Higth = c.getString(c.getColumnIndex("Sign_Higth"));
                        Sign_Width = c.getString(c.getColumnIndex("Sign_Width"));
                        Sign_Side = c.getString(c.getColumnIndex("Sign_Side"));
                        Sign_Tax = c.getString(c.getColumnIndex("Sign_Tax"));
                        CreateBy = c.getString(c.getColumnIndex("CreateBy"));
                        CreateDate = c.getString(c.getColumnIndex("CreateDate"));
                        ModifyBy = c.getString(c.getColumnIndex("ModifyBy"));
                        ModifyDate = c.getString(c.getColumnIndex("ModifyDate"));
                        StatusLink = c.getString(c.getColumnIndex("StatusLink"));

                        SurveySignBoard surveySignBoard = new SurveySignBoard();
                        surveySignBoard.Sign_No = Sign_No;
                        surveySignBoard.Sign_Type = Sign_Type;
                        surveySignBoard.Sign_txt = Sign_txt;
                        surveySignBoard.Sign_Width = Sign_Width;
                        surveySignBoard.Sign_Higth = Sign_Higth;
                        surveySignBoard.Sign_Side = Sign_Side;
                        surveySignBoard.Sign_Tax = Sign_Tax;
                        surveySignBoard.CreateBy = CreateBy;
                        surveySignBoard.CreateDate = CreateDate;
                        surveySignBoard.ModifyBy = ModifyBy;
                        surveySignBoard.ModifyDate = ModifyDate;
                        surveySignBoard.StatusLink = StatusLink;
                        surveySignBoard.Parcel_ID = Parcel_ID;
                        surveySignBoard.Parcel_Ser = Parcel_Ser;
                        surveySignBoard.Parcel_Tmb = Parcel_Tmb;

                        arrSurveySignBoard.add(surveySignBoard);
                        c.moveToNext();
                    }

                    return arrSurveySignBoard;
                }
                else
                    return arrSurveySignBoard;

            } catch (Exception ex) {
                Log.e(Tag, "getSurveySignBoardOwner()-->ex = " + ex.toString());
            } finally {
                if (c != null)
                    c.close();
            }
        }

        return arrSurveySignBoard;
    }

    public static ArrayList<BuildingInfo> getBuildingInfo( String parcel_id ,
                                                           String parcel_ser,
                                                           String Build_count)
    {
        ArrayList<BuildingInfo> arLstBuildInfo = new ArrayList<BuildingInfo>();
        ArrayList<SurveyBuilding> arLstSurveyBuilding = null;
        ArrayList<SurveyBuildingOwner> arLstSurveyBuildingOwner = null;
        arLstSurveyBuilding = getSurveyBuilding(parcel_id,parcel_ser,"");
        arLstSurveyBuildingOwner = getSurveyBuildingOwner(parcel_id,parcel_ser,"");

        for( int n=0; n < arLstSurveyBuilding.size(); n++ )
        {
            BuildingInfo buildingInfo = new BuildingInfo();

            if( arLstSurveyBuilding.size() > n )
            {
                ArrayList<SurveyBuilding> arLstTmpSignBoard = new ArrayList<SurveyBuilding>();
                arLstTmpSignBoard.add(arLstSurveyBuilding.get(n));
                buildingInfo.arLstSurveyBuilding = arLstTmpSignBoard;
            }

            if( arLstSurveyBuildingOwner.size() > n )
            {
                ArrayList<SurveyBuildingOwner> arLstTmpSignBoardOwner = new ArrayList<SurveyBuildingOwner>();
                arLstTmpSignBoardOwner.add(arLstSurveyBuildingOwner.get(n));
                buildingInfo.arLstSurveyBuildingOwner = arLstTmpSignBoardOwner;
            }


            arLstBuildInfo.add(buildingInfo);
        }


        return arLstBuildInfo;
    }

    public static ArrayList<SignBoardInfo> getSignBoardInfo(String parcel_id ,
                                                            String parcel_ser,
                                                            String Build_count)
    {
        ArrayList<SignBoardInfo> arLstBuildInfo = new ArrayList<SignBoardInfo>();
        ArrayList<SurveySignBoard> arLstSurveySignBoard = null;
        ArrayList<SurveySignBoardOwner> arLstSurveySignBoardOwner = null;
        arLstSurveySignBoard = getSurveySignBoard(parcel_id,parcel_ser);
        arLstSurveySignBoardOwner = getSurveySignBoardOwner(parcel_id,parcel_ser);

        for( int n=0; n < arLstSurveySignBoard.size(); n++ )
        {
            SignBoardInfo buildingInfo = new SignBoardInfo();
            ArrayList<SurveySignBoard> arLstTmpSignBoard = new ArrayList<SurveySignBoard>();
            arLstTmpSignBoard.add(arLstSurveySignBoard.get(n));
            buildingInfo.arLstSurveySignBoard = arLstTmpSignBoard;

            ArrayList<SurveySignBoardOwner> arLstTmpSignBoardOwner = new ArrayList<SurveySignBoardOwner>();
            arLstTmpSignBoardOwner.add(arLstSurveySignBoardOwner.get(n));
            buildingInfo.arLstSurveySignBoardOwner = arLstTmpSignBoardOwner;
            arLstBuildInfo.add(buildingInfo);
        }
        return arLstBuildInfo;
    }

    public static ArrayList<ParcelInfo> getParcelInfo(String parcel_id ,
                                                        String parcel_ser,
                                                        String Build_count)
    {
        ArrayList<ParcelInfo> arLstParcelInfo = new ArrayList<ParcelInfo>();

        ParcelInfo parcelInfo = new ParcelInfo();

        parcelInfo.arLstLandUse = getSurveyParcelLandUse(parcel_id,parcel_ser,Build_count);
        parcelInfo.arLstParcelOwner = getSurveyParcelOwner(parcel_id,parcel_ser);

        if( parcelInfo.arLstLandUse.size() > 0 ||
                parcelInfo.arLstParcelOwner.size() > 0 )
        {
            arLstParcelInfo.add(parcelInfo);
        }

        return arLstParcelInfo;
    }

//    public static SurveyParcelOwner getSurveyParcelOwner(String parcel_id , String parcel_ser)
//    {
//        SurveyParcelOwner surveyParcelOwner = new SurveyParcelOwner();
//
//        String sql = " SELECT * "+
//                " FROM SurveyParcelOwner "+
//                " WHERE SurveyParcelOwner.Parcel_ID = '"+parcel_id+"' "+
//                " AND SurveyParcelOwner.Parcel_Ser = '"+parcel_ser+"' ";
//
//        Log.e(Tag,"sql = "+sql);
//        Cursor c = mSqliteDB.rawQuery(sql, null);
//        if (c != null) {
//            try {
//                if( c.moveToFirst() )
//                {
//                    String Adds = "";
//                    String fullName = "";
//                    String CustomerID = "";
//                    String parcelId = "";
//                    String parcelSer = "";
//                    for( int n=0; n < c.getCount(); n++ )
//                    {
//                        fullName += c.getString(c.getColumnIndex("FullName"));
//                        fullName += ",";
//
//                        Adds = c.getString(c.getColumnIndex("Adds"));
//                        CustomerID = c.getString(c.getColumnIndex("CustomerID"));
//                        parcelId = c.getString(c.getColumnIndex("Parcel_ID"));
//                        parcelSer = c.getString(c.getColumnIndex("Parcel_Ser"));
//
//                        c.moveToNext();
//                    }
//                    fullName = fullName.substring(0,fullName.length()-1); //remove ,
//                    surveyParcelOwner.fullName = fullName;
//                    surveyParcelOwner.address = Adds;
//                    surveyParcelOwner.CustomerID = CustomerID;
//                    surveyParcelOwner.Parcel_Ser = parcelSer;
//                    surveyParcelOwner.Parcel_ID = parcelId;
//
//                    return surveyParcelOwner;
//                }
//                else
//                    return null;
//
//            } catch (Exception ex) {
//                Log.e(Tag, "getLandInfo()-->ex = " + ex.toString());
//            } finally {
//                if (c != null)
//                    c.close();
//            }
//        }
//
//        return surveyParcelOwner;
//    }

    public static ArrayList<SurveyBuilding> getSurveyBuilding(
            String parcel_id ,
            String parcel_ser,
            String Build_count)
    {
        ArrayList<SurveyBuilding> arrSurveySignBoard = new ArrayList<SurveyBuilding>();

        String sql = "";

        //if( TextUtils.isEmpty(Build_count) )
        {
            sql = " SELECT * "+
                    " FROM SurveyBuilding "+
                    " WHERE SurveyBuilding.Parcel_ID = '"+parcel_id+"' "+
                    " AND SurveyBuilding.Parcel_Ser = '"+parcel_ser+"' ";
        }
//        else
//        {
//            sql = " SELECT * "+
//                    " FROM SurveyBuilding "+
//                    " WHERE SurveyBuilding.Parcel_ID = '"+parcel_id+"' "+
//                    " AND SurveyBuilding.Parcel_Ser = '"+parcel_ser+"' "+
//                    " AND SurveyBuilding.Build_No = '"+Build_count+"' ";
//        }

        Log.e(Tag,"sql = "+sql);
        Cursor c = mSqliteDB.rawQuery(sql, null);
        if (c != null) {
            try {
                if( c.moveToFirst() )
                {
                    for( int n=0; n < c.getCount(); n++ )
                    {
                        int Build_No = -1;
                        String Parcel_ID = "";
                        String Parcel_Ser = "";
                        String Parcel_Tmb = "";
                        String Building_Group = "";
                        String Building_Moo = "";
                        String Building_Num = "";
                        String Building_Location = "";
                        String Building_Type = "";
                        String Building_Room = "";
                        String Building_Floor = "";
                        String Building_Width = "";
                        String Building_Higth = "";
                        String Building_UseA = "";
                        String Building_UseB = "";
                        String Building_UseC = "";
                        String Building_Rent = "";
                        String Building_Tax = "";
                        String CreateBy = "";
                        String CreateDate = "";
                        String ModifyBy = "";
                        String ModifyDate = "";
                        String StatusLink = "";

                        Build_No = c.getInt(c.getColumnIndex("Build_No"));
                        Parcel_ID = c.getString(c.getColumnIndex("Parcel_ID"));
                        Parcel_Ser = c.getString(c.getColumnIndex("Parcel_Ser"));
                        Parcel_Tmb = c.getString(c.getColumnIndex("Parcel_Tmb"));
                        Building_Group = c.getString(c.getColumnIndex("Building_Group"));
                        Building_Moo = c.getString(c.getColumnIndex("Building_Moo"));
                        Building_Num = c.getString(c.getColumnIndex("Building_Num"));
                        Building_Location = c.getString(c.getColumnIndex("Building_Location"));
                        Building_Type = c.getString(c.getColumnIndex("Building_Type"));
                        Building_Room = c.getString(c.getColumnIndex("Building_Room"));
                        Building_Width = c.getString(c.getColumnIndex("Building_Width"));
                        Building_Higth = c.getString(c.getColumnIndex("Building_Higth"));
                        Building_UseA = c.getString(c.getColumnIndex("Building_UseA"));
                        Building_UseB = c.getString(c.getColumnIndex("Building_UseB"));
                        Building_UseC = c.getString(c.getColumnIndex("Building_UseC"));
                        Building_Rent = c.getString(c.getColumnIndex("Building_Rent"));
                        Building_Tax = c.getString(c.getColumnIndex("Building_Tax"));
                        Building_Floor = c.getString(c.getColumnIndex("Building_Floor"));
                        CreateBy = c.getString(c.getColumnIndex("CreateBy"));
                        CreateDate = c.getString(c.getColumnIndex("CreateDate"));
                        ModifyBy = c.getString(c.getColumnIndex("ModifyBy"));
                        ModifyDate = c.getString(c.getColumnIndex("ModifyDate"));
                        StatusLink = c.getString(c.getColumnIndex("StatusLink"));

                        SurveyBuilding surveyBuilding = new SurveyBuilding();
                        surveyBuilding.Parcel_ID = Parcel_ID;
                        surveyBuilding.Parcel_Ser = Parcel_Ser;
                        surveyBuilding.Parcel_Tmb = Parcel_Tmb;
                        surveyBuilding.Build_No = Build_No;
                        surveyBuilding.Building_Group = Building_Group;
                        surveyBuilding.Building_Moo = Building_Moo;
                        surveyBuilding.Building_Floor = Building_Floor;
                        surveyBuilding.Building_Num = Building_Num;
                        surveyBuilding.Building_Location = Building_Location;
                        surveyBuilding.Building_Type = Building_Type;
                        surveyBuilding.Building_Room = Building_Room;
                        surveyBuilding.Building_Width = Building_Width;
                        surveyBuilding.Building_Higth = Building_Higth;
                        surveyBuilding.Building_UseA = Building_UseA;
                        surveyBuilding.Building_UseB = Building_UseB;
                        surveyBuilding.Building_UseC = Building_UseC;
                        surveyBuilding.Building_Rent = Building_Rent;
                        surveyBuilding.Building_Tax = Building_Tax;

                        surveyBuilding.CreateBy = CreateBy;
                        surveyBuilding.CreateDate = CreateDate;
                        surveyBuilding.ModifyBy = ModifyBy;
                        surveyBuilding.ModifyDate = ModifyDate;
                        surveyBuilding.StatusLink = StatusLink;

                        arrSurveySignBoard.add(surveyBuilding);
                        c.moveToNext();
                    }

                    return arrSurveySignBoard;
                }
                else
                    return arrSurveySignBoard;

            } catch (Exception ex) {
                Log.e(Tag, "getSurveySignBoardOwner()-->ex = " + ex.toString());
            } finally {
                if (c != null)
                    c.close();
            }
        }

        return arrSurveySignBoard;
    }
    public static ArrayList<String> getParcelOwner()
    {
        ArrayList<String> arLstParcelOwner = new ArrayList<>();
        Cursor c = mSqliteDB.rawQuery("select * from SurveyParcelOwner", null);
        if (c != null) {
            try {
                if( c.moveToFirst() )
                {
                    for( int n=0; n < c.getCount(); n++ )
                    {
                        String Parcel_ID = "";
                        String Parcel_Ser = "";
                        String FullName = "";
                        String Adds = "";
                        String CustomerID = "";

                        Parcel_ID = c.getString(c.getColumnIndex("Parcel_ID"));
                        Parcel_Ser = c.getString(c.getColumnIndex("Parcel_Ser"));
                        FullName = c.getString(c.getColumnIndex("FullName"));
                        Adds = c.getString(c.getColumnIndex("Adds"));
                        CustomerID = c.getString(c.getColumnIndex("CustomerID"));

                        arLstParcelOwner.add(FullName);
                        c.moveToNext();
                    }

                    return arLstParcelOwner;
                }
                else
                    return arLstParcelOwner;

            } catch (Exception ex) {
                Log.e(Tag, "getSurveySignBoardOwner()-->ex = " + ex.toString());
            } finally {
                if (c != null)
                    c.close();
            }
        }
        return arLstParcelOwner;

    }
    public static ArrayAdapter<String> getParcelOwnerAdapter(Context context)
    {
        ArrayAdapter<String> arAdapter = new ArrayAdapter<String>(context, R.layout.spinner_item, getParcelOwner());
        arAdapter.setDropDownViewResource(R.layout.spinner_item);

        return arAdapter;
    }

    public static ArrayAdapter<String> getAdapter(Context context , ArrayList<String> arlst)
    {
        ArrayAdapter<String> arAdapter = new ArrayAdapter<String>(context, R.layout.spinner_item,arlst);
        arAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        return arAdapter;
    }

    public static ArrayAdapter<float[]> getAdapterFloat(Context context ,float[] arrFloat)
    {
        List<float[]> arLst = Arrays.asList(arrFloat);
        ArrayAdapter<float[]> arAdapter = new ArrayAdapter<float[]>(context, R.layout.spinner_item,arLst);
        arAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        return arAdapter;
    }

}
